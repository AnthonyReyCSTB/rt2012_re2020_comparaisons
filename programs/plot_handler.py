# =========================================
# coding = utf-8
# =========================================
# Name         : plot_handler.py
# Author       : Anthony Rey
# Version      : 1.0.0.0
# Date         : 03/10/2019
# Update       : 14/10/2019
# Description  : Class used for plotting data more easily
# ========================================

# ========================================
# Links
# ========================================

#

# ========================================
# Packages
# ========================================

# Python core packages/libraries/modules
try:
    import os
except ImportError:
    raise ImportError("Sorry, but the \"os\" module/package is required to run this program!")
try:
    import operator
except ImportError:
    raise ImportError("Sorry, but the \"operator\" module/package is required to run this program!")
try:
    import pandas as pd
except ImportError:
    raise ImportError("Sorry, but the \"pandas\" module/package is required to run this program!")
try:
    import numpy as np
except ImportError:
    raise ImportError("Sorry, but the \"numpy\" module/package is required to run this program!")
try:
    import matplotlib
except ImportError:
    raise ImportError("Sorry, but the \"matplotlib\" module/package is required to run this program!")
try:
    import matplotlib.pyplot as plt
except ImportError:
    raise ImportError("Sorry, but the \"matplotlib\" module/package is required to run this program!")
try:
    import seaborn
except ImportError:
    raise ImportError("Sorry, but the \"seaborn\" module/package is required to run this program!")
try:
    import copy
except ImportError:
    raise ImportError("Sorry, but the \"copy\" module/package is required to run this program!")
try:
    import collections
except ImportError:
    raise ImportError("Sorry, but the \"collections\" module/package is required to run this program!")

# Python extension packages/libraries/modules

# Set the figure to the default style
matplotlib.style.use("classic")

# ========================================
# Classes
# ========================================

# Initialize the global variable "NUMBER_OF_GRAPHS" to avoid overlapping
NUMBER_OF_GRAPHS = 0


# Class used for plotting data more easily
class PlotHandler:
    """
    Class used for plotting data more easily
    ...
    Constants
    ---------
    None

    Class attributes
    ----------------
    None

    Instance attributes
    -------------------
    None

    Methods
    -------
    display_violin_plot(self, figure_name, given_function, indicators, vertical_axis_name, horizontal_axis_name, axes = None, tick_labels = None, occurrences = False):
        Display a violin plot figure (and save it at the predefined location)
    display_scatter_plot(self, figure_name, indicators, vertical_axis_name, horizontal_axis_name, axes = None, tick_labels = None):
        Display scatter plot figure (and save it at the predefined location)
    """

    # Initialize (construct) a PlotHandler object
    def __init__(self, dataframe, usages, systems, figures_path):
        """
        Initialize (construct) a PlotHandler object
        -------------------------------------------------------------
        Parameters
        ----------
        dataframe: pandas.DataFrame
            Dataframe containing the data
        usages: list
            List of conditions on the usages
        systems: list
            List of conditions on the systems
        figures_path: str
            Path to the directory where the figure will be saved

        Returns
        -------
        PlotHandler object

        Example
        -------
        >>> PlotHandler(dataframe, ["mi", "lc"], [], ".//data//")
        """
        # Copy dataframe(in case we could need the initial one later)
        self.dataframe_temporary    = copy.deepcopy(dataframe)
        # Store initial dataframe in memory (just in case)
        self.dataframe              = dataframe
        # Save other parameters
        self._additional_dataframe  = None
        self.usages                 = usages
        self.systems                = systems
        self.figures_path           = figures_path

    # Display a violin plot figure (and save it at the predefined location)
    def display_violin_plot(self, figure_name, given_function, indicators, vertical_axis_name, horizontal_axis_name, axes = None, tick_labels = None, show_occurrences = False):
        """
        Display a violin plot figure (and save it at the predefined location)
        -------------------------------------------------------------
        Parameters
        ----------
        figure_name: str
            Name of figure
        given_function: function
            Function to build the dictionary (later converted to a dataframe) used for the violin plot
        indicators: list
            List of indicators for the horizontal and vertical axes
        vertical_axis_name: str
            Name of the vertical axis
        horizontal_axis_name: str
            Name of the horizontal axis
        axes: list (optional = None)
            List of tuples of values representing the (vertical) axes and ticks [(min,max), (min, max + step, step)]
        tick_labels: list (optional = None)
            List of names for the (vertical) tick labels
        show_occurrences: bool (optional = False)
            Tell whether or not the number of cases (occurrences) should be displayed in the figure

        Returns
        -------
        None

        Example
        -------
        >>> display_violin_plot(["system", "bbio"], "Typologies [-]", "Bbio [-]")
        """
        # Apply the filters previously defined
        self._apply_filters()
        # Build a dataframe for the figure based on a given function
        dictionary = self._build_dictionary_for_figure(given_function, indicators)
        # Generate figure
        self._generate_figure(dictionary, figure_name, given_function, indicators, vertical_axis_name, horizontal_axis_name, axes, tick_labels, show_occurrences)

    # Display scatter plot figure (and save it at the predefined location)
    def display_scatter_plot(self, figure_name, given_function, indicators, vertical_axis_name, horizontal_axis_name, axes = None, tick_labels = None):
            """
            Display scatter plot figure (and save it at the predefined location)
            -------------------------------------------------------------
            Parameters
            ----------
            figure_name: str
                Name of figure
            given_function: function
                Function to build the dictionary (later converted to a dataframe) used for the violin plot
            indicators: list
                List of indicators for the horizontal and vertical axes
            vertical_axis_name: str
                Name of the vertical axis
            horizontal_axis_name: str
                Name of the horizontal axis
            axes: list (optional = None)
                List of tuples of values representing the (vertical) axes and ticks [(min,max), (min, max + step, step)]
            tick_labels: list (optional = None)
                List of names for the (vertical) tick labels

            Returns
            -------
            None

            Example
            -------
            >>> display_scatter_plot(["system", "bbio"], "Typologies [-]", "Bbio [-]")
            """
            # Apply the filters previously defined
            self._apply_filters()
            # Build a dataframe for the figure based on a given function
            dictionary = self._build_dictionary_for_figure(given_function, indicators)
            # Generate figure
            self._generate_figure(dictionary, figure_name, None, indicators, vertical_axis_name, horizontal_axis_name, axes, tick_labels, False)

    def add_additional_dataframe(self, dataframe):
        self._additional_dataframe = copy.deepcopy(dataframe)

    # Apply the filters previously defined
    def _apply_filters(self):
        """
        Apply the filters previously defined
        -------------------------------------------------------------
        Parameters
        ----------
        None

        Returns
        -------
        None

        Example
        -------
        >>> _apply_filters()
        """
        # Filter dataframe based on usages and systems
        self._filtered_dataframe(self.usages)
        self._filtered_dataframe(self.systems)

    # Get the number of occurrences for each type of series
    def _get_number_of_occurrences(self, given_function, indicators):
        """
        Get the number of occurrences for each type of series
        -------------------------------------------------------------
        Parameters
        ----------
        given_function: function
            Function taking a dataframe and indicators as arguments then returning a dictionary
        indicators: list
            List of indicators for the horizontal and vertical axes

        Returns
        -------
        A dictionary with each type of series (as a key) and its length (as a value)

        Example
        -------
        >>> _get_number_of_occurrences(first_function,["system", "bbio"])
        """
        # Build a dataframe for the figure based on a given function
        dataframe = self._build_dictionary_for_figure(given_function, indicators)
        # Return a dictionary with each type of series (as a key) and its length (as a value)
        return {key:len(dataframe[key]) for key in dataframe.keys()}

    # Filter a dataframe based on some conditions (list of conditions)
    def _filtered_dataframe(self, conditions):
        """
        Filter a dataframe based on some conditions (list of conditions)
        -------------------------------------------------------------
        Parameters
        ----------
        conditions: list
            List of tuples, each containing a condition in the following format : (condition, operator, value)

        Returns
        -------
        None

        Example
        -------
        >>> _filtered_dataframe([("usage", "==", "mi")])
        """
        if conditions:
            # Filter the dataframe by usage (keep only the desired ones)
            mask = np.logical_or.reduce(
                [self._get_operator(condition[1])(self.dataframe_temporary[condition[0]], condition[2]) for condition in
                 conditions])
            # Keep only rows which satisfy the conditions
            self.dataframe_temporary = self.dataframe_temporary[mask]

    # Return a python operator based on some string operator
    def _get_operator(self, operator_string):
        """
        Return a python operator based on some string operator
        -------------------------------------------------------------
        Parameters
        ----------
        operator_string: str
            Operator as a string (e.g., "==")

        Returns
        -------
        An operator object (e.g., operator.eq)

        Example
        -------
        >>> _get_operator("==")
        """
        # Dictionary of operators
        dictionary_of_operators = {"<=": operator.lt,
                                   "==": operator.eq}
        # Return the desired operator
        return dictionary_of_operators[operator_string]

    # Build a violing plot
    def _build_dictionary_for_figure(self, given_function, indicators):
        """
        Generate a violing plot
        -------------------------------------------------------------
        Parameters
        ----------
        given_function: function
            Function taking a dataframe and indicators as arguments then returning a dictionary
        indicators: list
            List of indicators for the horizontal and vertical axes

        Returns
        -------
        dictionary: dict
            Dictionary containing the data to be plotted (each key is an element on the x-axis)

        Example
        -------
        >>> _generate_figure("cep_tous_les_usages", ["system", "bbio"], "Systèmes [-]", "Bbio [-]", [(0, 275), (0, 300, 25)], ["mi", "lc"])
        """
        if self._additional_dataframe is None:
            return given_function(self.dataframe_temporary, indicators)
        else:
            return given_function(self.dataframe_temporary, self._additional_dataframe, indicators)

    # Generate a violing plot
    def _generate_figure(self, dictionary, figure_name, given_function, indicators, vertical_axis_name, horizontal_axis_name, axes, tick_labels, show_occurrences):
        """
        Generate a violing plot
        -------------------------------------------------------------
        Parameters
        ----------
        dictionary: dict
            Dictionary containing the data to be plotted (each key is an element on the x-axis)
        figure_name: str
            Name of the figure
        indicators: list
            List of indicators for the horizontal and vertical axes
        vertical_axis_name: str
            Name of the vertical axis
        horizontal_axis_name: str
            Name of the horizontal axis
        axes: list
            List of tuples of values representing the (vertical) axes and ticks [(min,max), (min, max + step, step)]
        tick_labels: list
            List of names for the (vertical) tick labels
        show_occurrences: bool (optional = False)
            Tell whether or not the number of cases (occurrences) should be displayed in the figure

        Returns
        -------
        None

        Example
        -------
        >>> _generate_figure(dictionary, "cep_tous_les_usages", ["system", "bbio"], "Systèmes [-]", "Bbio [-]", [(0, 275), (0, 300, 25)], ["mi", "lc"], True)
        """
        # Use the global variable "NUMBER_OF_GRAPHS"
        global NUMBER_OF_GRAPHS
        # Font size
        font_size = 14
        # Figure
        figure = plt.figure(num = NUMBER_OF_GRAPHS, figsize = (15, 11), facecolor = 'w', edgecolor = 'k')
        # Set up axis
        axis = figure.add_subplot(111)  # number of rows, number of columns, and plot number
        if given_function is None:
            markers = [".", "o", "^", "8", "*", "x", "D"]
            colors  = ['red', 'green', 'yellow', 'peru', 'cyan', 'magenta', 'salmon', 'silver', 'darkseagreen', 'royalblue', 'greenyellow', 'plum', 'rosybrown', 'teal', 'palegreen', 'wheat', 'mediumaquamarine', 'crimson', 'cornsilk', 'cadetblue', 'darkorange']
            for index, key, in enumerate(dictionary.keys()):
                # Graphs
                marker = markers[index]
                color  = colors[index]
                axis.plot(dictionary[key][indicators[1]], dictionary[key][indicators[2]], linestyle = "None", marker = marker, color = color, alpha = 0.6, label =  key)
        else:
            # Compute the number of occurrences for each case
            number_of_occurrences = self._get_number_of_occurrences(given_function, indicators)
            # Generate violin plots
            seaborn.violinplot(data = pd.DataFrame(dictionary), cut = 0)
        # Define the axes if not equal to None
        if axes is not None:
            axis.set_ylim(axes[0][0], axes[0][1])
            axis.set_yticks(np.arange(axes[1][0], axes[1][1], axes[1][2]))

        # Font tick labels
        #print(dictionary.keys())
        #axis.set_xticklabels(axis.get_xticks(), fontsize = font_size) # BE CAREFUL! Numbers instead of text
        if isinstance(axis.get_yticks()[1], float):
            ticks = [round(tick,1) for tick in axis.get_yticks()]
            axis.set_yticklabels(ticks, fontsize=font_size)
        else:
            axis.set_yticklabels(axis.get_yticks(), fontsize=font_size)

        # Define the tick labels if not equal to None
        if tick_labels is not None:
            if show_occurrences is True:
                tick_labels = [tick_label + "\n" + "(nombre de cas : " + str(list(number_of_occurrences.values())[index]) + ")" for index, tick_label in enumerate(tick_labels)]
                axis.set_xticklabels(tick_labels, fontsize = font_size)
            else:
                axis.set_xticklabels(tick_labels, fontsize = font_size)
        else:
            if show_occurrences is True:
                tick_labels = [tick_label + "\n" + "(nombre de cas : " + str(list(number_of_occurrences.values())[index]) + ")" for index, tick_label in enumerate(dictionary.keys())]
                axis.set_xticklabels(tick_labels, fontsize = font_size)

        # Set the labels for each axis (with its font size)
        axis.set_xlabel(vertical_axis_name, fontsize = font_size)
        axis.set_ylabel(horizontal_axis_name, fontsize = font_size)


        if given_function is not None:
            # Minimum value
            maximum_value = max([max(dictionary[key]) for key in dictionary.keys()])
            minimum_value = min([min(dictionary[key]) for key in dictionary.keys()])
            # Add mean to the plots
            for index, key in enumerate(dictionary.keys()):
                axis.text(index + 0.05, np.mean(dictionary[key]), "%0.1f" % np.mean(dictionary[key]), fontsize = 1.5*font_size)
                if "couple_system" in figure_name:
                    if "relatif" in figure_name:
                        co2_mean = np.mean(self.dataframe_temporary[self.dataframe_temporary[indicators[0]] == key][indicators[2]])
                        axis.text(index, maximum_value*1.09, "CO$_{\mathrm{2}}$\n[kg$_{\mathrm{eq}}$ CO$_{\mathrm{2}}$/(m²$\cdot$an)]\n%0.1f" % co2_mean, fontsize = font_size, ha = "center", va = "center")

                    else:
                        co2_mean = np.mean(self.dataframe_temporary[self.dataframe_temporary[indicators[0]] == key][indicators[2]])
                        axis.text(index, minimum_value*0.55, "CO$_{\mathrm{2}}$\n[kg$_{\mathrm{eq}}$ CO$_{\mathrm{2}}$/(m²$\cdot$an)]\n%0.1f" % co2_mean, fontsize = font_size, ha = "center", va = "center")


        # Adjust everything automatically
        plt.tight_layout()
        if given_function is None:
            axis.legend(loc = "upper right", numpoints = 1, prop = {"size": font_size})
        plt.savefig(self.figures_path + os.sep + figure_name + ".png", format = "png", dpi = 350)
        # Increment the number of graphs
        NUMBER_OF_GRAPHS += 1

# ========================================
# Functions
# ========================================

























