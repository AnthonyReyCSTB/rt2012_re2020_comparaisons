#=========================================
# coding = utf-8
#=========================================
# Name         : buildings_handler.py
# Author       : Anthony Rey
# Version      : 1.0.0.0
# Date         : 04/10/2019
# Update       : 07/01/2019
# Description  : Class used for data storage of buildings' characteristics
#========================================

#========================================
# Links
#========================================

#

#========================================
# Packages
#========================================

# Python core packages/libraries/modules
try:
    import copy
except ImportError:
    raise ImportError("Sorry, but the \"copy\" module/package is required to run this program!")
    
# Python extension packages/libraries/modules


#========================================
# Classes
#========================================

# Class used for data storage of buildings' characteristics
class BuildingsHandler:
    """
    Class representing a point
    ...
    Constants
    ---------
    None
    
    Class attributes
    ----------------
    None
    
    Instance attributes
    -------------------
    None
    
    Methods
    -------
    set_directory_name(self, simulation_directory_name):
        Set the directory's name where the simulation has been performed
    set_building_characteristic(self, characteristic_name, value):
        Set the value of a given building characteristic  
    reset_counter(self):
        Reset the counter (counting the total number of buildings)
    update_counter(self):
        Update the counter (counting the total number of buildings)
    get_buildings_characteristics(self):
        Return a copy of all the buildings' characteristics stored in self._characteristics 
    """
    
    # Initialize (construct) a BuildingsHandler object
    def __init__(self):
        """
        Initialize (construct) a BuildingsHandler object
        -------------------------------------------------------------
        Parameters
        ----------
        simulation_directory: str
            Simulation directory (where the simulation has been run)
            
        Returns
        -------
        BuildingsHandler object

        Example
        -------
        >>> BuildingHandler()
        """
        # Create counters to keep track of where we are (initialized at 1)
        self._counter                               = 1
        self._temporary_counter                     = 1
        # Store temporarily the name of the last building characteristic under study
        self._temporary_characteristic_name         = None
        # Store temporarily the name of the directory where the simulation is stored
        self._directory_name                        = None
        # Create a dicitonary where all the buildings' characteristics will be stored
        self._characteristics                       = dict()
        self._characteristics[self._counter]        = dict()

        
    # Set the directory's name where the simulation has been performed
    def set_directory_name(self, simulation_directory_name):
        """
        Set the directory's name where the simulation has been performed
        -------------------------------------------------------------
        Parameters
        ----------
        simulation_directory_name: str
            Name of the directory where the simulation has been performed

        Returns
        -------
        None

        Example
        -------
        >>> set_directory_name("0")
        """       
        self._characteristics[self._temporary_counter]["directory"] = simulation_directory_name
        self._directory_name = simulation_directory_name

    # Set the value of a given building characteristic  
    def set_building_characteristic(self, characteristic_name, value):
        """
        Set the value of a given building characteristic  
        -------------------------------------------------------------
        Parameters
        ----------
        characteristic_name: str
            Desired characteristic's name of the building
        value: float 
            Value associated with the desired characteristic of the building 

        Returns
        -------
        None

        Example
        -------
        >>> set_building_characteristic("cep", 48)
        """ 
        # Initialize the "temporary_characteristic_name" variable
        if self._temporary_characteristic_name is None:
            self._temporary_characteristic_name = characteristic_name
        else:
            # Increment counter only if the same building characteristic is under study (meaning more than one building)
            if characteristic_name == self._temporary_characteristic_name:
                self._increment_counter()
                self.set_directory_name(self._directory_name)
        # Store the desired characteristic of the building 
        self._store_building_characteristic(characteristic_name, value)
        # Update the building characteristic under study
        self._temporary_characteristic_name = characteristic_name
            
    # Reset the counter (counting the total number of buildings)
    def reset_counter(self):
        """
        Reset the counter (counting the total number of buildings)
        -------------------------------------------------------------
        Parameters
        ----------
        None
            
        Returns
        -------
        None

        Example
        -------
        >>> reset_counter()
        """
        self._temporary_counter = self._counter
        
    # Update the counter (counting the total number of buildings)
    def update_counter(self):
        """
        Update the counter (counting the total number of buildings)
        -------------------------------------------------------------
        Parameters
        ----------
        None
            
        Returns
        -------
        None

        Example
        -------
        >>> update_counter()
        """
        self._increment_counter()
        self._counter = self._temporary_counter
        
    # Return a copy of all the buildings' characteristics stored in self._characteristics 
    def get_buildings_characteristics(self):
        """
        Return a copy of all the buildings' characteristics stored in self._characteristics 
        -------------------------------------------------------------
        Parameters
        ----------
        None
            
        Returns
        -------
        self._characteristics: dict
            Copy of all the buildings' characteristics stored in self._characteristics 

        Example
        -------
        >>> get_buildings_characteristics()
        """
        # Remove empty dictionary value before copying
        return copy.deepcopy({key: value for key, value in self._characteristics.items() if bool(value) is not False})
        
    # Increment the counter (counting the total number of buildings)
    def _increment_counter(self):
        """
        Increment the counter (counting the total number of buildings)
        -------------------------------------------------------------
        Parameters
        ----------
        None
            
        Returns
        -------
        None

        Example
        -------
        >>> _increment_counter()
        """
        self._temporary_counter += 1
        self._characteristics.setdefault(self._temporary_counter, dict())
            
    # Store the value of the desired characteristic of the building 
    def _store_building_characteristic(self, characteristic_name, value):
        """
        Store the value of the desired characteristic of the building 
        -------------------------------------------------------------
        Parameters
        ----------
        characteristic_name: str
            Desired characteristic's name of the building
        value: float 
            Value associated with the desired characteristic of the building 
            
        Returns
        -------
        None

        Example
        -------
        >>> _store_building_characteristic("cep", 48)
        """
        self._characteristics[self._temporary_counter][characteristic_name] = value
        
        
        