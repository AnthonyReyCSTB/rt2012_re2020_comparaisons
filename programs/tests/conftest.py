#=========================================
# coding = utf-8
#=========================================
# Name         : conftest.py
# Author       : Anthony Rey
# Version      : 1.0.0.0
# Date         : 07/10/2019
# Update       : 07/10/2019
# Description  : Define fixture functions for the rt2012_re2020_comparisons project
#========================================

#========================================
# Links
#========================================

#

#========================================
# Packages
#========================================

# Python core packages/libraries/modules
try:
    import pytest
except ImportError:
    raise ImportError("Sorry, but the \"pytest\" module/package is required to run this program!")
try:
    import pandas as pd 
except ImportError:
    raise ImportError("Sorry, but the \"pandas\" module/package is required to run this program!")
try:
    import random
except ImportError:
    raise ImportError("Sorry, but the \"random\" module/package is required to run this program!")
    
# Python extension packages/libraries/modules


#========================================
# Classes
#========================================

#========================================
# Functions
#========================================

def _get_usage(random_usage):
        if random_usage == 0:
            usage = "mi"
        elif random_usage == 1:
            usage = "lc"
        elif random_usage == 2:
            usage = "bu"
        else:
            usage = "ens"
        return usage

def _get_system(random_system):
        if random_system == 0:
            system = "ds-ds"
        elif random_system == 1:
            system = "ch_cet"
        elif random_system == 2:
            system = "poele-cet"
        elif random_system == 3:
            system = "ch_ch"
        elif random_system == 4:
            system = "ej-cet"
        else:
            system = "pac-cet"
        return system

@pytest.fixture
def input_dataframe():
    # Initialize a list of lists (containing data)
    number_of_simulations   = 2000 
    data                    = []
    for index in range(0, number_of_simulations):
        random_usage    = random.randint(0,3)
        random_system   = random.randint(0,5)
        data.append([str(index), str(index), _get_usage(random_usage), _get_system(random_system), random.randint(35,65), random.gauss(random.randint(45,55), random.randint(2,7))])
    # Create the pandas DataFrame 
    input = pd.DataFrame(data, columns = ["index", "directory", "usage", "system", "cep", "bbio"]) 
    return input
        
@pytest.fixture
def input_directory_path():
    input = r".//data"
    return input 
    
@pytest.fixture
def input_usages():
    input = ["mi", "lc", "bu", "ens"]
    return input

@pytest.fixture
def input_building_characteristics():
    input = [("cep", "/Sortie_Batiment_C/O_Cep_annuel", "kWh/m²"), 
             ("bbio", "/Sortie_Batiment_B/O_Bbio_pts_annuel", "-")]
    return input

@pytest.fixture
def input_figure_path():
    # MUST BE LAUNCHED FROM THE TESTS DIRECTORY (pytest)
    input = r".//data//figures" 
    return input 
    












