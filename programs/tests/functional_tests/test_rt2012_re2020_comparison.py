#=========================================
# coding = utf-8
#=========================================
# Name         : test_rt2012_re2020_comparison.py
# Author       : Anthony Rey
# Version      : 1.0.0.0
# Date         : 07/10/2019
# Update       : 07/10/2019
# Description  : Tests for the rt2012_re2020_comparisons project
#========================================

#========================================
# Links
#========================================

#

#========================================
# Packages
#========================================

# Python core packages/libraries/modules
try:
    import os
except ImportError:
    raise ImportError("Sorry, but the \"os\" module/package is required to run this program!")
try:
    import pytest
except ImportError:
    raise ImportError("Sorry, but the \"pytest\" module/package is required to run this program!")
    
# Python extension packages/libraries/modules
try:
    from plot_handler import PlotHandler
except ImportError:
    raise ImportError("Sorry, but the \"miracle\" module/package is required to run this program!")
try:
    from analyzer import Analyzer
except ImportError:
    raise ImportError("Sorry, but the \"analyzer\" module/package is required to run this program!")
    
    
#========================================
# Classes
#========================================

#========================================
# Functions
#========================================

@pytest.mark.rt2012_re2020_comparisons
def test_analyzer(input_directory_path, input_building_characteristics, input_usages):
    analyzer    = Analyzer(input_directory_path, input_building_characteristics, input_usages) 
    dataframe   = analyzer.get_results()
    assert sorted(dataframe[input_usages[0]].columns[:2]) == sorted([elements[0] for elements in input_building_characteristics])

@pytest.mark.rt2012_re2020_comparisons
def test_plot_handler(input_dataframe, input_figure_path):
    files = [file for file in os.listdir(input_figure_path)]
    for file in files:
        os.remove(os.path.join(input_figure_path, file))
    analysis = PlotHandler(input_dataframe, [("usage", "==", "mi")], [], input_figure_path)
    analysis.display_violin_plot(["system", "bbio"], "Systèmes [-]", "Bbio [-]")
    assert os.path.isfile(input_figure_path + os.sep + "bbio.png") == True    

    