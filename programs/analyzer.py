# =========================================
# coding = utf-8
# =========================================
# Name         : analyzer.py
# Author       : Anthony Rey
# Version      : 1.0.0.0
# Date         : 03/10/2019
# Update       : 07/10/2019
# Description  : Class to analyze re-simulated xml cases
# ========================================

# ========================================
# Links
# ========================================

#

# ========================================
# Packages
# ========================================

# Python core packages/libraries/modules
try:
    import os
except ImportError:
    raise ImportError("Sorry, but the \"os\" module/package is required to run this program!")
try:
    import pandas as pd
except ImportError:
    raise ImportError("Sorry, but the \"pandas\" module/package is required to run this program!")

# Python extension packages/libraries/modules
try:
    import pycometh
except ImportError:
    raise ImportError("Sorry, but the \"pycometh\" module/package is required to run this program!")
try:
    from buildings_handler import BuildingsHandler
except ImportError:
    raise ImportError("Sorry, but the \"building\" module/package is required to run this program!")


# ========================================
# Classes
# ========================================

# Class to analyze re-simulated xml cases
class Analyzer:
    """
    Class to analyze re-simulated xml cases
    ...
    Constants
    ---------
    None

    Class attributes
    ----------------
    None

    Instance attributes
    -------------------
    None

    Methods
    -------
    get_results(self):
        Get results based on the given output directory and usages
    """

    # Initialize (construct) a Analyzer object
    def __init__(self, output_directory_path, desired_building_characteristics, usages):
        """
        Initialize (construct) a Analyzer object
        -------------------------------------------------------------
        Parameters
        ----------
        output_directory_path: str
            Path to the directory where all the simulations have been performed
        desired_building_characteristics: list
            List of building characteristics (outputs of interest from the xml file) under the following format (name, path, unit)
        usages: list
            List of usages of interest under the following format (name, path, values) - the name must be equal to the name of the directory where the simulations are stored

        Returns
        -------
        Analyzer object

        Example
        -------
        >>> Analyzer("C://Users//outputs", [("cep","/Sortie_Batiment_C/O_Cep_annuel","kwh/(m².an)")], [("mi", "/Sortie_Zone_C/Usage", [1,2]), ("lc", "/Sortie_Zone_C/Usage", [3])])
        """
        # Save parameters
        self._output_directory_path = output_directory_path
        self._desired_building_characteristics = desired_building_characteristics
        self._usages = usages

        # Get results based on the given output directory and usages

    def get_results(self):
        """
        Get results based on the given output directory and usages
        -------------------------------------------------------------
        Parameters
        ----------
        None

        Returns
        -------
        self._dataframe: pandas.DataFrame
            Dataframe containing all the results (row = simulation / column = output/buildinc characteristic of interest)

        Example
        -------
        >>> get_results()
        """
        # Create an empty directory to store the results per usage
        self._dataframe = dict()
        # Read results
        self._read_results()
        # Return the dataframe containing the results
        return self._dataframe

    # Read results based on the given output directory and usages
    def _read_results(self):
        """
        Read results based on the given output directory and usages
        -------------------------------------------------------------
        Parameters
        ----------
        None

        Returns
        -------
        None

        Example
        -------
        >>> _read_results()
        """
        # Create an empty directory to store the results
        self._results = dict()
        # Iterate through each usage
        for usage in self._usages:
            self._read_results_per_usage(usage)
            # Store the results (dictionary) in a dataframe
            self._dataframe[usage[0]] = pd.DataFrame(self._results[usage[0]])
            # Transform the dataframe to have each case as a row
            self._dataframe[usage[0]] = self._dataframe[usage[0]].T
            # Set the index name
            self._dataframe[usage[0]].index.name = "index"

            # Read results based on the given usage

    def _read_results_per_usage(self, usage):
        """
        Read results based on the given usage
        -------------------------------------------------------------
        Parameters
        ----------
        usage: tuple
            Tuple of usages of interest under the following format (name, path, values) - the name must be equal to the name of the directory where the simulations are stored

        Returns
        -------
        None

        Example
        -------
        >>> _read_results_per_usage(("mi", "/Sortie_Zone_C/Usage", [1,2]))
        """
        # Create a building object to store/handle the results
        self._buildings_handler = BuildingsHandler()
        # Scan directory where simulations have been run
        dataframe_scanner = pycometh.exp_plan.scan_exp_plan(self._get_right_directory_name(self._output_directory_path + os.sep + usage[0]))
        # Filter only the directories that we want
        dataframe_scanner = dataframe_scanner.query("sub_dir_1 == \"" + self._get_subdirectory_name() + "\"")
        # For each simulation, create a RT2012Sortie object to get the outputs of interest
        for index, row in dataframe_scanner.iterrows():
            # Check if the simulation has run properly
            if row.output_file is not None:
                # Create a RT2012Sortie object
                rt_outputs = pycometh.Rt2012Sortie(row.output_file)
                # Get the outputs of interest (values from xml file associated with a given tag) for this specific file
                self._get_xml_outputs(rt_outputs, row, usage)
                # Update the counter to properly store the results
                self._buildings_handler.update_counter()
        # Store the results per usage
        self._results[usage[0]] = self._buildings_handler.get_buildings_characteristics()

    # Get the xml outputs of interest (values from xml file associated with a given tag)
    def _get_xml_outputs(self, rt_outputs, row, usage):
        """
        Get the xml outputs of interest (values from xml file associated with a given tag)
        -------------------------------------------------------------
        Parameters
        ----------
        rt_outputs: Rt2012Sortie object
            Object containing functions to deal with a particular RT2012 file (which has been run)
        usage: tuple
            Tuple of usages of interest under the following format (name, path, values) - the name must be equal to the name of the directory where the simulations are stored

        Returns
        -------
        None

        Example
        -------
        >>> _get_xml_outputs(rt_outputs, ("mi", "/Sortie_Zone_C/Usage", [1,2]))
        """
        # For each output of interest, get its value in the xml file
        for desired_output in self._desired_building_characteristics:
            if self._get_subdirectory_name() == "run_re2020_D":
                with open(r".//usages.out", 'r') as file_descriptor:
                    usages = [line.split()[0] for line in file_descriptor]
                usages = list(set(usages))
                if str(row.sub_dir_0) not in usages:
                    # Set the directory associated with the simulation under study
                    self._buildings_handler.set_directory_name(str(row.sub_dir_0))
                    # Get the outputs of interest
                    if "post_processing" in desired_output[1]:
                        method_to_call = getattr(rt_outputs.post_processing, desired_output[1].split(".")[1])
                        outputs = method_to_call()
                        outputs = [value for value in outputs.values()]
                    else:
                        outputs = rt_outputs.get_xml_values(desired_output[1])
                    # Reset the counter (counting the total number of buildings)
                    self._buildings_handler.reset_counter()
                    # Get the outputs from xml file under a specific shape
                    self._shape_xml_outputs(outputs, desired_output[0])
            else:
                usages = rt_outputs.get_xml_values(usage[1])
            # Use only single-usage xml file
            if all(element in usage[2] for element in usages):
                # Set the directory associated with the simulation under study
                self._buildings_handler.set_directory_name(str(row.sub_dir_0))
                # Get the outputs of interest
                if "post_processing" in desired_output[1]:
                    method_to_call = getattr(rt_outputs.post_processing, desired_output[1].split(".")[1])
                    outputs = method_to_call()
                    outputs = [value for value in outputs.values()]
                else:
                    outputs = rt_outputs.get_xml_values(desired_output[1])
                # Reset the counter (counting the total number of buildings)
                self._buildings_handler.reset_counter()
                # Get the outputs from xml file under a specific shape
                self._shape_xml_outputs(outputs, desired_output[0])
            else:
                #pass
                with open(r".//usages.out", 'a') as file_descriptor:
                    file_descriptor.write(str(row.sub_dir_0) + "\n")


    # Get the outputs from xml file under a specific shape
    def _shape_xml_outputs(self, outputs, desired_output):
        """
        Get the outputs from xml file under a specific shape
        -------------------------------------------------------------
        Parameters
        ----------
        outputs: list
            List of values (outputs of interest from the xml file)
        desired_output: str
            Name of the desired output

        Returns
        -------
        None

        Example
        -------
        >>> _shape_xml_outputs([44,53], "cep")
        """
        # For each output, store it properly using buildings_handler
        for output in outputs:
            self._buildings_handler.set_building_characteristic(desired_output, output)

    # Get the subdirectory name associated with a specific case study
    def _get_subdirectory_name(self):
        """
        Get the subdirectory name associated with a specific case study
        -------------------------------------------------------------
        Parameters
        ----------
        None

        Returns
        -------
        subdirectory: str
            Subdirectory name associated with a specific case study

        Example
        -------
        >>> _get_subdirectory_name()
        """
        case_study      = self._output_directory_path.split(os.sep)[-2]
        if "2012" in case_study:
            subdirectory = "RT"
        elif "fictive"in case_study:
            subdirectory = "run_re2020_clim_fictive"
        elif "meteo" in case_study:
            subdirectory = "run_re2020_old_meteo"
        elif "mode" in case_study:
            subdirectory = "run_re2020_D"
        else:
            subdirectory = "run_re2020"
        return subdirectory

    # Get the right directory name associated with a specific case study
    def _get_right_directory_name(self, directory_path):
        """
        Get the subdirectory name associated with a specific case study
        -------------------------------------------------------------
        Parameters
        ----------
        directory_path: str
            Path to the directory of interest

        Returns
        -------
        String containing the directory name associated with a specific case study

        Example
        -------
        >>> _get_right_directory_name("..//cases//re2020//outputs//mi")
        """
        directory_path_parts    = directory_path.split(os.sep)
        directory_base          = directory_path_parts[0]
        case_study              = directory_path_parts[1]
        usage                   = directory_path_parts[3]
        #and ("fictive" not in case_study)
        if ("re2020" in case_study):
            case_study = "re2020"
        return directory_base + os.sep + case_study + os.sep + "outputs" + os.sep + usage
