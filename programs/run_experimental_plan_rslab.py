# coding: utf-8


# apply_exp_plan_serie(exp_plan_rt,apply_exp_to_rt,xml_path=xml_path)
if __name__ == '__main__':
    # In[2]:

    from copy import deepcopy

    # Exp_plan_RT


    exp_plan_rt = dict()

    ## global

    from pathlib import Path
    from pycometh.tools import disable_warnings

    disable_warnings()
    root_output_dir = Path(r'F:\rt2012_re2020_comparaisons\cases\re2020\outputs')
    input_dir = Path(r'F:\rt2012_re2020_comparaisons\cases\re2020\inputs')

    run_cfg = {"simu_mode": "BCDE", "write_in_output_dir": True, "engine_version": '8.1.0.0', "timeout": 5 * 60}

    for a_dir in input_dir.iterdir():

        usage = a_dir.name

        for a_file in a_dir.iterdir():
            if 'Comethwrapper' in a_file.name or 'run' in a_file.name:
                a_file.unlink()
        for a_file in a_dir.iterdir():
            ## config by xml

            xml_path = str(a_file.absolute())
            xml_name = a_file.name.split('.')[0]
            xml_prefix = usage + '_' + xml_name
            xml_output_dir = root_output_dir / usage / xml_name

            ## exp 1 run 8.0.0.0

            # #Clim fictive
            exp_run_8000 = dict()
            exp_run_8000['xml_path'] = xml_path
            exp_run_8000['run_cfg'] = {"simu_mode": "C", "write_in_output_dir": True, "engine_version": '8.1.0.0', "timeout": 5 * 60}
            exp_run_8000['run_cfg']['output_dir'] = str((xml_output_dir / 'run_re2020_clim_fictive').absolute())
            exp_plan_rt[xml_prefix + '_run_re2020_clim_fictive'] = deepcopy(exp_run_8000)
            exp_plan_rt[xml_prefix + '_run_re2020_clim_fictive']['pre_processing_rt2012'] = [('set_clim_fictive',)]

            # old meteo
            exp_run_8000 = dict()
            exp_run_8000['xml_path'] = xml_path
            exp_run_8000['run_cfg'] = {"simu_mode": "BCDE", "write_in_output_dir": True, "engine_version": '8.1.0.1', "timeout": 5 * 60}
            exp_run_8000['run_cfg']['output_dir'] = str((xml_output_dir / 'run_re2020_old_meteo').absolute())
            exp_plan_rt[xml_prefix + 'run_re2020_old_meteo'] = deepcopy(exp_run_8000)
            # exp_plan_rt[xml_prefix + 'run_re2020_old_meteo']['pre_processing_rt2012'] = [('set_clim_fictive',)]





    from pycometh.exp_plan import apply_exp_plan_serie, apply_exp_to_rt, mp_apply_exp_plan_by_process

    mp_apply_exp_plan_by_process(exp_plan_rt, func=apply_exp_to_rt, processes=7,
                                 skip_already_run=True, skip_error=True, skip_timeout_rt=True,
                                 func_kwargs={'engine': 'RE2020'})


