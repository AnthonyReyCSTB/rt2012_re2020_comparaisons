# -*- coding: utf-8 -*-
"""
Created on Mon Oct 14 11:35:05 2019

@author: MOREIRA
"""

import rslab as rs
import seaborn as sns
import matplotlib.pyplot as plt
import math
import os
import random
import pandas as pd
import numpy as np
import lxml
from lxml import *
import pycometh as pc
import json
from pkg_resources import resource_filename
from pycometh.xml_objects import Rt2012,Rset,Rt2015,ComethWrapper
from pycometh.tools import reload_package
from pathlib import Path
import statistics
import shutil
import warnings
cn=rs.cn_ope
from xml.etree import cElementTree as et

#tree = ET.parse(r"C:\Users\MOREIRA\Documents\rt2012_re2020_comparaisons\programs\pre_processeur\convert_T5_tags_rt2012_to_re2020\Cas_tests\Xml_RSET_BE16-16709_ETU_001_05-HOME REPUBLIC-IV_PACDS.xml")
#root = tree.getroot()
#
## modifying an attribute
#for elem in root.iter('T5_CSTB_GenerateurThermodynamiqueDoubleService'):
#    elem.tag = 'GenerateurThermodynamiqueDoubleService'
#
#tree.write(r"C:\Users\MOREIRA\Documents\rt2012_re2020_comparaisons\programs\pre_processeur\convert_T5_tags_rt2012_to_re2020\Cas_tests\test.xml")


def convert_tags_T5_rt2012_to_re2020(self):
    root = self.tree.getroot()
    for elem in root.iter('T5_CSTB_GenerateurThermodynamiqueDoubleService'):
        elem.tag = 'Source_Ballon_Base_Thermodynamique_Elec_DoubleService'
    for elem in root.iter('T5_CSTB_GenerateurThermodynamiqueGazDoubleService'):
        elem.tag = 'Source_Ballon_Base_Thermodynamique_AbsoGaz_DoubleService'
    for elem in root.iter('T5_UNICLIMA_PAC_TS'):
        elem.tag = 'Source_Ballon_Base_Thermodynamique_Elec_TripleService'
    for elem in root.iter('T5_Uniclima_Gestion_Appoint_Nuit'):
        elem.tag = 'Gestion_Appoint_Nuit'
    for elem in root.iter('T5_AFPG_Gen_Echangeur_Geocooling'):
        elem.tag = 'Gen_Echangeur_Geocooling'
    for elem in root.iter('T5_AFPG_Geocooling_NonClimatise_Generation'):
        elem.tag = 'Geocooling_NonClimatise_Generation'
    for elem in root.iter('T5_AFPG_Geocooling_NonClimatise_Emission'):
        elem.tag = 'Geocooling_NonClimatise_Emission'
    for elem in root.iter('T5_AFPG_Geocooling_NonClimatise_Emission'):
        elem.tag = 'Geocooling_NonClimatise_Emission'
        
def delete_shon_rt_for_re2020_xml(self):
    root = self.tree.getroot()
    for element in root.iter('Zone'):
        for subelement in element[:]:
            if subelement.tag == 'SHON_RT':
                element.remove(subelement)
                
def set_lower_inertie(self):
    for element_inertie in self.get_in_xml('Inertie', with_path=True):
        path_inertie = element_inertie[1]
        inertie_q = self.get_in_xml(path_inertie + '//' + 'Type_Inertie_Quotidienne', with_value=True)[0][1]
        inertie_s = self.get_in_xml(path_inertie + '//' + 'Type_Inertie_Sequentielle', with_value=True)[0][1]
        inertie_a = self.get_in_xml(path_inertie + '//' + 'Type_Inertie_Annuelle', with_value=True)[0][1]

        q_dict_value = {1: "très légère",
                      2: "légère",
                      3: "moyenne",
                      4: "lourde",
                      5: "très lourde"
                      }

        q_dict_str = {"très légère": (80, 2.5),
                      "légère": (110, 2.5),
                      "moyenne": (165, 2.5),
                      "lourde": (260, 3.0),
                      "très lourde": (370, 3.5)
                      }

        # Inertie quotidienne
        if inertie_q != 0:
            if inertie_q == 1:
                self.set_in_xml(path_inertie + '//' + 'Cmq_surf', q_dict_str["très légère"][0])
                self.set_in_xml(path_inertie + '//' + 'Amq_surf', q_dict_str["très légère"][1])
            if inertie_q == 2:
                self.set_in_xml(path_inertie + '//' + 'Cmq_surf', q_dict_str["légère"][0])
                self.set_in_xml(path_inertie + '//' + 'Amq_surf', q_dict_str["légère"][1])
            if inertie_q == 3:
                self.set_in_xml(path_inertie + '//' + 'Cmq_surf', q_dict_str["moyenne"][0])
                self.set_in_xml(path_inertie + '//' + 'Amq_surf', q_dict_str["moyenne"][1])
            if inertie_q == 4:
                self.set_in_xml(path_inertie + '//' + 'Cmq_surf', q_dict_str["lourde"][0])
                self.set_in_xml(path_inertie + '//' + 'Amq_surf', q_dict_str["lourde"][1])
            if inertie_q == 5:
                self.set_in_xml(path_inertie + '//' + 'Cmq_surf', q_dict_str["très lourde"][0])
                self.set_in_xml(path_inertie + '//' + 'Amq_surf', q_dict_str["très lourde"][1])

            new_inertie_q = 0
            self.set_in_xml(path_inertie + '//' + 'Type_Inertie_Quotidienne', new_inertie_q)

        Amq = self.get_in_xml(path_inertie + '//' + 'Amq_surf', with_value=True)[0][1]
        Cmq = self.get_in_xml(path_inertie + '//' + 'Cmq_surf', with_value=True)[0][1]

        #if Cmq < q_dict_str["très légère"][0]:
         #   self.set_in_xml(path_inertie + '//' + 'Cmq_surf', q_dict_str["légère"][0])
        #if 2*Cmq < q_dict_str["très légère"][0]:
         #   self.set_in_xml(path_inertie + '//' + 'Cmq_surf', q_dict_str["très légère"][0])
        if (Cmq >= q_dict_str["très légère"][0] and Cmq < q_dict_str["légère"][0]):
            None
        if (Cmq >= q_dict_str["légère"][0] and Cmq < q_dict_str["moyenne"][0]):
            self.set_in_xml(path_inertie + '//' + 'Cmq_surf', q_dict_str["très légère"][0])
        if (Cmq >= q_dict_str["moyenne"][0] and Cmq < q_dict_str["lourde"][0]):
            self.set_in_xml(path_inertie + '//' + 'Cmq_surf', q_dict_str["légère"][0])
        if (Cmq >= q_dict_str["lourde"][0] and Cmq < q_dict_str["très lourde"][0]):
            self.set_in_xml(path_inertie + '//' + 'Cmq_surf', q_dict_str["moyenne"][0])
        if (Cmq >= q_dict_str["très lourde"][0] and Cmq < (110 + q_dict_str["très lourde"][0])):
            self.set_in_xml(path_inertie + '//' + 'Cmq_surf', q_dict_str["lourde"][0])
        if Cmq >= (110 + q_dict_str["très lourde"][0]):
            self.set_in_xml(path_inertie + '//' + 'Cmq_surf', q_dict_str["très lourde"][0])

        Cmq = self.get_in_xml(path_inertie + '//' + 'Cmq_surf', with_value=True)[0][1]

        # Inertie séquentielle
        s_dict_value = {1: "défaut",
                        2: "très légère",
                        3: "légère",
                        4: "moyenne",
                        5: "lourde"
                        }

        s_dict_str = {"défaut": (Cmq, Amq),
                      "très légère": (Cmq, Amq),
                      "légère": (260, 2.5),
                      "moyenne": (500, 3),
                      "lourde": (850, 3.5)
                      }

        if inertie_s != 0:
            if inertie_s == 1:
                self.set_in_xml(path_inertie + '//' + 'Cms_surf', s_dict_str["défaut"][0])
                self.set_in_xml(path_inertie + '//' + 'Ams_surf', s_dict_str["défaut"][1])
            if inertie_s == 2:
                self.set_in_xml(path_inertie + '//' + 'Cms_surf', s_dict_str["très légère"][0])
                self.set_in_xml(path_inertie + '//' + 'Ams_surf', s_dict_str["très légère"][1])
            if inertie_s == 3:
                self.set_in_xml(path_inertie + '//' + 'Cms_surf', s_dict_str["légère"][0])
                self.set_in_xml(path_inertie + '//' + 'Ams_surf', s_dict_str["légère"][1])
            if inertie_s == 4:
                self.set_in_xml(path_inertie + '//' + 'Cms_surf', s_dict_str["moyenne"][0])
                self.set_in_xml(path_inertie + '//' + 'Ams_surf', s_dict_str["moyenne"][1])
            if inertie_s == 5:
                self.set_in_xml(path_inertie + '//' + 'Cms_surf', s_dict_str["lourde"][0])
                self.set_in_xml(path_inertie + '//' + 'Ams_surf', s_dict_str["lourde"][1])

            new_inertie_s = 0
            self.set_in_xml(path_inertie + '//' + 'Type_Inertie_Sequentielle', new_inertie_s)

        Cms = self.get_in_xml(path_inertie + '//' + 'Cms_surf', with_value=True)[0][1]
        Ams = self.get_in_xml(path_inertie + '//' + 'Ams_surf', with_value=True)[0][1]

        #if Cms < s_dict_str["très légère"][0]:
         #   self.set_in_xml(path_inertie + '//' + 'Cms_surf', s_dict_str["très légère"][0])
        if (Cms >= s_dict_str["très légère"][0] and Cms < s_dict_str["légère"][0]):
            None
        if (Cms >= s_dict_str["légère"][0] and Cms < s_dict_str["moyenne"][0]):
            self.set_in_xml(path_inertie + '//' + 'Cms_surf', s_dict_str["très légère"][0])
        if (Cms >= s_dict_str["moyenne"][0] and Cms < s_dict_str["lourde"][0]):
            self.set_in_xml(path_inertie + '//' + 'Cms_surf', s_dict_str["légère"][0])
        if (Cms >= s_dict_str["lourde"][0] and Cms < (350 + s_dict_str["lourde"][0])):
            self.set_in_xml(path_inertie + '//' + 'Cms_surf', s_dict_str["moyenne"][0])
        if Cms >= (350 + s_dict_str["lourde"][0]):
            self.set_in_xml(path_inertie + '//' + 'Cms_surf', s_dict_str["lourde"][0])

        Cms = self.get_in_xml(path_inertie + '//' + 'Cms_surf', with_value=True)[0][1]

        # Inertie annuelle
        a_dict_value = {1: "défaut",
                        2: "très légère",
                        3: "légère",
                        4: "moyenne"
                        }

        a_dict_str = {"défaut": (Cms, Ams),
                      "très légère": (Cms, Ams),
                      "légère": (Cms, Ams),
                      "moyenne": (6000, 1)
                      }

        if inertie_a != 0:
            if inertie_a == 1:
                self.set_in_xml(path_inertie + '//' + 'Cma_surf', a_dict_str["défaut"][0])
                self.set_in_xml(path_inertie + '//' + 'Ama_surf', a_dict_str["défaut"][1])
            if inertie_a == 2:
                self.set_in_xml(path_inertie + '//' + 'Cma_surf', a_dict_str["très légère"][0])
                self.set_in_xml(path_inertie + '//' + 'Ama_surf', a_dict_str["très légère"][1])
            if inertie_a == 3:
                self.set_in_xml(path_inertie + '//' + 'Cma_surf', a_dict_str["légère"][0])
                self.set_in_xml(path_inertie + '//' + 'Ama_surf', a_dict_str["légère"][1])
            if inertie_a == 4:
                self.set_in_xml(path_inertie + '//' + 'Cma_surf', a_dict_str["moyenne"][0])
                self.set_in_xml(path_inertie + '//' + 'Ama_surf', a_dict_str["moyenne"][1])

            new_inertie_a = 0
            self.set_in_xml(path_inertie + '//' + 'Type_Inertie_Annuelle', new_inertie_a)

        Cma = self.get_in_xml(path_inertie + '//' + 'Cma_surf', with_value=True)[0][1]
        Ama = self.get_in_xml(path_inertie + '//' + 'Ama_surf', with_value=True)[0][1]

        #if Cma < a_dict_str["moyenne"][0]:
         #   self.set_in_xml(path_inertie + '//' + 'Cma_surf', a_dict_str["très légère"][0])
        if (Cma >= a_dict_str["très légère"][0] and Cma < a_dict_str["légère"][0]):
            None
        if (Cma >= a_dict_str["légère"][0] and Cma < a_dict_str["moyenne"][0]):
            self.set_in_xml(path_inertie + '//' + 'Cma_surf', a_dict_str["très légère"][0])
        if (Cma >= a_dict_str["moyenne"][0] and Cma < (a_dict_str["moyenne"][0] + (6000 - Cms))):
            self.set_in_xml(path_inertie + '//' + 'Cma_surf', a_dict_str["légère"][0])
        if Cma >= (a_dict_str["moyenne"][0] + (6000 - Cms)):
            self.set_in_xml(path_inertie + '//' + 'Cma_surf', a_dict_str["moyenne"][0])

        Cma = self.get_in_xml(path_inertie + '//' + 'Cma_surf', with_value=True)[0][1]

        if Ams > Amq:
            self.set_in_xml(path_inertie + '//' + 'Ams_surf', Amq)
        if Ama > Ams:
            self.set_in_xml(path_inertie + '//' + 'Ama_surf', Ams)
        if Cms < Cmq:
            self.set_in_xml(path_inertie + '//' + 'Cms_surf', Cmq)
        if Cma < Cms:
            self.set_in_xml(path_inertie + '//' + 'Cma_surf', Cms)

def set_higher_inertie(self):
    for element_inertie in self.get_in_xml('Inertie', with_path=True):
        path_inertie = element_inertie[1]
        inertie_q = self.get_in_xml(path_inertie + '//' + 'Type_Inertie_Quotidienne', with_value=True)[0][1]
        inertie_s = self.get_in_xml(path_inertie + '//' + 'Type_Inertie_Sequentielle', with_value=True)[0][1]
        inertie_a = self.get_in_xml(path_inertie + '//' + 'Type_Inertie_Annuelle', with_value=True)[0][1]

        q_dict_value = {1: "très légère",
                        2: "légère",
                        3: "moyenne",
                        4: "lourde",
                        5: "très lourde"
                        }

        q_dict_str = {"très légère": (80, 2.5),
                      "légère": (110, 2.5),
                      "moyenne": (165, 2.5),
                      "lourde": (260, 3.0),
                      "très lourde": (370, 3.5)
                      }

        # Inertie quotidienne
        if inertie_q != 0:
            if inertie_q == 1:
                self.set_in_xml(path_inertie + '//' + 'Cmq_surf', q_dict_str["très légère"][0])
                self.set_in_xml(path_inertie + '//' + 'Amq_surf', q_dict_str["très légère"][1])
            if inertie_q == 2:
                self.set_in_xml(path_inertie + '//' + 'Cmq_surf', q_dict_str["légère"][0])
                self.set_in_xml(path_inertie + '//' + 'Amq_surf', q_dict_str["légère"][1])
            if inertie_q == 3:
                self.set_in_xml(path_inertie + '//' + 'Cmq_surf', q_dict_str["moyenne"][0])
                self.set_in_xml(path_inertie + '//' + 'Amq_surf', q_dict_str["moyenne"][1])
            if inertie_q == 4:
                self.set_in_xml(path_inertie + '//' + 'Cmq_surf', q_dict_str["lourde"][0])
                self.set_in_xml(path_inertie + '//' + 'Amq_surf', q_dict_str["lourde"][1])
            if inertie_q == 5:
                self.set_in_xml(path_inertie + '//' + 'Cmq_surf', q_dict_str["très lourde"][0])
                self.set_in_xml(path_inertie + '//' + 'Amq_surf', q_dict_str["très lourde"][1])

            new_inertie_q = 0
            self.set_in_xml(path_inertie + '//' + 'Type_Inertie_Quotidienne', new_inertie_q)

        Cmq = self.get_in_xml(path_inertie + '//' + 'Cmq_surf', with_value=True)[0][1]
        Amq = self.get_in_xml(path_inertie + '//' + 'Amq_surf', with_value=True)[0][1]

        if Cmq < (q_dict_str["très légère"][0] - 30):
            self.set_in_xml(path_inertie + '//' + 'Cmq_surf', q_dict_str["très légère"][0])
        if (Cmq >= (q_dict_str["très légère"][0] - 30) and Cmq < q_dict_str["très légère"][0]):
            self.set_in_xml(path_inertie + '//' + 'Cmq_surf', q_dict_str["légère"][0])
        if (Cmq >= q_dict_str["très légère"][0] and Cmq < q_dict_str["légère"][0]):
            self.set_in_xml(path_inertie + '//' + 'Cmq_surf', q_dict_str["légère"][0])
        if (Cmq >= q_dict_str["légère"][0] and Cmq < q_dict_str["moyenne"][0]):
            self.set_in_xml(path_inertie + '//' + 'Cmq_surf', q_dict_str["moyenne"][0])
        if (Cmq >= q_dict_str["moyenne"][0] and Cmq < q_dict_str["lourde"][0]):
            self.set_in_xml(path_inertie + '//' + 'Cmq_surf', q_dict_str["lourde"][0])
        if (Cmq >= q_dict_str["lourde"][0] and Cmq < q_dict_str["très lourde"][0]):
            self.set_in_xml(path_inertie + '//' + 'Cmq_surf', q_dict_str["très lourde"][0])
        if Cmq >= q_dict_str["très lourde"][0]:
            None
        #if new_Cmq >= 2*q_dict_str["très lourde"][0]:
         #   self.set_in_xml(path_inertie + '//' + 'Cmq_surf', q_dict_str["très lourde"][0])

        Cmq = self.get_in_xml(path_inertie + '//' + 'Cmq_surf', with_value=True)[0][1]

        # Inertie séquentielle
        s_dict_value = {1: "défaut",
                        2: "très légère",
                        3: "légère",
                        4: "moyenne",
                        5: "lourde"
                        }

        s_dict_str = {"défaut": (Cmq, Amq),
                      "très légère": (Cmq, Amq),
                      "légère": (260, 2.5),
                      "moyenne": (500, 3),
                      "lourde": (850, 3.5)}

        if inertie_s != 0:
            if inertie_s == 1:
                self.set_in_xml(path_inertie + '//' + 'Cms_surf', s_dict_str["défaut"][0])
                self.set_in_xml(path_inertie + '//' + 'Ams_surf', s_dict_str["défaut"][1])
            if inertie_s == 2:
                self.set_in_xml(path_inertie + '//' + 'Cms_surf', s_dict_str["très légère"][0])
                self.set_in_xml(path_inertie + '//' + 'Ams_surf', s_dict_str["très légère"][1])
            if inertie_s == 3:
                self.set_in_xml(path_inertie + '//' + 'Cms_surf', s_dict_str["légère"][0])
                self.set_in_xml(path_inertie + '//' + 'Ams_surf', s_dict_str["légère"][1])
            if inertie_s == 4:
                self.set_in_xml(path_inertie + '//' + 'Cms_surf', s_dict_str["moyenne"][0])
                self.set_in_xml(path_inertie + '//' + 'Ams_surf', s_dict_str["moyenne"][1])
            if inertie_s == 5:
                self.set_in_xml(path_inertie + '//' + 'Cms_surf', s_dict_str["lourde"][0])
                self.set_in_xml(path_inertie + '//' + 'Ams_surf', s_dict_str["lourde"][1])

            new_inertie_s = 0
            self.set_in_xml(path_inertie + '//' + 'Type_Inertie_Sequentielle', new_inertie_s)

        Cms = self.get_in_xml(path_inertie + '//' + 'Cms_surf', with_value=True)[0][1]
        Ams = self.get_in_xml(path_inertie + '//' + 'Ams_surf', with_value=True)[0][1]

        if Cms < (s_dict_str["très légère"][0] - (260 - Cmq)):
            self.set_in_xml(path_inertie + '//' + 'Cms_surf', s_dict_str["très légère"][0])
        if (Cms >= (s_dict_str["très légère"][0] - (260 - Cmq)) and Cms < s_dict_str["très légère"][0]):
            self.set_in_xml(path_inertie + '//' + 'Cms_surf', s_dict_str["légère"][0])
        if (Cms >= s_dict_str["très légère"][0] and Cms < s_dict_str["légère"][0]):
            self.set_in_xml(path_inertie + '//' + 'Cms_surf', s_dict_str["légère"][0])
        if (Cms >= s_dict_str["légère"][0] and Cms < s_dict_str["moyenne"][0]):
            self.set_in_xml(path_inertie + '//' + 'Cms_surf', s_dict_str["moyenne"][0])
        if (Cms >= s_dict_str["moyenne"][0] and Cms < s_dict_str["lourde"][0]):
            self.set_in_xml(path_inertie + '//' + 'Cms_surf', s_dict_str["lourde"][0])
        if Cms >= s_dict_str["lourde"][0]:
            None
        #if new_Cms >= 2*s_dict_str["lourde"][0]:
         #   self.set_in_xml(path_inertie + '//' + 'Cms_surf', s_dict_str["lourde"][0])

        Cms = self.get_in_xml(path_inertie + '//' + 'Cms_surf', with_value=True)[0][1]

        # Inertie annuelle
        a_dict_value = {1: "défaut",
                        2: "très légère",
                        3: "légère",
                        4: "moyenne"
                        }

        a_dict_str = {"défaut": (Cms, Ams),
                      "très légère": (Cms, Ams),
                      "légère": (Cms, Ams),
                      "moyenne": (6000, 1)
                      }

        if inertie_a != 0:
            if inertie_a == 1:
                self.set_in_xml(path_inertie + '//' + 'Cma_surf', a_dict_str["défaut"][0])
                self.set_in_xml(path_inertie + '//' + 'Ama_surf', a_dict_str["défaut"][1])
            if inertie_a == 2:
                self.set_in_xml(path_inertie + '//' + 'Cma_surf', a_dict_str["très légère"][0])
                self.set_in_xml(path_inertie + '//' + 'Ama_surf', a_dict_str["très légère"][1])
            if inertie_a == 3:
                self.set_in_xml(path_inertie + '//' + 'Cma_surf', a_dict_str["légère"][0])
                self.set_in_xml(path_inertie + '//' + 'Ama_surf', a_dict_str["légère"][1])
            if inertie_a == 4:
                self.set_in_xml(path_inertie + '//' + 'Cma_surf', a_dict_str["moyenne"][0])
                self.set_in_xml(path_inertie + '//' + 'Ama_surf', a_dict_str["moyenne"][1])

            new_inertie_a = 0
            self.set_in_xml(path_inertie + '//' + 'Type_Inertie_Annuelle', new_inertie_a)

        Cma = self.get_in_xml(path_inertie + '//' + 'Cma_surf', with_value=True)[0][1]
        Ama = self.get_in_xml(path_inertie + '//' + 'Ama_surf', with_value=True)[0][1]

        if Cma < (a_dict_str["très légère"][0]/2):
            self.set_in_xml(path_inertie + '//' + 'Cma_surf', a_dict_str["très légère"][0])
        if (Cma >= a_dict_str["très légère"][0] and Cma < a_dict_str["légère"][0]):
            self.set_in_xml(path_inertie + '//' + 'Cma_surf', a_dict_str["légère"][0])
        if (Cma >= a_dict_str["légère"][0] and Cma < a_dict_str["moyenne"][0]):
            self.set_in_xml(path_inertie + '//' + 'Cma_surf', a_dict_str["moyenne"][0])
        if Cma >= a_dict_str["moyenne"][0]:
            None
        #if new_Cma >= 2*a_dict_str["moyenne"][0]:
         #   self.set_in_xml(path_inertie + '//' + 'Cma_surf', a_dict_str["moyenne"][0])

        Cma = self.get_in_xml(path_inertie + '//' + 'Cma_surf', with_value=True)[0][1]

        if Ams > Amq:
            self.set_in_xml(path_inertie + '//' + 'Ams_surf', Amq)
        if Ama > Ams:
            self.set_in_xml(path_inertie + '//' + 'Ama_surf', Ams)
        if Cms < Cmq:
            self.set_in_xml(path_inertie + '//' + 'Cms_surf', Cmq)
        if Cma < Cms:
            self.set_in_xml(path_inertie + '//' + 'Cma_surf', Cms)
            
            
def convert_tags_for_re2020(self):
    for baie in self.get_in_xml('Baie', with_path=True):
        self.set_in_xml(baie[1] + '/Type_Baie', value=2)
        self.set_in_xml(baie[1] + '/Type_volume', value=1)

    temp_ecs_gen = self.get_in_xml('Theta_Wm_Ecs', with_path=True, with_value=True)
    for temp in temp_ecs_gen:
        if temp[2] < 48:
            self.set_in_xml(temp[1], value=50)

    root = self.tree.getroot()
    for element in root.iter('Zone'):
        for subelement in element[:]:
            if subelement.tag == 'SHON_RT':
                element.remove(subelement)

    if self.get_in_xml('Type_Inertie_Quotidienne') == []:
        for element in root.iter('Inertie'):
            for subelement in element[:]:
                if subelement.tag == 'Type_Inertie':
                    element.remove(subelement)
        self.set_in_xml('/Inertie/Type_Inertie_Quotidienne', value=0)

    if self.get_in_xml('Amq_surf') == []:
        for element in root.iter('Inertie'):
            for subelement in element[:]:
                if subelement.tag == 'Am_surf':
                    element.remove(subelement)
        self.set_in_xml('/Inertie/Amq_surf', value=1500)

    if self.get_in_xml('Cmq_surf') == []:
        for element in root.iter('Inertie'):
            for subelement in element[:]:
                if subelement.tag == 'Cm_surf':
                    element.remove(subelement)
        self.set_in_xml('/Inertie/Cmq_surf', value=20000)

    if self.get_in_xml('Ams_surf') == []:
        self.set_in_xml('/Inertie/Ams_surf', value=1500)

    if self.get_in_xml('Type_Inertie_Annuelle') == []:
        self.set_in_xml('/Inertie/Type_Inertie_Annuelle', value= 1)
        self.set_in_xml('/Inertie/Ama_surf', value=1500)
        self.set_in_xml('/Inertie/Cma_surf', value=20000)

#xml.get_in_xml('/Groupe_Collection/Groupe')
#Out[56]: 
#[<Element Groupe at 0x209d185e808>,
# <Element Groupe at 0x209d18dda48>,
# <Element Groupe at 0x209d14f5188>,
# <Element Groupe at 0x209d185e908>]
#
#xml.get_in_xml('/Groupe_Collection/Groupe',with_path=True)
#Out[57]: 
#[(<Element Groupe at 0x209d185e808>,
#  '/Projet/Batiment_Collection/Batiment/Zone_Collection/Zone[1]/Groupe_Collection/Groupe'),
# (<Element Groupe at 0x209d18dda48>,
#  '/Projet/Batiment_Collection/Batiment/Zone_Collection/Zone[2]/Groupe_Collection/Groupe'),
# (<Element Groupe at 0x209d14f5188>,
#  '/Projet/Batiment_Collection/Batiment/Zone_Collection/Zone[3]/Groupe_Collection/Groupe[1]'),
# (<Element Groupe at 0x209d185e908>,
#  '/Projet/Batiment_Collection/Batiment/Zone_Collection/Zone[3]/Groupe_Collection/Groupe[2]')]
#
#xml.get_in_xml('Inertie',with_path=True)
#Out[58]: 
#[(<Element Inertie at 0x209d1864688>,
#  '/Projet/Batiment_Collection/Batiment/Zone_Collection/Zone[1]/Groupe_Collection/Groupe/Inertie'),
# (<Element Inertie at 0x209d18641c8>,
#  '/Projet/Batiment_Collection/Batiment/Zone_Collection/Zone[2]/Groupe_Collection/Groupe/Inertie'),
# (<Element Inertie at 0x209d1864908>,
#  '/Projet/Batiment_Collection/Batiment/Zone_Collection/Zone[3]/Groupe_Collection/Groupe[1]/Inertie'),
# (<Element Inertie at 0x209d1864e48>,
#  '/Projet/Batiment_Collection/Batiment/Zone_Collection/Zone[3]/Groupe_Collection/Groupe[2]/Inertie')]
#
#toto = xml.get_in_xml('Inertie',with_path=True)
#
#toto[0][0]
#Out[60]: <Element Inertie at 0x209d1864688>
#
#toto[0][1]
#Out[61]: '/Projet/Batiment_Collection/Batiment/Zone_Collection/Zone[1]/Groupe_Collection/Groupe/Inertie'
#
#xml.get_in_xml(toto[0][1]+'//'+'Amq_surf',with_value=True)
#Out[62]: [(<Element Amq_surf at 0x209d1860c08>, 2.5)]
                
                
# Programme test des fonctions inertie avec inertie =/= 0
dict_comparaison = {}
l=[]
tags = ['Type_Inertie_Quotidienne', 'Type_Inertie_Sequentielle', 'Type_Inertie_Annuelle', 'Cmq_surf', 'Amq_surf', 'Cms_surf', 'Ams_surf', 'Cma_surf', 'Ama_surf']
self=pc.load_xml_object(r"C:\Users\MOREIRA\Documents\rt2012_re2020_comparaisons\programs\pre_processeur\convert_rt2012_to_re2020\Cas_tests\plus_moins_1_classe_inertie\U22W2012v71BugECSGROUPE_Rt2012_multizone_re2020_avec_inertie_annuelle.xml")
for tag in tags:
    l.append('{} : {}'.format(tag, self.get_xml_values(tag)))
dict_comparaison['self'] = l

l=[]
self_lower_inertie = set_lower_inertie(self)
self_lower_inertie.write(r"C:\Users\MOREIRA\Documents\rt2012_re2020_comparaisons\programs\pre_processeur\convert_rt2012_to_re2020\Cas_tests\plus_moins_1_classe_inertie\multizone_re2020_lower_inertie.xml")
for tag in tags:
    l.append('{} : {}'.format(tag, self_lower_inertie.get_xml_values(tag)))
dict_comparaison['self_lower_inertie'] = l

l=[]
self=pc.load_xml_object(r"C:\Users\MOREIRA\Documents\rt2012_re2020_comparaisons\programs\pre_processeur\convert_rt2012_to_re2020\Cas_tests\plus_moins_1_classe_inertie\U22W2012v71BugECSGROUPE_Rt2012_multizone_re2020_avec_inertie_annuelle.xml")
self_higher_inertie = set_higher_inertie(self)
self_higher_inertie.write(r"C:\Users\MOREIRA\Documents\rt2012_re2020_comparaisons\programs\pre_processeur\convert_rt2012_to_re2020\Cas_tests\plus_moins_1_classe_inertie\multizone_re2020_higer_inertie.xml")
for tag in tags:
    l.append('{} : {}'.format(tag, self_higher_inertie.get_xml_values(tag)))
dict_comparaison['self_higher_inertie'] = l

# Programme test des fonctions inertie avec inertie == 0
dict_comparaison = {}
l=[]
tags = ['Type_Inertie_Quotidienne', 'Type_Inertie_Sequentielle', 'Type_Inertie_Annuelle', 'Cmq_surf', 'Amq_surf', 'Cms_surf', 'Ams_surf', 'Cma_surf', 'Ama_surf']
self=pc.load_xml_object(r"C:\Users\MOREIRA\Documents\rt2012_re2020_comparaisons\programs\pre_processeur\convert_rt2012_to_re2020\Cas_tests\plus_moins_1_classe_inertie\U22W2012v71BugECSGROUPE_Rt2012_multizone_re2020_avec_inertie_annuelle_inertie_perso.xml")
for tag in tags:
    l.append('{} : {}'.format(tag, self.get_xml_values(tag)))
dict_comparaison['self'] = l

l=[]
self_lower_inertie = set_lower_inertie(self)
self_lower_inertie.write(r"C:\Users\MOREIRA\Documents\rt2012_re2020_comparaisons\programs\pre_processeur\convert_rt2012_to_re2020\Cas_tests\plus_moins_1_classe_inertie\multizone_re2020_inertie_perso_lower_inertie.xml")
for tag in tags:
    l.append('{} : {}'.format(tag, self_lower_inertie.get_xml_values(tag)))
dict_comparaison['self_lower_inertie'] = l

l=[]
self=pc.load_xml_object(r"C:\Users\MOREIRA\Documents\rt2012_re2020_comparaisons\programs\pre_processeur\convert_rt2012_to_re2020\Cas_tests\plus_moins_1_classe_inertie\U22W2012v71BugECSGROUPE_Rt2012_multizone_re2020_avec_inertie_annuelle_inertie_perso.xml")
self_higher_inertie = set_higher_inertie(self)
self_higher_inertie.write(r"C:\Users\MOREIRA\Documents\rt2012_re2020_comparaisons\programs\pre_processeur\convert_rt2012_to_re2020\Cas_tests\plus_moins_1_classe_inertie\multizone_re2020_inertie_perso_higher_inertie.xml")
for tag in tags:
    l.append('{} : {}'.format(tag, self_higher_inertie.get_xml_values(tag)))
dict_comparaison['self_higher_inertie'] = l

