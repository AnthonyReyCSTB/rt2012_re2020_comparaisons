# -*- coding: utf-8 -*-
"""
Created on Wed Oct  9 17:38:33 2019

@author: MOREIRA
"""

# ========================================
# Packages
# ========================================

# Python core packages/libraries/modules
import rslab as rs
from rslab import analysis_tools
import seaborn as sns
import matplotlib.pyplot as plt
import math
import os
import random
from random import *
import pandas as pd
import numpy as np
from lxml import etree, objectify
import pycometh as pc
import json
from pkg_resources import resource_filename
from pycometh.xml_objects import Rt2012,Rset,Rt2015,ComethWrapper,load_xml_object
from pycometh.tools import reload_package
from pycometh.tools import download_cometh_engines,force_download_cometh_engines
from pathlib import Path
import statistics
import shutil
import warnings
cn=rs.cn_ope
cn_opebn = rs.cn_opebn
df_opebn=rs.cn_opebn.valid_opebn()

try:
    import os
except ImportError:
    raise ImportError("Sorry, but the \"os\" module/package is required to run this program!")
try:
    import operator
except ImportError:
    raise ImportError("Sorry, but the \"operator\" module/package is required to run this program!")
try:
    import pandas as pd
except ImportError:
    raise ImportError("Sorry, but the \"pandas\" module/package is required to run this program!")
try:
    import numpy as np
except ImportError:
    raise ImportError("Sorry, but the \"numpy\" module/package is required to run this program!")
try:
    import matplotlib
except ImportError:
    raise ImportError("Sorry, but the \"matplotlib\" module/package is required to run this program!")
try:
    import matplotlib.pyplot as plt
except ImportError:
    raise ImportError("Sorry, but the \"matplotlib\" module/package is required to run this program!")
try:
    import seaborn
except ImportError:
    raise ImportError("Sorry, but the \"seaborn\" module/package is required to run this program!")
try:
    import copy
except ImportError:
    raise ImportError("Sorry, but the \"copy\" module/package is required to run this program!")
try:
    import collections
except ImportError:
    raise ImportError("Sorry, but the \"collections\" module/package is required to run this program!")

# Python extension packages/libraries/modules

# Set the figure to the default style
matplotlib.style.use("classic")

# ========================================
# Classes
# ========================================

i=0
k=0
tables_plus_fields=[]
fields=cn._fields
tables=cn._tables
while i <= len(tables)-1:
        tables_plus_fields.append("Table : "+tables[i])
        for j in range(0,len(cn.find(tables[i]))):
            tables_plus_fields.append("   Field : "+cn.find(tables[i])[j])
        i=i+1
        
        
# Fonction permettant de filtrer avec une certaine valeur de quantile les valeurs extremes d'un dataframe
        
def filter_df_with_quantiles(df, column, percent):
    quantile_min = percent
    quantile_max = 1 - percent
    quantiles = df.quantile([quantile_min, quantile_max])
    
    if column == "":
        for col in quantiles.columns:
            min_max = quantiles[col].values
            value_min = min_max[0]
            value_max = min_max[1]
            mask = (df[col] >= value_min) & (df[col] <= value_max)
            df = df.loc[mask]
    
    else:
        min_max = quantiles[column].values
        value_min = min_max[0]
        value_max = min_max[1]
        mask = (df[column] >= value_min) & (df[column] <= value_max)
        df = df.loc[mask]
            
    return df

def unique_usage(df):
    df_u=df.unique()
    if len(df_u)>1:
        return False
    else:
        return df_u[0]
    
def tri_usage_batiments(df_init, usage_principal, usage_zone):
    usage_principal_to_usage = {}
    res_int_usage = {}
    res_final = {}
    columns = df_init.columns
    df_all = pd.DataFrame()
     
    if usage_principal == 1:
        usage_principal_to_usage[usage_principal] = [1]
    if usage_principal == 2:
        usage_principal_to_usage[usage_principal] = [1]
    if usage_principal == 3:
        usage_principal_to_usage[usage_principal] = [2]
    if usage_principal == 4:
        usage_principal_to_usage[usage_principal] = [2, 16, 22]
    if usage_principal == 5:
        usage_principal_to_usage[usage_principal] = [4, 5, 6, 7]
    if usage_principal == 6:
        usage_principal_to_usage[usage_principal] = [8, 10, 11, 12, 13, 14, 15]
    if usage_principal == 7:
        usage_principal_to_usage[usage_principal] = [16]
    if usage_principal == 8:
        usage_principal_to_usage[usage_principal] = [17, 18, 19, 20, 37, 38]
    if usage_principal == 9:
        usage_principal_to_usage[usage_principal] = [22]
    if usage_principal == 10:
        usage_principal_to_usage[usage_principal] = [26, 27, 28]
    if usage_principal == 11:
        usage_principal_to_usage[usage_principal] = [29]
    if usage_principal == 12:
        usage_principal_to_usage[usage_principal] = [24, 36]
    if usage_principal == 13:
        usage_principal_to_usage[usage_principal] = [32, 33, 34]
    
    for col in df_init.columns:
        if 'usage_princ' in col:
            col_ref = col
            
    df = df_init.loc[df_init[col_ref] == usage_principal]
    
    if len(df) == 0:
        return "L'usage principal {} ({}) n'est pas représenté dans ce dataframe".format(cn.value_to_meaning('usage_principal', usage_principal), usage_principal)
    
    else:
        if type(usage_zone) == list:
            for usage in usage_principal_to_usage[usage_principal]:
                if usage in usage_zone:
                    df_unique = df.groupby('batiments.id')['zones.usage'].apply(lambda x: unique_usage(x))
                    df_unique_false = df_unique.loc[df_unique != usage]
                    bat_to_delete = df_unique_false.index
                    df_index_bat = df.set_index('batiments.id')
                    df_filter = df_index_bat.drop(index = bat_to_delete)
                    df_filter_f = df_filter.reset_index()
                    res_int_usage[usage] = df_filter_f
                    df_all = pd.concat([df_all,res_int_usage[usage]], axis=0, ignore_index=True)
                else:
                    continue
            
            res_final[usage_principal] = df_all
            df_all_vf = res_final[usage_principal]
            
            return df_all_vf
            
        else:
            for usage in usage_principal_to_usage[usage_principal]:
                df_unique = df.groupby('batiments.id')['zones.usage'].apply(lambda x: unique_usage(x))
                df_unique_false = df_unique.loc[df_unique != usage]
                bat_to_delete = df_unique_false.index
                df_index_bat = df.set_index('batiments.id')
                df_filter = df_index_bat.drop(index = bat_to_delete)
                df_filter_f = df_filter.reset_index()
                res_int_usage[usage] = df_filter_f
            
            res_final[usage_principal] = res_int_usage
        
            df_final = res_final[usage_principal][usage_zone]
        
            return df_final

# Initialize the global variable "NUMBER_OF_GRAPHS" to avoid overlapping
NUMBER_OF_GRAPHS = 0


# Class used for plotting data more easily
class PlotHandler:
    """
    Class used for plotting data more easily
    ...
    Constants
    ---------
    None

    Class attributes
    ----------------
    None

    Instance attributes
    -------------------
    None

    Methods
    -------
    display_violin_plot(self, figure_name, given_function, indicators, vertical_axis_name, horizontal_axis_name, axes = None, tick_labels = None, occurrences = False):
        Display a violin plot figure (and save it at the predefined location)
    display_scatter_plot(self, figure_name, indicators, vertical_axis_name, horizontal_axis_name, axes = None, tick_labels = None):
        Display scatter plot figure (and save it at the predefined location)
    """

    # Initialize (construct) a PlotHandler object
    def __init__(self, dataframe, usages, systems, figures_path):
        """
        Initialize (construct) a PlotHandler object
        -------------------------------------------------------------
        Parameters
        ----------
        dataframe: pandas.DataFrame
            Dataframe containing the data
        usages: list
            List of conditions on the usages
        systems: list
            List of conditions on the systems
        figures_path: str
            Path to the directory where the figure will be saved

        Returns
        -------
        PlotHandler object

        Example
        -------
        >>> PlotHandler(dataframe, ["mi", "lc"], [], ".//data//")
        """
        # Copy dataframe(in case we could need the initial one later)
        self.dataframe_temporary = copy.deepcopy(dataframe)
        # Store initial dataframe in memory (just in case)
        self.dataframe           = dataframe
        # Save other parameters
        self.usages              = usages
        self.systems             = systems
        self.figures_path        = figures_path

    # Display a violin plot figure (and save it at the predefined location)
    def display_violin_plot(self, figure_name, given_function, indicators, vertical_axis_name, horizontal_axis_name, axes = None, tick_labels = None, show_occurrences = False):
        """
        Display a violin plot figure (and save it at the predefined location)
        -------------------------------------------------------------
        Parameters
        ----------
        figure_name: str
            Name of figure
        given_function: function
            Function to build the dictionary (later converted to a dataframe) used for the violin plot
        indicators: list
            List of indicators for the horizontal and vertical axes
        vertical_axis_name: str
            Name of the vertical axis
        horizontal_axis_name: str
            Name of the horizontal axis
        axes: list (optional = None)
            List of tuples of values representing the (vertical) axes and ticks [(min,max), (min, max + step, step)]
        tick_labels: list (optional = None)
            List of names for the (vertical) tick labels
        show_occurrences: bool (optional = False)
            Tell whether or not the number of cases (occurrences) should be displayed in the figure

        Returns
        -------
        None

        Example
        -------
        >>> display_violin_plot(["system", "bbio"], "Typologies [-]", "Bbio [-]")
        """
        # Apply the filters previously defined
        self._apply_filters()
        # Build a dataframe for the figure based on a given function
        dataframe = self._build_dictionary_for_figure(given_function, indicators)
        # Generate figure
        self._generate_figure(dataframe, figure_name, given_function, indicators, vertical_axis_name, horizontal_axis_name, axes, tick_labels, show_occurrences)

    # Display scatter plot figure (and save it at the predefined location)
    def display_scatter_plot(self, figure_name, indicators, vertical_axis_name, horizontal_axis_name, axes = None, tick_labels = None):
            """
            Display scatter plot figure (and save it at the predefined location)
            -------------------------------------------------------------
            Parameters
            ----------
            figure_name: str
                Name of figure
                Function to build the dictionary (later converted to a dataframe) used for the violin plot
            indicators: list
                List of indicators for the horizontal and vertical axes
            vertical_axis_name: str
                Name of the vertical axis
            horizontal_axis_name: str
                Name of the horizontal axis
            axes: list (optional = None)
                List of tuples of values representing the (vertical) axes and ticks [(min,max), (min, max + step, step)]
            tick_labels: list (optional = None)
                List of names for the (vertical) tick labels

            Returns
            -------
            None

            Example
            -------
            >>> display_scatter_plot(["system", "bbio"], "Typologies [-]", "Bbio [-]")
            """
            # Apply the filters previously defined
            self._apply_filters()
            # Generate figure
            self._generate_figure(None, figure_name, None, indicators, vertical_axis_name, horizontal_axis_name, axes, tick_labels, False)

    # Apply the filters previously defined
    def _apply_filters(self):
        """
        Apply the filters previously defined
        -------------------------------------------------------------
        Parameters
        ----------
        None

        Returns
        -------
        None

        Example
        -------
        >>> _apply_filters()
        """
        # Filter dataframe based on usages and systems
        self._filtered_dataframe(self.usages)
        self._filtered_dataframe(self.systems)

    # Get the number of occurrences for each type of series
    def _get_number_of_occurrences(self, given_function, indicators):
        """
        Get the number of occurrences for each type of series
        -------------------------------------------------------------
        Parameters
        ----------
        given_function: function
            Function taking a dataframe and indicators as arguments then returning a dictionary
        indicators: list
            List of indicators for the horizontal and vertical axes

        Returns
        -------
        A dictionary with each type of series (as a key) and its length (as a value)

        Example
        -------
        >>> _get_number_of_occurrences(first_function,["system", "bbio"])
        """
        # Build a dataframe for the figure based on a given function
        dataframe = self._build_dictionary_for_figure(given_function, indicators)
        # Return a dictionary with each type of series (as a key) and its length (as a value)
        return {key:len(dataframe[key]) for key in dataframe.keys()}

    # Filter a dataframe based on some conditions (list of conditions)
    def _filtered_dataframe(self, conditions):
        """
        Filter a dataframe based on some conditions (list of conditions)
        -------------------------------------------------------------
        Parameters
        ----------
        conditions: list
            List of tuples, each containing a condition in the following format : (condition, operator, value)

        Returns
        -------
        None

        Example
        -------
        >>> _filtered_dataframe([("usage", "==", "mi")])
        """
        if conditions:
            # Filter the dataframe by usage (keep only the desired ones)
            mask = np.logical_or.reduce(
                [self._get_operator(condition[1])(self.dataframe_temporary[condition[0]], condition[2]) for condition in
                 conditions])
            # Keep only rows which satisfy the conditions
            self.dataframe_temporary = self.dataframe_temporary[mask]

    # Return a python operator based on some string operator
    def _get_operator(self, operator_string):
        """
        Return a python operator based on some string operator
        -------------------------------------------------------------
        Parameters
        ----------
        operator_string: str
            Operator as a string (e.g., "==")

        Returns
        -------
        An operator object (e.g., operator.eq)

        Example
        -------
        >>> _get_operator("==")
        """
        # Dictionary of operators
        dictionary_of_operators = {"<=": operator.lt,
                                   "==": operator.eq}
        # Return the desired operator
        return dictionary_of_operators[operator_string]

    # Build a violing plot
    def _build_dictionary_for_figure(self, given_function, indicators):
        """
        Generate a violing plot
        -------------------------------------------------------------
        Parameters
        ----------
        given_function: function
            Function taking a dataframe and indicators as arguments then returning a dictionary
        indicators: list
            List of indicators for the horizontal and vertical axes

        Returns
        -------
        dictionary: dict
            Dictionary containing the data to be plotted (each key is an element on the x-axis)

        Example
        -------
        >>> _generate_figure("cep_tous_les_usages", ["system", "bbio"], "Systèmes [-]", "Bbio [-]", [(0, 275), (0, 300, 25)], ["mi", "lc"])
        """
        return given_function(self.dataframe_temporary, indicators)

    # Generate a violing plot
    def _generate_figure(self, dictionary, figure_name, given_function, indicators, vertical_axis_name, horizontal_axis_name, axes, tick_labels, show_occurrences):
        """
        Generate a violing plot
        -------------------------------------------------------------
        Parameters
        ----------
        dictionary: dict
            Dictionary containing the data to be plotted (each key is an element on the x-axis)
        figure_name: str
            Name of the figure
        indicators: list
            List of indicators for the horizontal and vertical axes
        vertical_axis_name: str
            Name of the vertical axis
        horizontal_axis_name: str
            Name of the horizontal axis
        axes: list
            List of tuples of values representing the (vertical) axes and ticks [(min,max), (min, max + step, step)]
        tick_labels: list
            List of names for the (vertical) tick labels
        show_occurrences: bool (optional = False)
            Tell whether or not the number of cases (occurrences) should be displayed in the figure

        Returns
        -------
        None

        Example
        -------
        >>> _generate_figure(dictionary, "cep_tous_les_usages", ["system", "bbio"], "Systèmes [-]", "Bbio [-]", [(0, 275), (0, 300, 25)], ["mi", "lc"], True)
        """
        # Use the global variable "NUMBER_OF_GRAPHS"
        global NUMBER_OF_GRAPHS
        # Figure
        figure = plt.figure(num = NUMBER_OF_GRAPHS, figsize = (12.5, 9.5), facecolor = 'w', edgecolor = 'k')
        # Set up axis
        axis = figure.add_subplot(111)  # number of rows, number of columns, and plot number
        if dictionary is None:
            pass
        else:
            # Generate violin plots
            seaborn.violinplot(data = pd.DataFrame(dictionary), cut = 0)
        # Define the axes if not equal to None
        if axes is not None:
            axis.set_ylim(axes[0][0], axes[0][1])
            axis.set_yticks(np.arange(axes[1][0], axes[1][1], axes[1][2]))
        # Compute the number of occurrences for each case
        number_of_occurrences = self._get_number_of_occurrences(given_function, indicators)
        # Define the tick labels if not equal to None
        if tick_labels is not None:
            if show_occurrences is True:
                tick_labels = [tick_label + "\n" + "(cas : " + str(list(number_of_occurrences.values())[index]) + ")" for index, tick_label in enumerate(tick_labels)]
                axis.set_xticklabels(tick_labels)
            else:
                axis.set_xticklabels(tick_labels)
        else:
            if show_occurrences is True:
                tick_labels = [tick_label + "\n" + "(cas : " + str(list(number_of_occurrences.values())[index]) + ")" for index, tick_label in enumerate(dictionary.keys())]
                axis.set_xticklabels(tick_labels)

        # Set the labels for each axis (with its font size)
        axis.set_xlabel(vertical_axis_name, fontsize = 11)
        axis.set_ylabel(horizontal_axis_name, fontsize = 11)
        # Add mean to the plots
        for index, key in enumerate(dictionary.keys()):
            axis.text(index + 0.05, np.mean(dictionary[key]), "%0.1f" % np.mean(dictionary[key]))
        # Adjust everything automatically
        plt.tight_layout()
        plt.savefig(self.figures_path + os.sep + figure_name + ".png", format="png", dpi=300)
        # Increment the number of graphs
        NUMBER_OF_GRAPHS += 1

def indicator_builder(dataframe, indicators):
    # Create empty dictionary
    dictionary = dict()
    # For each indicator, get data
    for key in collections.Counter(dataframe[indicators[0]]):
        dictionary[key] = copy.deepcopy(dataframe[dataframe[indicators[0]] == key][indicators[1]])
    return dictionary

def indicator_builder_2(dataframe, indicators):
    # Create empty dictionary
    dictionary = dict()
    # For each indicator, get data
    for key in collections.Counter(dataframe[indicators[0]]):
        dictionary[key] = copy.deepcopy(dataframe[dataframe[indicators[0]] == key][indicators[1]])
    dataframe_ =  copy.deepcopy(dataframe)
    dataframe_.drop_duplicates()
    dictionary[indicators[2]] = copy.deepcopy(dataframe_[indicators[1]])
    return dictionary
    
# ========================================
# Functions
# ========================================

dict_princ_type_gen = {'i1': 'Indetermine',
                       's1': "Absence d'information",
                       's2': 'Bat_sans_équip',
                       't_2': 'Syst_ballon_eau_morte',
                       't2': 'Syst_ballon_eau_morte',
                        'g100': 'Chaud_gaz_cond',
                        'g101': 'Chaud_gaz_BT',
                        'g102': 'Chaud_gaz_stand',
                        'g103': 'Chaud_fioul_cond',
                        'g104': 'Chaud_fioul_BT',
                        'g105': 'Chaud_fioul_stand',
                        'g106': 'Chaud_bois',
                        'g107': 'Radiat_gaz',
                        'g108': 'C-E_gaz',
                        'g109': 'Accu_gaz_stand',
                        'g110': 'Accu_gaz_cond',
                        'g111': 'Gen_air_ch_stand',
                        'g112': 'Gen_air_ch_cond',
                        'g113': 'Tube_radiant',
                        'g114': 'Panneau_radiant',
                        'g2': 'Gen_EJ',
                        'g200': 'Gen_EJ',
                        'g3': 'Poele/insert',
                        'g300': 'Poele/insert',
                        'g4': 'Res_ch',
                        'g400': 'Res_ch',
                        'g501': 'GAHP_rev_air/eau',
                        'g502': 'GAHP_rev_eau/eau',
                        'g601': 'GAHP_air/eau',
                        'g602': 'GAHP_air/eau_HT',
                        'g603': 'GAHP_eau_glyc/eau',
                        'g604': 'GAHP_eau_glyc/eau_HT',
                        'g605': 'GAHP_eau_nappe/eau_HT',
                        'g701': 'GAHP_refr_air/eau',
                        'g702': 'GAHP_refr_eau/eau',
                        'g801': 'GAHP_air/eau_HT',
                        'g802': 'GAHP_eau_glyc/eau_HT',
                        'g803': 'GAHP_eau_nappe/eau_HT',
                        'g901': 'PAC_rev',
                        'g902': 'Syst_cond_air_DRV',
                        'g903': 'Gr_frigo_eau/eau_ou_thermofrigopompes',
                        'g904': 'PAC_thermo_bcl_eau',
                        'g1101': 'Refr_air/eau',
                        'g1201': 'PAC_air_ext/eau',
                        'g1001': 'PAC_air_ext/eau',
                        'g1002': 'PAC_air_ext/air_recy',
                        'g1003': 'PAC_air_extr/air_neuf',
                        'g1004': 'PAC_eau_nappe/eau',
                        'g1204': 'PAC_eau_nappe/eau',
                        'g1005': 'PAC_eau_glyc/eau',
                        'g1006': 'PAC_eau_nappe/air',
                        'g1007': 'PAC_eau_bcl/air',
                        'g1008': 'PAC_sol/eau',
                        'g1205': 'PAC_sol/eau',
                        'g1009': 'PAC_sol/sol',
                        'g1102': 'Refr_air_ext/air_recy',
                        'g1103': 'Refr_air_extr/air_neuf',
                        'g1104': 'Refr_eau/eau_+_eau_glyc/eau',
                        'g1105': 'Refr_eau/air_+_eau_bcl/air',
                        'g1106': 'Refr_eau_nappe/eau',
                        'g1202': 'PAC_air_extr/eau',
                        'g1203': 'PAC_air_amb/eau',
                        'g13': 'T5_PAC',
                        'b1': 'B_boucle_solaire',
                        'b200': 'B_chaud_gaz_cond',
                        'b201': 'B_chaud_gaz_BT',
                        'b202': 'B_chaud_gaz_stand',
                        'b203': 'B_chaud_fioul_cond',
                        'b204': 'B_chaud_fioul_BT',
                        'b205': 'B_chaud_fioul_stand',
                        'b206': 'B_chaud_bois',
                        'b207': 'B_adiat_gaz',
                        'b208': 'B_C-E_gaz',
                        'b209': 'B_accu_gaz_stand',
                        'b210': 'B_accu_gaz_cond',
                        'b211': 'B_gen_air_ch_stand',
                        'b212': 'B_gen_air_ch_cond',
                        'b213': 'B_tube_radiant',
                        'b214': 'B_panneau_radiant',
                        'b3': 'B_EJ',
                        'b4': 'B_res_ch',
                        'b501': 'B_PAC_air_ext/eau',
                        'b502': 'B_PAC_air_extr/eau',
                        'b503': 'CET',
                        'b504': 'B_PAC_eau_nappe/eau',
                        'b505': 'B_PAC_sol/eau',
                        'b601': 'B_GAHP_air/eau',
                        'b801': 'B_GAHP_air/eau_HT',
                        'b603': 'B_GAHP_eau_glyc/eau',
                        'b802': 'B_GAHP_eau_glyc/eau_HT',
                        'b803': 'B_GAHP_eau_nappe/eau_HT',
                        'b701': 'B_refr_air/eau',
                        'b702': 'B_refr_eau/eau',
                        'b9': 'PAC_eau_glyc/eau',
                        'b10': 'PAC_DS',
                        'b11': 'PAC_gaz_DS',
                        'b12': 'PAC_TS',
                        'b13': 'T5_CET',
                        'p1': 'PCAD'}


zones = ['H1-a', 'H1-b', 'H1-c', 'H2-a', 'H2-b', 'H2-c', 'H2-d', 'H3']
usages = [(3, 2), (1, 1), (7, 16), (5, (4, 5, 6, 7))]
usages_p = [1,2,3,4,5,6,7,8,9,10,11,12,13]
res_by_usage = {}
res_by_usage_vf = {}

dict_field_opebn = cn_opebn.field_dict
for key in dict_field_opebn.keys():
    print(key)
    print(dict_field_opebn[key])
    print('')

dict_field_opebn = cn_opebn.field_dict
fields_opebn = []
for key in dict_field_opebn.keys():
    print(key)
    for el in dict_field_opebn[key]:
        if 'cep' in el:
            print("--> {}".format(el))
            fields_opebn.append("{}.{}".format(key, el))
        elif 'bbio' in el:
            print("--> {}".format(el))
            fields_opebn.append("{}.{}".format(key, el))
        elif 'dies' in el:
            print("--> {}".format(el))
            fields_opebn.append("{}.{}".format(key, el))
        elif 'park' in el:
            print("--> {}".format(el))
            fields_opebn.append("{}.{}".format(key, el))
        elif 'asc' in el:
            print("--> {}".format(el))
            fields_opebn.append("{}.{}".format(key, el))
        elif 'co2' in el:
            print("--> {}".format(el))
            fields_opebn.append("{}.{}".format(key, el))
        elif 'eges' in el:
            print("--> {}".format(el))
            fields_opebn.append("{}.{}".format(key, el))
        elif 'dh' in el:
            print("--> {}".format(el))
            fields_opebn.append("{}.{}".format(key, el))
        elif 'generat' in el:
            print("--> {}".format(el))
            fields_opebn.append("{}.{}".format(key, el))
        elif 'inertie' in el:
            print("--> {}".format(el))
            fields_opebn.append("{}.{}".format(key, el))
        elif 'bepos' in el:
            print("--> {}".format(el))
            fields_opebn.append("{}.{}".format(key, el))
        elif 'cef' in el:
            print("--> {}".format(el))
            fields_opebn.append("{}.{}".format(key, el))
        else:
            continue
    print("")

## Création de la liste des opérations valides selon le filtre "OPEBN.valid()"
#df_opebn_by_id = df_opebn.groupby('id').first()
#index_df_opebn_by_id = df_opebn_by_id.index
#tuple_index_df_opebn_id = tuple(index_df_opebn_by_id)
#
## Création du dataframe contenant les indicateurs de bâtiment nécessaires avec en plus la colonne de l'EGES en exploitation
#df_opebn_final = pd.DataFrame()
#for id_rsee in index_df_opebn_by_id:
#    rsee = cn_opebn.get_rsee_by_id(id_rsee)
#    df_rsee_indic_bat = rsee.get_indicateurs_batiment()
#    df_rsee_indic_bat['Eges_exploitation'] = 0
#    for i in range(0, len(df_rsee_indic_bat)):
#        df_rsee_indic_bat.loc[df_rsee_indic_bat.index[i], 'Eges_exploitation'] = df_rsee_indic_bat.loc[df_rsee_indic_bat.index[i], 'Eges'] - df_rsee_indic_bat.loc[df_rsee_indic_bat.index[i], 'EgesPCE']
#    df_opebn_final = pd.concat([df_opebn_final, df_rsee_indic_bat])
#            
#df_opebn_final_2 = df_opebn_final.drop(['id_rsee', 'id_bat'], axis=1, inplace=False)
#df_opebn_vf = df_opebn_final_2.reset_index(drop = True)
#
## Création du dataframe avec la requete rslab sur la base OPEBN contenant les paramètres dont on a besoin et qui ne sont pas dans le précédent
#fields_opebn_final = ['operation.id', 
#                      'operation.nom', 
#                      'operation_rsenv.id', 
#                      'operation_rsenv.operation_id',
#                      'batiment_rset.is_park',
#                      'batiment_rset.nb_park',
#                      'batiment_rset.surf_park',
#                      'batiment.is_ascenceur',
#                      'batiment.asc_hauteur',
#                      'batiment.asc_nb_etage',
#                      'indicateurs_rset.inertie_quotidienne',
#                      'indicateurs_rset.cep_max',
#                      'indicateurs_rset.cep_projet',
#                      'indicateurs_rset.cep_fr',
#                      'indicateurs_rset.cef_aue',
#                      'indicateurs_rset.cep_elec_mob',
#                      'indicateurs_rset.cep_elec_immob',
#                      'indicateurs_rset.cep_projet_hp',
#                      'indicateurs_rset.cep_projet_nr',
#                      'indicateurs_rset.bbio_max',
#                      'indicateurs_rset.bbio_projet',
#                      'indicateurs_rset.bbio_fr',
#                      'indicateurs_rset.dies',
#                      'batiment_rsenv.generateur_principal_ch',
#                      'batiment_rsenv.generateur_principal_ecs',
#                      'batiment_rsenv.generateur_principal_fr']
#    
#conditions=['operation.id in {}'.format(tuple_index_df_opebn_id)]
#
#df_base = cn_opebn.auto_query(fields_opebn_final, conditions)
#
## Dataframes finaux à exploiter
#df_OPEBN_final = pd.concat([df_base, df_opebn_vf], axis=1)
#df_OPEBN_final_with_park = df_OPEBN_final.loc[df_OPEBN_final['batiment_rset.is_park'] == 1]
#df_OPEBN_final_with_ascenseur = df_OPEBN_final.loc[df_OPEBN_final['batiment.is_ascenceur'] == 1]
#df_OPEBN_final_with_park_and_asc = df_OPEBN_final.loc[(df_OPEBN_final['batiment_rset.is_park'] == 1) & (df_OPEBN_final['batiment.is_ascenceur'] == 1)]
#
## Sauvegarde des DF finaux
#df_OPEBN_final.to_csv(r"C:\Users\MOREIRA\OneDrive - CSTBGroup\2019\COTEC_RSLAB\RT2012\RT2012_simulation_OPEBN\dataframes\dataframe_OPEBN_global")
#df_OPEBN_final_with_park.to_csv(r"C:\Users\MOREIRA\OneDrive - CSTBGroup\2019\COTEC_RSLAB\RT2012\RT2012_simulation_OPEBN\dataframes\dataframe_OPEBN_parking")
#df_OPEBN_final_with_ascenseur.to_csv(r"C:\Users\MOREIRA\OneDrive - CSTBGroup\2019\COTEC_RSLAB\RT2012\RT2012_simulation_OPEBN\dataframes\dataframe_OPEBN_ascenseur")
#df_OPEBN_final_with_park_and_asc.to_csv(r"C:\Users\MOREIRA\OneDrive - CSTBGroup\2019\COTEC_RSLAB\RT2012\RT2012_simulation_OPEBN\dataframes\dataframe_OPEBN_parking_and_ascenseur")

# Lecture des DF finaux
df = pd.read_csv(r"C:\Users\MOREIRA\OneDrive - CSTBGroup\2019\COTEC_RSLAB\RT2012\RT2012_simulation_OPEBN\dataframes\dataframe_OPEBN_global", engine='python', encoding='utf-8')
df_OPEBN_final = df
#df = pd.read_csv(r"C:\Users\MOREIRA\OneDrive - CSTBGroup\2019\COTEC_RSLAB\RT2012\RT2012_simulation_OPEBN\dataframes\dataframe_OPEBN_parking", engine='python', encoding='utf-8')
#df_OPEBN_final_with_park = df
#df = pd.read_csv(r"C:\Users\MOREIRA\OneDrive - CSTBGroup\2019\COTEC_RSLAB\RT2012\RT2012_simulation_OPEBN\dataframes\dataframe_OPEBN_ascenseur", engine='python', encoding='utf-8')
#df_OPEBN_final_with_ascenseur = df
#df = pd.read_csv(r"C:\Users\MOREIRA\OneDrive - CSTBGroup\2019\COTEC_RSLAB\RT2012\RT2012_simulation_OPEBN\dataframes\dataframe_OPEBN_parking_and_ascenseur", engine='python', encoding='utf-8')
#df_OPEBN_final_with_park_and_asc = df

df_OPEBN_final_filter = filter_df_with_quantiles(df_OPEBN_final, "indicateurs_rset.cep_projet", 0.05)

df_OPEBN_final_with_park_filter = df_OPEBN_final_filter.loc[df_OPEBN_final_filter['batiment_rset.is_park'] == 1]
df_OPEBN_final_with_ascenseur_filter = df_OPEBN_final_filter.loc[df_OPEBN_final_filter['batiment.is_ascenceur'] == 1]
df_OPEBN_final_with_park_and_asc_filter = df_OPEBN_final_filter.loc[(df_OPEBN_final_filter['batiment_rset.is_park'] == 1) & (df_OPEBN_final['batiment.is_ascenceur'] == 1)]
df_OPEBN_final_no_park_and_asc_filter = df_OPEBN_final_filter.loc[(df_OPEBN_final_filter['batiment_rset.is_park'] == 0) & (df_OPEBN_final['batiment.is_ascenceur'] == 0)]


# Distributions des paramètres pertinents pour chaque df
# POUR LE DATAFRAME GLOBAL
# CEP
folder_results = r"C:\Users\MOREIRA\Documents\rt2012_re2020_comparaisons\figures\opebn\histograms\echantillon_global"
#df_OPEBN_final_filter = filter_df_with_quantiles(df_OPEBN_final, "indicateurs_rset.cep_projet", 0.05)
fig = plt.figure()
ax = df_OPEBN_final_filter['indicateurs_rset.cep_projet'].hist(bins=50)
plt.xlabel('Cep (kWh/m².an)')
plt.ylabel("Nombre de bâtiments (-)")
plt.title('Répartition du Cep par bâtiments ({} bâtiments)'.format(len(df_OPEBN_final_filter)))
ax.grid(False)
plt.tight_layout()
plt.savefig(os.path.join(folder_results, 'hist_cep_bat.png'), dpi=150)

# BBIO
folder_results = r"C:\Users\MOREIRA\Documents\rt2012_re2020_comparaisons\figures\opebn\histograms\echantillon_global"
#df_OPEBN_final_filter = filter_df_with_quantiles(df_OPEBN_final, "indicateurs_rset.bbio_projet", 0.05)
fig = plt.figure()
ax = df_OPEBN_final_filter['indicateurs_rset.bbio_projet'].hist(bins=50)
plt.xlabel('Bbio (points)')
plt.ylabel("Nombre de bâtiments (-)")
plt.title('Répartition du Bbio par bâtiments ({} bâtiments)'.format(len(df_OPEBN_final_filter)))
ax.grid(False)
plt.tight_layout()
plt.savefig(os.path.join(folder_results, 'hist_bbio_bat.png'), dpi=150)

# DIES
#folder_results = r"C:\Users\MOREIRA\OneDrive - CSTBGroup\2019\COTEC_RSLAB\RT2012\RT2012_simulation_OPEBN\histogrammes_avec_filtre\echantillon_global"
#df_OPEBN_final_filter = filter_df_with_quantiles(df_OPEBN_final, "indicateurs_rset.dies", 0.05)
#fig = plt.figure()
#ax = df_OPEBN_final_filter['indicateurs_rset.dies'].hist(bins=50)
#plt.xlabel('DIES (-)')
#plt.ylabel("Nombre de bâtiments (-)")
#plt.title('Répartition de la DIES par bâtiments ({} bâtiments)'.format(len(df_OPEBN_final_filter)))
#ax.grid(False)
#plt.tight_layout()
#plt.savefig(os.path.join(folder_results, 'hist_dies_bat.png'), dpi=150)

############################################################################
# POUR LE DATAFRAME AVEC PARKING
# CEP
folder_results = r"C:\Users\MOREIRA\Documents\rt2012_re2020_comparaisons\figures\opebn\histograms\echantillon_parking"
#df_OPEBN_final_with_park_filter = filter_df_with_quantiles(df_OPEBN_final_with_park, "indicateurs_rset.cep_projet", 0.05)
fig = plt.figure()
ax = df_OPEBN_final_with_park_filter['indicateurs_rset.cep_projet'].hist(bins=50)
plt.xlabel('Cep (kWh/m².an)')
plt.ylabel("Nombre de bâtiments (-)")
plt.title('Répartition du Cep par bâtiments ({} bâtiments)'.format(len(df_OPEBN_final_with_park_filter)))
ax.grid(False)
plt.tight_layout()
plt.savefig(os.path.join(folder_results, 'hist_cep_bat_parking.png'), dpi=150)

# BBIO
folder_results = r"C:\Users\MOREIRA\Documents\rt2012_re2020_comparaisons\figures\opebn\histograms\echantillon_parking"
#df_OPEBN_final_with_park_filter = filter_df_with_quantiles(df_OPEBN_final_with_park, "indicateurs_rset.bbio_projet", 0.05)
fig = plt.figure()
ax = df_OPEBN_final_with_park_filter['indicateurs_rset.bbio_projet'].hist(bins=50)
plt.xlabel('Bbio (points)')
plt.ylabel("Nombre de bâtiments (-)")
plt.title('Répartition du Bbio par bâtiments ({} bâtiments)'.format(len(df_OPEBN_final_with_park_filter)))
ax.grid(False)
plt.tight_layout()
plt.savefig(os.path.join(folder_results, 'hist_bbio_bat_parking.png'), dpi=150)

# DIES
#folder_results = r"C:\Users\MOREIRA\OneDrive - CSTBGroup\2019\COTEC_RSLAB\RT2012\RT2012_simulation_OPEBN\histogrammes_avec_filtre\echantillon_parking"
#df_OPEBN_final_with_park_filter = filter_df_with_quantiles(df_OPEBN_final_with_park, "indicateurs_rset.dies", 0.05)
#fig = plt.figure()
#ax = df_OPEBN_final_with_park_filter['indicateurs_rset.dies'].hist(bins=50)
#plt.xlabel('DIES (-)')
#plt.ylabel("Nombre de bâtiments (-)")
#plt.title('Répartition de la DIES par bâtiments ({} bâtiments)'.format(len(df_OPEBN_final_with_park_filter)))
#ax.grid(False)
#plt.tight_layout()
#plt.savefig(os.path.join(folder_results, 'hist_dies_bat_parking.png'), dpi=150)

# CEF AUE
folder_results = r"C:\Users\MOREIRA\Documents\rt2012_re2020_comparaisons\figures\opebn\histograms\echantillon_parking"
#df_OPEBN_final_with_park_filter = filter_df_with_quantiles(df_OPEBN_final_with_park, "indicateurs_rset.cef_aue", 0.05)
fig = plt.figure()
ax = df_OPEBN_final_with_park_filter['indicateurs_rset.cef_aue'].hist(bins=50)
plt.xlabel('CEF AUE (kWhEF/m².an)')
plt.ylabel("Nombre de bâtiments (-)")
plt.title('Répartition du Cef AUE par bâtiments ({} bâtiments)'.format(len(df_OPEBN_final_with_park_filter)))
ax.grid(False)
plt.tight_layout()
plt.savefig(os.path.join(folder_results, 'hist_cef_aue_bat_parking.png'), dpi=150)

# CEP ELEC MOB
folder_results = r"C:\Users\MOREIRA\Documents\rt2012_re2020_comparaisons\figures\opebn\histograms\echantillon_parking"
#df_OPEBN_final_with_park_filter = filter_df_with_quantiles(df_OPEBN_final_with_park, "indicateurs_rset.cep_elec_mob", 0.05)
fig = plt.figure()
ax = df_OPEBN_final_with_park_filter['indicateurs_rset.cep_elec_mob'].hist(bins=50)
plt.xlabel('CEP usage mobilier (kWhEP/m².an)')
plt.ylabel("Nombre de bâtiments (-)")
plt.title('Répartition du Cep usage mobilier par bâtiments ({} bâtiments)'.format(len(df_OPEBN_final_with_park_filter)))
ax.grid(False)
plt.tight_layout()
plt.savefig(os.path.join(folder_results, 'hist_cep_elec_mob_bat_parking.png'), dpi=150)

# CEP ELEC IMMOB
folder_results = r"C:\Users\MOREIRA\Documents\rt2012_re2020_comparaisons\figures\opebn\histograms\echantillon_parking"
#df_OPEBN_final_with_park_filter = filter_df_with_quantiles(df_OPEBN_final_with_park, "indicateurs_rset.cep_elec_immob", 0.05)
fig = plt.figure()
ax = df_OPEBN_final_with_park_filter['indicateurs_rset.cep_elec_immob'].hist(bins=50)
plt.xlabel('CEP usage immobilier (kWhEP/m².an)')
plt.ylabel("Nombre de bâtiments (-)")
plt.title('Répartition du Cep usage immobilier par bâtiments ({} bâtiments)'.format(len(df_OPEBN_final_with_park_filter)))
ax.grid(False)
plt.tight_layout()
plt.savefig(os.path.join(folder_results, 'hist_cep_elec_immob_bat_parking.png'), dpi=150)

# CO2 EGES EXPLOITATION
folder_results = r"C:\Users\MOREIRA\Documents\rt2012_re2020_comparaisons\figures\opebn\histograms\echantillon_parking"
#df_OPEBN_final_with_park_filter = filter_df_with_quantiles(df_OPEBN_final_with_park, "Eges_exploitation", 0.05)
fig = plt.figure()
ax = df_OPEBN_final_with_park_filter['Eges_exploitation'].hist(bins=50)
plt.xlabel('Eges en exploitation (-)')
plt.ylabel("Nombre de bâtiments (-)")
plt.title('Répartition du Eges en exploitation par bâtiments ({} bâtiments)'.format(len(df_OPEBN_final_with_park_filter)))
ax.grid(False)
plt.tight_layout()
plt.savefig(os.path.join(folder_results, 'hist_co2_eges_exploitation_parking.png'), dpi=150)

############################################################################
# POUR LE DATAFRAME AVEC ASCENSEUR
# CEP
folder_results = r"C:\Users\MOREIRA\Documents\rt2012_re2020_comparaisons\figures\opebn\histograms\echantillon_ascenseur"
#df_OPEBN_final_with_ascenseur_filter = filter_df_with_quantiles(df_OPEBN_final_with_ascenseur, "indicateurs_rset.cep_projet", 0.05)
fig = plt.figure()
ax = df_OPEBN_final_with_ascenseur_filter['indicateurs_rset.cep_projet'].hist(bins=50)
plt.xlabel('Cep (kWh/m².an)')
plt.ylabel("Nombre de bâtiments (-)")
plt.title('Répartition du Cep par bâtiments ({} bâtiments)'.format(len(df_OPEBN_final_with_ascenseur_filter)))
ax.grid(False)
plt.tight_layout()
plt.savefig(os.path.join(folder_results, 'hist_cep_bat_ascenseur.png'), dpi=150)

# BBIO
folder_results = r"C:\Users\MOREIRA\Documents\rt2012_re2020_comparaisons\figures\opebn\histograms\echantillon_ascenseur"
#df_OPEBN_final_with_ascenseur_filter = filter_df_with_quantiles(df_OPEBN_final_with_ascenseur, "indicateurs_rset.bbio_projet", 0.05)
fig = plt.figure()
ax = df_OPEBN_final_with_ascenseur_filter['indicateurs_rset.bbio_projet'].hist(bins=50)
plt.xlabel('Bbio (points)')
plt.ylabel("Nombre de bâtiments (-)")
plt.title('Répartition du Bbio par bâtiments ({} bâtiments)'.format(len(df_OPEBN_final_with_ascenseur_filter)))
ax.grid(False)
plt.tight_layout()
plt.savefig(os.path.join(folder_results, 'hist_bbio_bat_ascenseur.png'), dpi=150)

# DIES
#folder_results = r"C:\Users\MOREIRA\OneDrive - CSTBGroup\2019\COTEC_RSLAB\RT2012\RT2012_simulation_OPEBN\histogrammes_avec_filtre\echantillon_ascenseur"
#df_OPEBN_final_with_ascenseur_filter = filter_df_with_quantiles(df_OPEBN_final_with_ascenseur, "indicateurs_rset.dies", 0.05)
#fig = plt.figure()
#ax = df_OPEBN_final_with_ascenseur_filter['indicateurs_rset.dies'].hist(bins=50)
#plt.xlabel('DIES (-)')
#plt.ylabel("Nombre de bâtiments (-)")
#plt.title('Répartition de la DIES par bâtiments ({} bâtiments)'.format(len(df_OPEBN_final_with_ascenseur_filter)))
#ax.grid(False)
#plt.tight_layout()
#plt.savefig(os.path.join(folder_results, 'hist_dies_bat_ascenseur.png'), dpi=150)

# CEF AUE
folder_results = r"C:\Users\MOREIRA\Documents\rt2012_re2020_comparaisons\figures\opebn\histograms\echantillon_ascenseur"
#df_OPEBN_final_with_ascenseur_filter = filter_df_with_quantiles(df_OPEBN_final_with_ascenseur, "indicateurs_rset.cef_aue", 0.05)
fig = plt.figure()
ax = df_OPEBN_final_with_ascenseur_filter['indicateurs_rset.cef_aue'].hist(bins=50)
plt.xlabel('CEF AUE (kWhEF/m².an)')
plt.ylabel("Nombre de bâtiments (-)")
plt.title('Répartition du Cef AUE par bâtiments ({} bâtiments)'.format(len(df_OPEBN_final_with_ascenseur_filter)))
ax.grid(False)
plt.tight_layout()
plt.savefig(os.path.join(folder_results, 'hist_cef_aue_bat_ascenseur.png'), dpi=150)

# CEP ELEC MOB
folder_results = r"C:\Users\MOREIRA\Documents\rt2012_re2020_comparaisons\figures\opebn\histograms\echantillon_ascenseur"
#df_OPEBN_final_with_ascenseur_filter = filter_df_with_quantiles(df_OPEBN_final_with_ascenseur, "indicateurs_rset.cep_elec_mob", 0.05)
fig = plt.figure()
ax = df_OPEBN_final_with_ascenseur_filter['indicateurs_rset.cep_elec_mob'].hist(bins=50)
plt.xlabel('CEP usage mobilier (kWhEP/m².an)')
plt.ylabel("Nombre de bâtiments (-)")
plt.title('Répartition du Cep usage mobilier par bâtiments ({} bâtiments)'.format(len(df_OPEBN_final_with_ascenseur_filter)))
ax.grid(False)
plt.tight_layout()
plt.savefig(os.path.join(folder_results, 'hist_cep_elec_mob_bat_ascenseur.png'), dpi=150)

# CEP ELEC IMMOB
folder_results = r"C:\Users\MOREIRA\Documents\rt2012_re2020_comparaisons\figures\opebn\histograms\echantillon_ascenseur"
#df_OPEBN_final_with_ascenseur_filter = filter_df_with_quantiles(df_OPEBN_final_with_ascenseur, "indicateurs_rset.cep_elec_immob", 0.05)
fig = plt.figure()
ax = df_OPEBN_final_with_ascenseur_filter['indicateurs_rset.cep_elec_immob'].hist(bins=50)
plt.xlabel('CEP usage immobilier (kWhEP/m².an)')
plt.ylabel("Nombre de bâtiments (-)")
plt.title('Répartition du Cep usage immobilier par bâtiments ({} bâtiments)'.format(len(df_OPEBN_final_with_ascenseur_filter)))
ax.grid(False)
plt.tight_layout()
plt.savefig(os.path.join(folder_results, 'hist_cep_elec_immob_bat_ascenseur.png'), dpi=150)

# CO2 EGES EXPLOITATION
folder_results = r"C:\Users\MOREIRA\Documents\rt2012_re2020_comparaisons\figures\opebn\histograms\echantillon_ascenseur"
#df_OPEBN_final_with_ascenseur_filter = filter_df_with_quantiles(df_OPEBN_final_with_ascenseur, "Eges_exploitation", 0.05)
fig = plt.figure()
ax = df_OPEBN_final_with_ascenseur_filter['Eges_exploitation'].hist(bins=50)
plt.xlabel('Eges en exploitation (-)')
plt.ylabel("Nombre de bâtiments (-)")
plt.title('Répartition du Eges en exploitation par bâtiments ({} bâtiments)'.format(len(df_OPEBN_final_with_ascenseur_filter)))
ax.grid(False)
plt.tight_layout()
plt.savefig(os.path.join(folder_results, 'hist_co2_eges_exploitation_ascenseur.png'), dpi=150)


############################################################################
# POUR LE DATAFRAME AVEC PARKING ET ASCENSEUR
# CEP
folder_results = r"C:\Users\MOREIRA\Documents\rt2012_re2020_comparaisons\figures\opebn\histograms\echantillon_parking_asc"
#df_OPEBN_final_with_park_and_asc_filter = filter_df_with_quantiles(df_OPEBN_final_with_park_and_asc, "indicateurs_rset.cep_projet", 0.05)
fig = plt.figure()
ax = df_OPEBN_final_with_park_and_asc_filter['indicateurs_rset.cep_projet'].hist(bins=50)
plt.xlabel('Cep (kWh/m².an)')
plt.ylabel("Nombre de bâtiments (-)")
plt.title('Répartition du Cep par bâtiments ({} bâtiments)'.format(len(df_OPEBN_final_with_park_and_asc_filter)))
ax.grid(False)
plt.tight_layout()
plt.savefig(os.path.join(folder_results, 'hist_cep_bat_park_and_ascenseur.png'), dpi=150)

# BBIO
folder_results = r"C:\Users\MOREIRA\Documents\rt2012_re2020_comparaisons\figures\opebn\histograms\echantillon_parking_asc"
#df_OPEBN_final_with_park_and_asc_filter = filter_df_with_quantiles(df_OPEBN_final_with_park_and_asc, "indicateurs_rset.bbio_projet", 0.05)
fig = plt.figure()
ax = df_OPEBN_final_with_park_and_asc_filter['indicateurs_rset.bbio_projet'].hist(bins=50)
plt.xlabel('Bbio (points)')
plt.ylabel("Nombre de bâtiments (-)")
plt.title('Répartition du Bbio par bâtiments ({} bâtiments)'.format(len(df_OPEBN_final_with_park_and_asc_filter)))
ax.grid(False)
plt.tight_layout()
plt.savefig(os.path.join(folder_results, 'hist_bbio_bat_park_and_ascenseur.png'), dpi=150)

# DIES
#folder_results = r"C:\Users\MOREIRA\OneDrive - CSTBGroup\2019\COTEC_RSLAB\RT2012\RT2012_simulation_OPEBN\histogrammes_avec_filtre\echantillon_ascenseur"
#df_OPEBN_final_with_ascenseur_filter = filter_df_with_quantiles(df_OPEBN_final_with_ascenseur, "indicateurs_rset.dies", 0.05)
#fig = plt.figure()
#ax = df_OPEBN_final_with_ascenseur_filter['indicateurs_rset.dies'].hist(bins=50)
#plt.xlabel('DIES (-)')
#plt.ylabel("Nombre de bâtiments (-)")
#plt.title('Répartition de la DIES par bâtiments ({} bâtiments)'.format(len(df_OPEBN_final_with_ascenseur_filter)))
#ax.grid(False)
#plt.tight_layout()
#plt.savefig(os.path.join(folder_results, 'hist_dies_bat_ascenseur.png'), dpi=150)

# CEF AUE
folder_results = r"C:\Users\MOREIRA\Documents\rt2012_re2020_comparaisons\figures\opebn\histograms\echantillon_parking_asc"
#df_OPEBN_final_with_park_and_asc_filter = filter_df_with_quantiles(df_OPEBN_final_with_park_and_asc, "indicateurs_rset.cef_aue", 0.05)
fig = plt.figure()
ax = df_OPEBN_final_with_park_and_asc_filter['indicateurs_rset.cef_aue'].hist(bins=50)
plt.xlabel('CEF AUE (kWhEF/m².an)')
plt.ylabel("Nombre de bâtiments (-)")
plt.title('Répartition du Cef AUE par bâtiments ({} bâtiments)'.format(len(df_OPEBN_final_with_park_and_asc_filter)))
ax.grid(False)
plt.tight_layout()
plt.savefig(os.path.join(folder_results, 'hist_cef_aue_bat_park_and_ascenseur.png'), dpi=150)

# CEP ELEC MOB
folder_results = r"C:\Users\MOREIRA\Documents\rt2012_re2020_comparaisons\figures\opebn\histograms\echantillon_parking_asc"
#df_OPEBN_final_with_park_and_asc_filter = filter_df_with_quantiles(df_OPEBN_final_with_park_and_asc, "indicateurs_rset.cep_elec_mob", 0.05)
fig = plt.figure()
ax = df_OPEBN_final_with_park_and_asc_filter['indicateurs_rset.cep_elec_mob'].hist(bins=50)
plt.xlabel('CEP usage mobilier (kWhEP/m².an)')
plt.ylabel("Nombre de bâtiments (-)")
plt.title('Répartition du Cep usage mobilier par bâtiments ({} bâtiments)'.format(len(df_OPEBN_final_with_park_and_asc_filter)))
ax.grid(False)
plt.tight_layout()
plt.savefig(os.path.join(folder_results, 'hist_cep_elec_mob_bat_park_and_ascenseur.png'), dpi=150)

# CEP ELEC IMMOB
folder_results = r"C:\Users\MOREIRA\Documents\rt2012_re2020_comparaisons\figures\opebn\histograms\echantillon_parking_asc"
#df_OPEBN_final_with_park_and_asc_filter = filter_df_with_quantiles(df_OPEBN_final_with_park_and_asc, "indicateurs_rset.cep_elec_immob", 0.05)
fig = plt.figure()
ax = df_OPEBN_final_with_park_and_asc_filter['indicateurs_rset.cep_elec_immob'].hist(bins=50)
plt.xlabel('CEP usage immobilier (kWhEP/m².an)')
plt.ylabel("Nombre de bâtiments (-)")
plt.title('Répartition du Cep usage immobilier par bâtiments ({} bâtiments)'.format(len(df_OPEBN_final_with_park_and_asc_filter)))
ax.grid(False)
plt.tight_layout()
plt.savefig(os.path.join(folder_results, 'hist_cep_elec_immob_bat_park_and_ascenseur.png'), dpi=150)

# CO2 EGES EXPLOITATION
folder_results = r"C:\Users\MOREIRA\Documents\rt2012_re2020_comparaisons\figures\opebn\histograms\echantillon_parking_asc"
#df_OPEBN_final_with_park_and_asc_filter = filter_df_with_quantiles(df_OPEBN_final_with_park_and_asc, "Eges_exploitation", 0.05)
fig = plt.figure()
ax = df_OPEBN_final_with_park_and_asc_filter['Eges_exploitation'].hist(bins=50)
plt.xlabel('Eges en exploitation (-)')
plt.ylabel("Nombre de bâtiments (-)")
plt.title('Répartition du Eges en exploitation par bâtiments ({} bâtiments)'.format(len(df_OPEBN_final_with_park_and_asc_filter)))
ax.grid(False)
plt.tight_layout()
plt.savefig(os.path.join(folder_results, 'hist_co2_eges_exploitation_ascenseur.png'), dpi=150)




#########################################################################################################################################################
#########################################################################################################################################################
#########################################################################################################################################################


        
# GRAPHIQUES EN VIOLON DU CEP A USAGE IMMOBILIER POUR CHAQUE TYPE DE CAS
df_base_opebn = df_OPEBN_final
df_base_opebn['cep_aue'] = 0
df_base_opebn['type_cas'] = 0

for i in range(0, len(df_base_opebn)):
    # CEP AUE
    df_base_opebn.loc[df_base_opebn.index[i], 'cep_aue'] = df_base_opebn.loc[df_base_opebn.index[i], 'indicateurs_rset.cef_aue'] * 2.58
    
    # Parkings et ascenseurs
    is_park = df_base_opebn.loc[df_base_opebn.index[i], 'batiment_rset.is_park']
    is_ascenseur = df_base_opebn.loc[df_base_opebn.index[i], 'batiment.is_ascenceur']
    if (is_park == 1 and is_ascenseur == 1):
        df_base_opebn.loc[df_base_opebn.index[i], 'type_cas'] = "Avec_parkings_et_ascenseurs"
    elif is_park == 1:
        df_base_opebn.loc[df_base_opebn.index[i], 'type_cas'] = "Avec_parkings"
    elif is_ascenseur == 1:
        df_base_opebn.loc[df_base_opebn.index[i], 'type_cas'] = "Avec_ascenseurs"
    else:
        df_base_opebn.loc[df_base_opebn.index[i], 'type_cas'] = "Sans_parkings_ni_ascenseurs"
    
df_cep_immob_filter = filter_df_with_quantiles(df_base_opebn, 'indicateurs_rset.cep_elec_immob', 0.05)
df_cep_filter = filter_df_with_quantiles(df_base_opebn, 'indicateurs_rset.cep_projet', 0.05)
df_cef_aue_filter = filter_df_with_quantiles(df_base_opebn, 'indicateurs_rset.cef_aue', 0.05)
df_cep_aue_filter = filter_df_with_quantiles(df_base_opebn, 'cep_aue', 0.05)
df_eges_exploitation_filter = filter_df_with_quantiles(df_base_opebn, 'Eges_exploitation', 0.05)

analysis = PlotHandler(df_cep_immob_filter, [], [], os.path.join(r"C:\Users\MOREIRA\Documents\rt2012_re2020_comparaisons\figures\opebn\violins"))
analysis.display_violin_plot("tous_les_types_de_cas_cep_immob", indicator_builder_2, ["type_cas", 'indicateurs_rset.cep_elec_immob', 'Totalité_des_cas'], "Type de cas [-]", "Cep usage immobilier [kWh/m².an]", None, None, True)

analysis = PlotHandler(df_cep_filter, [], [], os.path.join(r"C:\Users\MOREIRA\Documents\rt2012_re2020_comparaisons\figures\opebn\violins"))
analysis.display_violin_plot("tous_les_types_de_cas_cep_projet", indicator_builder_2, ["type_cas", 'indicateurs_rset.cep_projet', "Totalité_des_cas"], "Type de cas [-]", "Cep [kWh/m².an]", None, None, True)

analysis = PlotHandler(df_cef_aue_filter, [], [], os.path.join(r"C:\Users\MOREIRA\Documents\rt2012_re2020_comparaisons\figures\opebn\violins"))
analysis.display_violin_plot("tous_les_types_de_cas_cef_aue", indicator_builder_2, ["type_cas", 'indicateurs_rset.cef_aue', "Totalité_des_cas"], "Type de cas [-]", "Cef AUE [kWhEF/m².an]", None, None, True)

analysis = PlotHandler(df_cep_aue_filter, [], [], os.path.join(r"C:\Users\MOREIRA\Documents\rt2012_re2020_comparaisons\figures\opebn\violins"))
analysis.display_violin_plot("tous_les_types_de_cas_cep_aue", indicator_builder_2, ["type_cas", 'cep_aue', "Totalité_des_cas"], "Type de cas [-]", "Cep AUE [kWhEF/m².an]", None, None, True)

analysis = PlotHandler(df_eges_exploitation_filter, [], [], os.path.join(r"C:\Users\MOREIRA\Documents\rt2012_re2020_comparaisons\figures\opebn\violins"))
analysis.display_violin_plot("tous_les_types_de_cas_Eges_exploit", indicator_builder_2, ["type_cas", 'Eges_exploitation', "Totalité_des_cas"], "Type de cas [-]", "Eges en exploitation [kgCO2/m²]", None, None, True)
