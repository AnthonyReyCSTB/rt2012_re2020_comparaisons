# -*- coding: utf-8 -*-
"""
Created on Thu Oct  3 14:40:43 2019

@author: MOREIRA
"""

import rslab as rs
from rslab import analysis_tools
import seaborn as sns
import matplotlib.pyplot as plt
import math
import os
import random
from random import *
import pandas as pd
import numpy as np
from lxml import etree, objectify
import pycometh as pc
import json
from pkg_resources import resource_filename
from pycometh.xml_objects import Rt2012,Rset,Rt2015,ComethWrapper,load_xml_object
from pycometh.tools import reload_package
from pycometh.tools import download_cometh_engines,force_download_cometh_engines
from pathlib import Path
import statistics
import shutil
import warnings
cn=rs.cn_ope

dict_princ_type_gen = {'i1': 'Indetermine',
                       's1': "Absence d'information",
                       's2': 'Bat_sans_équip',
                       't_2': 'Syst_ballon_eau_morte',
                       't2': 'Syst_ballon_eau_morte',
                        'g100': 'Chaud_gaz_cond',
                        'g101': 'Chaud_gaz_BT',
                        'g102': 'Chaud_gaz_stand',
                        'g103': 'Chaud_fioul_cond',
                        'g104': 'Chaud_fioul_BT',
                        'g105': 'Chaud_fioul_stand',
                        'g106': 'Chaud_bois',
                        'g107': 'Radiat_gaz',
                        'g108': 'C-E_gaz',
                        'g109': 'Accu_gaz_stand',
                        'g110': 'Accu_gaz_cond',
                        'g111': 'Gen_air_ch_stand',
                        'g112': 'Gen_air_ch_cond',
                        'g113': 'Tube_radiant',
                        'g114': 'Panneau_radiant',
                        'g2': 'Gen_EJ',
                        'g200': 'Gen_EJ',
                        'g3': 'Poele/insert',
                        'g300': 'Poele/insert',
                        'g4': 'Res_ch',
                        'g400': 'Res_ch',
                        'g501': 'GAHP_rev_air/eau',
                        'g502': 'GAHP_rev_eau/eau',
                        'g601': 'GAHP_air/eau',
                        'g602': 'GAHP_air/eau_HT',
                        'g603': 'GAHP_eau_glyc/eau',
                        'g604': 'GAHP_eau_glyc/eau_HT',
                        'g605': 'GAHP_eau_nappe/eau_HT',
                        'g701': 'GAHP_refr_air/eau',
                        'g702': 'GAHP_refr_eau/eau',
                        'g801': 'GAHP_air/eau_HT',
                        'g802': 'GAHP_eau_glyc/eau_HT',
                        'g803': 'GAHP_eau_nappe/eau_HT',
                        'g901': 'PAC_rev',
                        'g902': 'Syst_cond_air_DRV',
                        'g903': 'Gr_frigo_eau/eau_ou_thermofrigopompes',
                        'g904': 'PAC_thermo_bcl_eau',
                        'g1101': 'Refr_air/eau',
                        'g1201': 'PAC_air_ext/eau',
                        'g1001': 'PAC_air_ext/eau',
                        'g1002': 'PAC_air_ext/air_recy',
                        'g1003': 'PAC_air_extr/air_neuf',
                        'g1004': 'PAC_eau_nappe/eau',
                        'g1204': 'PAC_eau_nappe/eau',
                        'g1005': 'PAC_eau_glyc/eau',
                        'g1006': 'PAC_eau_nappe/air',
                        'g1007': 'PAC_eau_bcl/air',
                        'g1008': 'PAC_sol/eau',
                        'g1205': 'PAC_sol/eau',
                        'g1009': 'PAC_sol/sol',
                        'g1102': 'Refr_air_ext/air_recy',
                        'g1103': 'Refr_air_extr/air_neuf',
                        'g1104': 'Refr_eau/eau_+_eau_glyc/eau',
                        'g1105': 'Refr_eau/air_+_eau_bcl/air',
                        'g1106': 'Refr_eau_nappe/eau',
                        'g1202': 'PAC_air_extr/eau',
                        'g1203': 'PAC_air_amb/eau',
                        'g13': 'T5_PAC',
                        'b1': 'B_boucle_solaire',
                        'b200': 'B_chaud_gaz_cond',
                        'b201': 'B_chaud_gaz_BT',
                        'b202': 'B_chaud_gaz_stand',
                        'b203': 'B_chaud_fioul_cond',
                        'b204': 'B_chaud_fioul_BT',
                        'b205': 'B_chaud_fioul_stand',
                        'b206': 'B_chaud_bois',
                        'b207': 'B_adiat_gaz',
                        'b208': 'B_C-E_gaz',
                        'b209': 'B_accu_gaz_stand',
                        'b210': 'B_accu_gaz_cond',
                        'b211': 'B_gen_air_ch_stand',
                        'b212': 'B_gen_air_ch_cond',
                        'b213': 'B_tube_radiant',
                        'b214': 'B_panneau_radiant',
                        'b3': 'B_EJ',
                        'b4': 'B_res_ch',
                        'b501': 'B_PAC_air_ext/eau',
                        'b502': 'B_PAC_air_extr/eau',
                        'b503': 'CET',
                        'b504': 'B_PAC_eau_nappe/eau',
                        'b505': 'B_PAC_sol/eau',
                        'b601': 'B_GAHP_air/eau',
                        'b801': 'B_GAHP_air/eau_HT',
                        'b603': 'B_GAHP_eau_glyc/eau',
                        'b802': 'B_GAHP_eau_glyc/eau_HT',
                        'b803': 'B_GAHP_eau_nappe/eau_HT',
                        'b701': 'B_refr_air/eau',
                        'b702': 'B_refr_eau/eau',
                        'b9': 'PAC_eau_glyc/eau',
                        'b10': 'PAC_DS',
                        'b11': 'PAC_gaz_DS',
                        'b12': 'PAC_TS',
                        'b13': 'T5_CET',
                        'p1': 'PCAD'}

# Programme permettant de créer une liste plus 'visuelle' contenant toutes les tables et les champs associés de la base de données 

i=0
k=0
tables_plus_fields=[]
fields=cn._fields
tables=cn._tables
while i <= len(tables)-1:
        tables_plus_fields.append("Table : "+tables[i])
        for j in range(0,len(cn.find(tables[i]))):
            tables_plus_fields.append("   Field : "+cn.find(tables[i])[j])
        i=i+1
        
        
# Fonction permettant de filtrer avec une certaine valeur de quantile les valeurs extremes d'un dataframe
        
def filter_df_with_quantiles(df, column, percent):
    quantile_min = percent
    quantile_max = 1 - percent
    quantiles = df.quantile([quantile_min, quantile_max])
    
    if column == "":
        for col in quantiles.columns:
            min_max = quantiles[col].values
            value_min = min_max[0]
            value_max = min_max[1]
            mask = (df[col] >= value_min) & (df[col] <= value_max)
            df = df.loc[mask]
    
    else:
        min_max = quantiles[column].values
        value_min = min_max[0]
        value_max = min_max[1]
        mask = (df[column] >= value_min) & (df[column] <= value_max)
        df = df.loc[mask]
            
    return df

def unique_usage(df):
    df_u=df.unique()
    if len(df_u)>1:
        return False
    else:
        return df_u[0]
    
def tri_usage_batiments(df_init, usage_principal, usage_zone):
    usage_principal_to_usage = {}
    res_int_usage = {}
    res_final = {}
    columns = df_init.columns
    df_all = pd.DataFrame()
     
    if usage_principal == 1:
        usage_principal_to_usage[usage_principal] = [1]
    if usage_principal == 2:
        usage_principal_to_usage[usage_principal] = [1]
    if usage_principal == 3:
        usage_principal_to_usage[usage_principal] = [2]
    if usage_principal == 4:
        usage_principal_to_usage[usage_principal] = [2, 16, 22]
    if usage_principal == 5:
        usage_principal_to_usage[usage_principal] = [4, 5, 6, 7]
    if usage_principal == 6:
        usage_principal_to_usage[usage_principal] = [8, 10, 11, 12, 13, 14, 15]
    if usage_principal == 7:
        usage_principal_to_usage[usage_principal] = [16]
    if usage_principal == 8:
        usage_principal_to_usage[usage_principal] = [17, 18, 19, 20, 37, 38]
    if usage_principal == 9:
        usage_principal_to_usage[usage_principal] = [22]
    if usage_principal == 10:
        usage_principal_to_usage[usage_principal] = [26, 27, 28]
    if usage_principal == 11:
        usage_principal_to_usage[usage_principal] = [29]
    if usage_principal == 12:
        usage_principal_to_usage[usage_principal] = [24, 36]
    if usage_principal == 13:
        usage_principal_to_usage[usage_principal] = [32, 33, 34]
    
    for col in df_init.columns:
        if 'usage_princ' in col:
            col_ref = col
            
    df = df_init.loc[df_init[col_ref] == usage_principal]
    
    if len(df) == 0:
        return "L'usage principal {} ({}) n'est pas représenté dans ce dataframe".format(cn.value_to_meaning('usage_principal', usage_principal), usage_principal)
    
    else:
        if type(usage_zone) == list:
            for usage in usage_principal_to_usage[usage_principal]:
                if usage in usage_zone:
                    df_unique = df.groupby('batiments.id')['zones.usage'].apply(lambda x: unique_usage(x))
                    df_unique_false = df_unique.loc[df_unique != usage]
                    bat_to_delete = df_unique_false.index
                    df_index_bat = df.set_index('batiments.id')
                    df_filter = df_index_bat.drop(index = bat_to_delete)
                    df_filter_f = df_filter.reset_index()
                    res_int_usage[usage] = df_filter_f
                    df_all = pd.concat([df_all,res_int_usage[usage]], axis=0, ignore_index=True)
                else:
                    continue
            
            res_final[usage_principal] = df_all
            df_all_vf = res_final[usage_principal]
            
            return df_all_vf
            
        else:
            for usage in usage_principal_to_usage[usage_principal]:
                df_unique = df.groupby('batiments.id')['zones.usage'].apply(lambda x: unique_usage(x))
                df_unique_false = df_unique.loc[df_unique != usage]
                bat_to_delete = df_unique_false.index
                df_index_bat = df.set_index('batiments.id')
                df_filter = df_index_bat.drop(index = bat_to_delete)
                df_filter_f = df_filter.reset_index()
                res_int_usage[usage] = df_filter_f
            
            res_final[usage_principal] = res_int_usage
        
            df_final = res_final[usage_principal][usage_zone]
        
            return df_final
        


zones = ['H1-a', 'H1-b', 'H1-c', 'H2-a', 'H2-b', 'H2-c', 'H2-d', 'H3']
usages = [(3, 2), (1, 1), (7, 16), (5, (4, 5, 6, 7))]
usages_p = [1,2,3,4,5,6,7,8,9,10,11,12,13]
res_by_usage = {}
res_by_usage_vf = {}
for usage_principal, usage in usages:
    if (usage_principal == 3 or usage_principal == 1):
        fields=['operations.nb_generations',
            'operations.nb_batiments',
            'batiments.id',
            'batiments_compl.nb_zones',
            'zones.id',
            'operations.moteur_version',
            'operations.zone_climatique',
            'operations.altitude',
            'batiments_compl.categorie_ce1_ce2',
            'batiments_compl.surf_ce1',
            'batiments_compl.surf_ce2',
            'batiments_compl.surf_clim',
            'batiments_compl.ex_BR',
            'batiments_compl.inertie_quotidienne',
            'batiments_compl.usage_principal',
            'zones.usage',
            'batiments.o_shon_rt',
            'batiments_compl.shab',
            'batiments_compl.ubat',
            'batiments_compl.nb_logements',
            'batiments_compl.hauteur_zone_max',
            'batiments_compl.princ_type_generateur_ch',
            'batiments_compl.princ_type_generateur_ecs',
            'batiments_compl.princ_energie_generateur_ch',
            'batiments_compl.princ_energie_generateur_ecs',
            'batiments_compl.famille_generateur_ch',
            'batiments_compl.famille_generateur_ecs',
            'batiments_compl.type_ventilation_principal',
            'batiments.cep_projet',
            'batiments.cep_max',
            'batiments.bbio_projet',
            'batiments.bbio_max',
            'batiments.aepenr',
            'batiments.aepenr/(batiments.aepenr + batiments.o_cep_ch + batiments.o_cep_ecs + batiments.o_cep_aux_vent)',
            'batiments_compl.s_surf_capt_PV']
        
    else:
        fields=['operations.nb_generations',
            'operations.nb_batiments',
            'batiments.id',
            'batiments_compl.nb_zones',
            'zones.id',
            'operations.moteur_version',
            'operations.zone_climatique',
            'operations.altitude',
            'batiments_compl.categorie_ce1_ce2',
            'batiments_compl.surf_ce1',
            'batiments_compl.surf_ce2',
            'batiments_compl.surf_clim',
            'batiments_compl.ex_BR',
            'batiments_compl.inertie_quotidienne',
            'batiments_compl.usage_principal',
            'batiments.is_ascenseur',
            'zones.usage',
            'batiments.o_shon_rt',
            'batiments_compl.surt',
            'batiments_compl.ubat',
            'batiments_compl.hauteur_zone_max',
            'batiments_compl.princ_type_generateur_ch',
            'batiments_compl.princ_type_generateur_ecs',
            'batiments_compl.princ_type_generateur_fr',
            'batiments_compl.princ_energie_generateur_ch',
            'batiments_compl.princ_energie_generateur_ecs',
            'batiments_compl.princ_energie_generateur_fr',
            'batiments_compl.famille_generateur_ch',
            'batiments_compl.famille_generateur_ecs',
#            'batiments_compl.famille_generateur_fr',
            'batiments_compl.type_ventilation_principal',
            'batiments.cep_projet',
            'batiments.cep_max',
            'batiments.bbio_projet',
            'batiments.bbio_max',
            'batiments.aepenr',
            'batiments.aepenr/(batiments.aepenr + batiments.o_cep_ch + batiments.o_cep_ecs + batiments.o_cep_aux_vent)',
            'batiments_compl.s_surf_capt_PV']

    if usage_principal == 1:
        conditions=['operations.moteur_version >= "8.0.0.0"',
                    "batiments_compl.princ_type_generateur_ch != ''",
                    "batiments_compl.princ_type_generateur_ecs != ''",
                    'batiments.type_travaux = 1',
                    'batiments_compl.usage_principal = {}'.format(usage_principal),
                    'zones.usage = {}'.format(usage),
                    'operations.doublon = 0',
                    'operations.valide = 1',
                    'batiments.cep_projet > 0',
                    'batiments.bbio_projet > 0',
                    'batiments.aepenr >= 0',
                    'batiments.aepenr < 100']
        
    if usage_principal == 3:
        conditions=['operations.moteur_version >= "7.5.0.3"',
                    "batiments_compl.princ_type_generateur_ch != ''",
                    "batiments_compl.princ_type_generateur_ecs != ''",
                    'batiments.type_travaux = 1',
                    'batiments_compl.usage_principal = {}'.format(usage_principal),
                    'zones.usage = {}'.format(usage),
                    'operations.doublon = 0',
                    'operations.valide = 1',
                    'batiments.cep_projet > 0',
                    'batiments.bbio_projet > 0',
                    'batiments.aepenr >= 0',
                    'batiments.aepenr < 100']
        
    if usage_principal == 7:
        conditions=['operations.moteur_version >= "7.5.0.2"',
                    "batiments_compl.princ_type_generateur_ch != ''",
                    "batiments_compl.princ_type_generateur_ecs != ''",
                    'batiments.type_travaux = 1',
                    'batiments_compl.usage_principal = {}'.format(usage_principal),
                    'zones.usage = {}'.format(usage),
                    'operations.doublon = 0',
                    'operations.valide = 1',
                    'batiments.cep_projet > 0',
                    'batiments.bbio_projet > 0',
                    'batiments.aepenr >= 0',
                    'batiments.aepenr < 100']
    
    if usage_principal == 5:
        conditions=['operations.moteur_version >= "6.3.0.0"',
                    "batiments_compl.princ_type_generateur_ch != ''",
                    "batiments_compl.princ_type_generateur_ecs != ''",
                    'batiments.type_travaux = 1',
                    'batiments_compl.usage_principal = {}'.format(usage_principal),
                    'zones.usage in {}'.format(usage),
                    'operations.doublon = 0',
                    'operations.valide = 1',
                    'batiments.cep_projet > 0',
                    'batiments.bbio_projet > 0',
                    'batiments.aepenr >= 0',
                    'batiments.aepenr < 100']
        
    res_by_usage[usage_principal] = cn.auto_query(fields=fields,conditions=conditions)
    df = res_by_usage[usage_principal]
    print('{} --> len(df) = {}'.format(cn.value_to_meaning('usage_principal', usage_principal), len(df)))
    
    df_columns = df.columns
    myList = list(df_columns)
    col_to_del = []
    
    for index,col in enumerate(df_columns):
        if 'nb_gen' in col:
            myList[index] = 'nb_generations'
        if 'nb_bat' in col:
            myList[index] = 'nb_batiments'
        if 'nb_zones' in col:
            myList[index] = 'nb_zones'
        if 'moteur' in col:
            myList[index] = 'version_moteur'
        if 'zone_climatique' in col:
            myList[index] = 'zone_climatique'
        if 'altitude' in col:
            myList[index] = 'altitude'
        if 'categorie' in col:
            myList[index] = 'categorie_CE1_CE2'
        if 'surf_ce1' in col:
            myList[index] = 'surf_CE1'
        if 'surf_ce2' in col:
            myList[index] = 'surf_CE2'
        if 'surf_clim' in col:
            myList[index] = 'surf_clim'
        if 'ex_BR' in col:
            myList[index] = 'valeur_BR'
        if 'inertie' in col:
            myList[index] = 'inertie_quotidienne'
        if 'usage_principal' in col:
            myList[index] = 'usage_principal'
#        if 'zones.usage' in col:
#            myList[index] = 'usage_zone'
        if 'shon' in col:
            myList[index] = 'SHON_RT'
        if 'shab' in col:
            myList[index] = 'SHAB'
        if 'surt' in col:
            myList[index] = 'SURT'
        if 'ubat' in col:
            myList[index] = 'Ubat'
        if 'nb_log' in col:
            myList[index] = 'nb_logements'
        if 'hauteur_zone_max' in col:
            myList[index] = 'hauteur_zone_max'
        if 'princ_type_generateur_ch' in col:
            myList[index] = 'princ_type_generateur_ch'
        if 'princ_type_generateur_ecs' in col:
            myList[index] = 'princ_type_generateur_ecs'
        if 'princ_type_generateur_fr' in col:
            myList[index] = 'princ_type_generateur_fr'
        if 'princ_energie_generateur_ch' in col:
            myList[index] = 'princ_energie_generateur_ch'
        if 'princ_energie_generateur_ecs' in col:
            myList[index] = 'princ_energie_generateur_ecs'
        if 'princ_energie_generateur_fr' in col:
            myList[index] = 'princ_energie_generateur_fr'
        if 'famille_generateur_ch' in col:
            myList[index] = 'famille_generateur_ch'
        if 'famille_generateur_ecs' in col:
            myList[index] = 'famille_generateur_ecs'
        if 'famille_generateur_fr' in col:
            myList[index] = 'famille_generateur_fr'
        if 'ventilation' in col:
            myList[index] = 'type_ventilation_principal'
        if 'is_ascenseur' in col:
            myList[index] = 'is_ascenseur'
        if 'is_park' in col:
            myList[index] = 'is_park'
        if 'cep_projet' in col:
            myList[index] = 'cep_projet'
        if 'cep_max' in col:
            myList[index] = 'cep_max'
        if 'bbio_projet' in col:
            myList[index] = 'bbio_projet'
        if 'bbio_max' in col:
            myList[index] = 'bbio_max'
        if 'aepenr' in col:
            myList[index] = 'Aepenr'
        if '/' in col:
            myList[index] = 'RCR'
        if 's_surf_capt_PV' in col:
            myList[index] = 's_surf_capt_PV'
        if '_id' in col:
            col_to_del.append(col)
    
    df.columns = myList
    df_vf = df.drop(col_to_del, axis = 1, inplace = False)
    
    if type(usage) == tuple:
        usage_l = []
        for el in usage:
            usage_l.append(el)
        df_corr = tri_usage_batiments(df, usage_principal, usage_l)
        
    else:
        df_corr = tri_usage_batiments(df, usage_principal, usage)
    
    print('FINAL : {} --> len(df_corr) = {}'.format(cn.value_to_meaning('usage_principal', usage_principal), len(df_corr)))
    print('')
    
    res_by_usage_vf[usage_principal] = df_corr
        
    

# Filtrage des cas généraux et description des stats de chaque df
res_by_usage_filtre_by_bat_no_NAN = {}
stats={}
verif_val_cep = {}

for key in res_by_usage_vf.keys():
    data_init = res_by_usage_vf[key].copy()
    data_origin = res_by_usage_vf[key]
    
    data_without_nan = data_origin.dropna(inplace=False)
    data_by_zone_final = data_without_nan
    data_by_bat = data_by_zone_final.groupby('batiments.id').first()
    data_no_quant = filter_df_with_quantiles(data_by_bat, 'cep_projet', 0.02)
    
    res_by_usage_filtre_by_bat_no_NAN[key] = data_no_quant
    print(key, len(data_no_quant))
    
    stats[key] = data_no_quant.describe()
    verif_val_cep[key] = data_no_quant.describe()

def filter_complete_df_by_object(df, quantile_percent, groupby_method='batiments.id', filter_column=''):
    data_origin = df.copy()
    data_without_nan = data_origin.dropna(inplace=False)
    data_int = data_without_nan
    data_groupby = data_int.groupby(groupby_method).first()
    data_no_quant = filter_df_with_quantiles(data_groupby, filter_column, quantile_percent)
    return data_no_quant


# Ajout des colonnes explicitant le type de gen principal pour le ch et l'ecs et la combinaison
res_by_usage_filtre_by_bat_no_NAN_syst_expl = {}
counter_comb_syst_expl = {}

for key in res_by_usage_filtre_by_bat_no_NAN.keys():
#    if 5 in key or 7 in key:
#        continue
    df = res_by_usage_filtre_by_bat_no_NAN[key].copy()
    df['princ_type_generateur_ch_expl'] = 0
    df['princ_type_generateur_ecs_expl'] = 0
    if (key == 7 or key == 5):
        df['princ_type_generateur_fr_expl'] = 0
    df['is_PV'] = 0
    df['is_clim'] = 0
    
    
    for i in range(0, len(df)):
        # Clim
        if df.loc[df.index[i], 'surf_clim'] > 0:
            df.loc[df.index[i], 'is_clim'] = 1
        else:
            df.loc[df.index[i], 'is_clim'] = 0
            
        # PV
        if df.loc[df.index[i], 's_surf_capt_PV'] > 0:
            df.loc[df.index[i], 'is_PV'] = '-PV'
        else:
            df.loc[df.index[i], 'is_PV'] = ''
        
        # CH
        syst_ch = df.loc[df.index[i], 'princ_type_generateur_ch']
        if syst_ch not in dict_princ_type_gen.keys():
            df.loc[df.index[i], 'princ_type_generateur_ch_expl'] = syst_ch
        else:
            df.loc[df.index[i], 'princ_type_generateur_ch_expl'] = dict_princ_type_gen[syst_ch]
            
        # ECS
        syst_ecs = df.loc[df.index[i], 'princ_type_generateur_ecs']
        if syst_ecs not in dict_princ_type_gen.keys():
            df.loc[df.index[i], 'princ_type_generateur_ecs_expl'] = syst_ecs
        else:
            df.loc[df.index[i], 'princ_type_generateur_ecs_expl'] = dict_princ_type_gen[syst_ecs]
            
        # FR
        if (key == 7 or key == 5):
            syst_fr = df.loc[df.index[i], 'princ_type_generateur_fr']
            if syst_fr not in dict_princ_type_gen.keys():
                df.loc[df.index[i], 'princ_type_generateur_fr_expl'] = syst_fr
            else:
                df.loc[df.index[i], 'princ_type_generateur_fr_expl'] = dict_princ_type_gen[syst_fr]
        
        
    # Ajout d'une colonne explicitant la combinaison de système utilisé
    print(key)
    if (key == 1 or key == 3):
        df_final = df.assign(couple_systeme= df['princ_type_generateur_ch_expl'] + '-' + df['princ_type_generateur_ecs_expl'] + df['is_PV'])
    if (key == 7 or key == 5):
        df_final = df.assign(couple_systeme= df['princ_type_generateur_ch_expl'] + '-' + df['princ_type_generateur_ecs_expl'] + '-' + df['princ_type_generateur_fr_expl'] + df['is_PV'])
        
    counter_comb_syst_expl[key] = df_final['couple_systeme'].value_counts()
    df_vf = df_final.reset_index()
    res_by_usage_filtre_by_bat_no_NAN_syst_expl[key] = df_vf
    
def df_with_system_combinaison_and_PV(df, dict_princ_type_gen_expl, columns_to_use, with_PV = True):
    df_init = df.copy()
    df_init_columns = df_init.columns
    for column in columns_to_use:
        if column not in df_init_columns:
            return "ERREUR : la colonne {} n'est pas dans la liste des colonnes du dataframe df_init".format(column)
        else:
            df_init['{}_expl'.format(column)] = 0
            if 'type_generateur_ch' in column:
                column_type_gen_ch = column
            if 'type_generateur_ecs' in column:
                column_type_gen_ecs = column
            if 'type_generateur_fr' in column:
                column_type_gen_fr = column
            if 'energie_generateur_ch' in column:
                column_energie_gen_ch = column
            if 'energie_generateur_ecs' in column:
                column_energie_gen_ecs = column
            if 'energie_generateur_fr' in column:
                column_energie_gen_fr = column
    
    #PV et clim
    for col in df_init_columns:
        if ('s_surf_capt_PV' in col or 's_PC_PV' in col):
            df_init['is_PV'] = 0
        if 'surf_clim' in col:
            df_init['is_clim'] = 0
        else:
            return "ERREUR : il manque une colonne d'information à propos du PV"
    
    for i in range(0, len(df_init)):
        # Clim
        if df_init.loc[df_init.index[i], 'surf_clim'] > 0:
            df_init.loc[df_init.index[i], 'is_clim'] = 1
        else:
            df_init.loc[df_init.index[i], 'is_clim'] = 0
            
        # PV
        if df_init.loc[df_init.index[i], 's_surf_capt_PV'] > 0:
            df_init.loc[df_init.index[i], 'is_PV'] = '-PV'
        else:
            df_init.loc[df_init.index[i], 'is_PV'] = ''
        
        # CH
        # Principal type générateur ch
        if "{}_expl".format(column_type_gen_ch) in df_init_columns:
            syst_ch = df_init.loc[df_init.index[i], column_type_gen_ch]
            if syst_ch not in dict_princ_type_gen_expl.keys():
                df_init.loc[df_init.index[i], "{}_expl".format(column_type_gen_ch)] = syst_ch
            else:
                df_init.loc[df_init.index[i], "{}_expl".format(column_type_gen_ch)] = dict_princ_type_gen_expl[syst_ch]
            
        # Principale énergie générateur ch
        if "{}_expl".format(column_energie_gen_ch) in df_init_columns:
            energie_ch = df_init.loc[df_init.index[i], column_energie_gen_ch]
            df_init.loc[df_init.index[i], "{}_expl".format(column_energie_gen_ch)] = energie_ch
            
        # ECS
        # Principal type générateur ecs
        if "{}_expl".format(column_type_gen_ecs) in df_init_columns:
            syst_ecs = df_init.loc[df_init.index[i], column_type_gen_ecs]
            if syst_ecs not in dict_princ_type_gen_expl.keys():
                df_init.loc[df_init.index[i], "{}_expl".format(column_type_gen_ecs)] = syst_ecs
            else:
                df_init.loc[df_init.index[i], "{}_expl".format(column_type_gen_ecs)] = dict_princ_type_gen_expl[syst_ecs]
            
        # Principale énergie générateur ecs
        if "{}_expl".format(column_energie_gen_ecs) in df_init_columns:
            energie_ecs = df_init.loc[df_init.index[i], column_energie_gen_ecs]
            df_init.loc[df_init.index[i], "{}_expl".format(column_energie_gen_ecs)] = energie_ecs
            
        # FR
        # Principal type générateur fr
        if df_init.loc[df_init.index[i], 'is_clim'] == 1:
            if "{}_expl".format(column_type_gen_fr) in df_init_columns:
                syst_fr = df_init.loc[df_init.index[i], column_type_gen_fr]
                if syst_fr not in dict_princ_type_gen_expl.keys():
                    df_init.loc[df_init.index[i], "{}_expl".format(column_type_gen_fr)] = "-" + syst_fr
                else:
                    df_init.loc[df_init.index[i], "{}_expl".format(column_type_gen_fr)] = "-" + dict_princ_type_gen_expl[syst_fr]
        else:
             df_init.loc[df_init.index[i], "{}_expl".format(column_type_gen_fr)] = ""
             
        # Principale énergie générateur fr
        if df_init.loc[df_init.index[i], 'is_clim'] == 1:
            if "{}_expl".format(column_energie_gen_fr) in df_init_columns:
                energie_fr = df_init.loc[df_init.index[i], column_energie_gen_fr]
                df_init.loc[df_init.index[i], "{}_expl".format(column_energie_gen_fr)] = "-" + energie_fr
        else:
             df_init.loc[df_init.index[i], "{}_expl".format(column_energie_gen_fr)] = ""
            
    if with_PV == True:
        df_syst = df.assign(couple_systeme = df["{}_expl".format(column_type_gen_ch)] + '-' + df["{}_expl".format(column_type_gen_ecs)] + df["{}_expl".format(column_type_gen_fr)] + df['is_PV'])
    else:
        df_syst = df.assign(couple_systeme = df["{}_expl".format(column_type_gen_ch)] + '-' + df["{}_expl".format(column_type_gen_ecs)] + df["{}_expl".format(column_type_gen_fr)])
    
    df_syst_et_energie = df_syst.assign(couple_energie = df["{}_expl".format(column_energie_gen_ch)] + '-' + df["{}_expl".format(column_energie_gen_ecs)] + df["{}_expl".format(column_energie_gen_fr)])   
    df_final = df_syst_et_energie.reset_index()
    
    return df_final


# Sauvegarde des dataframes finaux avec couple systeme
for key in res_by_usage_filtre_by_bat_no_NAN_syst_expl.keys():
    df = res_by_usage_filtre_by_bat_no_NAN_syst_expl[key]
    print(key)
    df.to_csv(r"C:\Users\MOREIRA\OneDrive - CSTBGroup\2019\COTEC_RSLAB\RT2012_simulation_OPE\dataframes_syst_COTEC\{}".format(key))
    
    
# Lecture des dataframes globaux filtrés avec couple système
res_usage_vf = {}
for csv in os.listdir(r"C:\Users\MOREIRA\OneDrive - CSTBGroup\2019\COTEC_RSLAB\RT2012_simulation_OPE\dataframes_syst_COTEC"):
    df = pd.read_csv(r"C:\Users\MOREIRA\OneDrive - CSTBGroup\2019\COTEC_RSLAB\RT2012_simulation_OPE\dataframes_syst_COTEC\{}".format(csv), engine='python', encoding='utf-8')
    df_vf = df.reset_index(drop = True)
    new_key = int(csv)
    df2 = df_vf.drop(columns='Unnamed: 0')
    res_usage_vf[new_key] = df2


# Création du df contenant toutes les configurations de systèmes et leur nombre, proportions d'altitude et de zone climatique
def proportion_param_echantillon(df, column_name, nb_lignes_ech):
    df_proportion_counter = df[column_name].value_counts()
    df_new_ech = pd.Series()
    nb_lignes_df = df_proportion_counter.sum()
    index_value_maj = 0
    
    for index in df_proportion_counter.index:
        index_value = df_proportion_counter[index]
        proportion_index = index_value/nb_lignes_df
        x = round(nb_lignes_ech * proportion_index)
        df_new_ech[index] = x
        if index_value_maj < index_value:
            index_value_maj = index_value
            index_maj = index
    
    if df_new_ech.sum() < nb_lignes_ech:
        df_new_ech[index_maj] = df_new_ech[index_maj] + 1
    if df_new_ech.sum() > nb_lignes_ech:
        df_new_ech[index_maj] = df_new_ech[index_maj] - 1
        
    return df_new_ech
        
def new_random_echantillon(df, column_name, nb_lignes_ech):
    compteur_taille_ech = 0
    index_df = []
    proportion_counter_new_ech = {}
    counter_by_param = {}
    df_int = pd.Series()
    df_final = pd.DataFrame()
    if type(column_name) == list:
        for col in column_name:
            proportion_counter_new_ech[col] = proportion_param_echantillon(df, col, nb_lignes_ech)
            df_int = pd.concat([df_int, proportion_counter_new_ech[col]])
            
    else:
        proportion_counter_new_ech[column_name] = proportion_param_echantillon(df, column_name, nb_lignes_ech)
        df_int = pd.concat([df_int, proportion_counter_new_ech[column_name]])
        
    for index in df_int.index:
        counter_by_param[index] = 0
        
    for i in range(0, len(df)):
        index_df.append(i)
        
    while (compteur_taille_ech < nb_lignes_ech-1):
        print(counter_by_param)
        nb_row = choice(index_df)
        print(compteur_taille_ech)
        print(df_int)
        df_random_row = df.loc[[nb_row], :]
        df_filter = df.drop([nb_row], axis = 0, inplace=False)
        alt = df_random_row['altitude'][nb_row]
        zc = df_random_row['zone_climatique'][nb_row]
        if (counter_by_param[alt] >= df_int[alt] and counter_by_param[zc] >= df_int[zc]):
            continue
        if (counter_by_param[alt] < df_int[alt] and counter_by_param[zc] < df_int[zc]):
            index_df.remove(nb_row)
            counter_by_param[alt] = counter_by_param[alt] + 1
            counter_by_param[zc] = counter_by_param[zc] + 1
                
            df_final = pd.concat([df_final, df_random_row])
            compteur_taille_ech = compteur_taille_ech + 1
#            print('altitude : {}'.format(counter_by_param[alt]))
#            print('zone_climatique : {}'.format(counter_by_param[zc]))
        
    print(counter_by_param)
    return df_final
    
    
liste_nb_systeme = {}
counter_comb_syst_expl_with_autres = {}
i=0
index_for_comb_syst_with_autres = dict()
def repartition_princ_comb_syst(df, column_name, boundary_value):
    if 'syst' in column_name:
        df_counter_comb_syst_expl = df[column_name].value_counts()
    else:
        return "ERREUR dans la saisie du paramètre 'column_name'" 
    
    s_syst_autres = 0
    compteur_ligne_repart = 1
    df = df_counter_comb_syst_expl.copy()
    nb_batiments = df.sum()
    
    for ind in df.index:
        nb_syst = df[ind]
        if (boundary_value > 1 and type(boundary_value) == int):
            if compteur_ligne_repart > boundary_value:
                s_syst_autres = s_syst_autres + nb_syst
                df.drop(index=ind, inplace=True)
                compteur_ligne_repart = compteur_ligne_repart + 1
            else:
                compteur_ligne_repart = compteur_ligne_repart + 1
                continue
        else:
            print(nb_syst/nb_batiments)
            if (nb_syst/nb_batiments) < boundary_value:
                s_syst_autres = s_syst_autres + nb_syst
                df.drop(index=ind, inplace=True)
            else:
                continue
        
    df_autres = pd.Series([s_syst_autres], index=['Autres'])
    df_final = df.append(df_autres)
    
    return df_final


df_cas_non_valables = pd.read_csv(r"C:\Users\MOREIRA\OneDrive - CSTBGroup\2019\COTEC_RSLAB\RT2012_simulation_OPE\cas_tests\cas_non_valables_par_usage", engine='python', encoding='utf-8')
keys = [1, 3, 5, 7]
dict_xml_non_valables = {}
for cle in keys:
    if cle == 1:
        dict_xml_non_valables[cle] = ['2153351.xml', '2153634.xml', '2153665.xml', '2153752.xml',
        '2153752.xml', '2153846.xml', '2153891.xml', '2154160.xml',
        '2154165.xml', '2154223.xml', '2154345.xml', '2154394.xml',
        '2154438.xml', '2154464.xml', '2154581.xml', '2154958.xml',
        '2155012.xml', '2155069.xml', '2155128.xml', '2155268.xml',
        '2155281.xml', '2155361.xml', '2155370.xml', '2155481.xml',
        '2155508.xml', '2155806.xml', '2155835.xml', '2155915.xml',
        '2155968.xml', '2156408.xml', '2156413.xml', '2156416.xml',
        '2156420.xml', '2156424.xml', '2156490.xml', '2156491.xml',
        '2156492.xml', '2156493.xml', '2156494.xml', '2156565.xml',
        '2156669.xml', '2156897.xml', '2157013.xml', '2157048.xml',
        '2157101.xml', '2157127.xml', '2157563.xml', '2157616.xml',
        '2157617.xml', '2157630.xml', '2157640.xml', '2157661.xml',
        '2157669.xml', '2157689.xml', '2157820.xml', '2157954.xml',
        '2158237.xml', '2158390.xml', '2158533.xml', '2158578.xml',
        '2158986.xml', '2159366.xml', '2159598.xml', '2159607.xml',
        '2159607.xml', '2159607.xml', '2159721.xml', '2160011.xml',
        '2160022.xml', '2160158.xml', '2160160.xml', '2160213.xml',
        '2160234.xml', '2160238.xml', '2160637.xml', '2160958.xml',
        '2161351.xml', '2161384.xml', '2161406.xml', '2161577.xml',
        '2161598.xml', '2161613.xml', '2161619.xml', '2161670.xml',
        '2161725.xml', '2161726.xml', '2161746.xml', '2161807.xml',
        '2161905.xml', '2161943.xml', '2161963.xml', '2161969.xml',
        '2162054.xml', '2162163.xml', '2162362.xml', '2163311.xml',
        '2163364.xml', '2163524.xml', '2163578.xml', '2163812.xml',
        '2163816.xml', '2164062.xml', '2164275.xml', '2164363.xml',
        '2164413.xml', '2164492.xml', '2164576.xml', '2164794.xml',
        '2164830.xml', '2164833.xml', '2165080.xml', '2165133.xml',
        '2165133.xml', '2165144.xml', '2165152.xml', '2165275.xml',
        '2165397.xml', '2165518.xml', '2165550.xml', '2165639.xml',
        '2165799.xml', '2165817.xml', '2165840.xml', '2166009.xml',
        '2166055.xml', '2166082.xml', '2166092.xml', '2166195.xml',
        '2166290.xml', '2166306.xml', '2166392.xml', '2166457.xml',
        '2166503.xml', '2166597.xml', '2166621.xml', '2166627.xml',
        '2166644.xml', '2166663.xml', '2166666.xml', '2166809.xml',
        '2166815.xml', '2166898.xml', '2166924.xml', '2166956.xml',
        '2166959.xml', '2167390.xml', '2167693.xml', '2167788.xml',
        '2167801.xml', '2167828.xml', '2167983.xml', '2168310.xml',
        '2168421.xml', '2168568.xml', '2168647.xml', '2168656.xml',
        '2168924.xml', '2168950.xml', '2169079.xml', '2169122.xml',
        '2169141.xml', '2169151.xml', '2169719.xml', '2170224.xml',
        '2170261.xml', '2170290.xml', '2170457.xml', '2170542.xml',
        '2170549.xml', '2170567.xml', '2170568.xml', '2170572.xml',
        '2170573.xml', '2170598.xml', '2170653.xml', '2170657.xml',
        '2170667.xml', '2170710.xml', '2170724.xml', '2170727.xml',
        '2170772.xml', '2170808.xml', '2171075.xml', '2171129.xml',
        '2171141.xml', '2171148.xml', '2171157.xml', '2171230.xml',
        '2171335.xml', '2171336.xml', '2171348.xml', '2171360.xml',
        '2171366.xml', '2171370.xml', '2281064.xml']
    if cle == 3:
        dict_xml_non_valables[cle] = ['2153477.xml', '2154900.xml', '2156102.xml', '2157821.xml',
        '2157832.xml', '2158595.xml', '2160736.xml', '2161143.xml',
        '2161144.xml', '2161145.xml', '2162344.xml', '2162539.xml',
        '2162905.xml', '2162994.xml', '2165485.xml', '2165997.xml',
        '2167574.xml', '2168289.xml', '2168289.xml', '2168720.xml',
        '2168720.xml', '2169103.xml', '2169219.xml', '2169329.xml',
        '2169767.xml', '2169767.xml', '2169767.xml', '2169767.xml',
        '2169767.xml', '2169767.xml', '2170216.xml']
    if cle == 5:
        dict_xml_non_valables[cle] = ['1236227.xml', '1486575.xml', '1873047.xml', '1876318.xml',
        '1994186.xml', '1994245.xml', '2155882.xml', '2156674.xml',
        '2157570.xml', '2157684.xml', '2157770.xml', '2158541.xml',
        '2160235.xml', '2160509.xml', '2164458.xml', '2168286.xml',
        '2170331.xml']
    if cle == 7:
        dict_xml_non_valables[cle] = ['1872850.xml', '1873727.xml', '1874516.xml', '1876083.xml',
        '1879523.xml', '1879670.xml', '1880198.xml', '1880805.xml',
        '1991601.xml', '1991853.xml', '1992773.xml', '1993390.xml',
        '1993569.xml', '1993626.xml', '1993752.xml', '2154593.xml',
        '2154926.xml', '2155179.xml', '2155916.xml', '2157146.xml',
        '2157466.xml', '2157508.xml', '2159167.xml', '2159346.xml',
        '2160115.xml', '2160239.xml', '2160736.xml', '2163448.xml',
        '2164312.xml', '2164516.xml', '2166066.xml', '2166485.xml',
        '2167187.xml', '2167773.xml', '2168889.xml', '2168901.xml',
        '2169163.xml', '2169589.xml', '2170347.xml', '2170368.xml',
        '2170853.xml']

dict_df_par_usage_final = {}
for key in dict_xml_non_valables.keys():
    df = res_usage_vf[key]
    df_final = df.copy()
    list_xml = dict_xml_non_valables[key]
    for i in range(0, len(df)):
        xml = df.loc[df.index[i], 'operations.xml']
        if xml in list_xml:
            df_final.drop([i], axis = 0, inplace=True)
    dict_df_par_usage_final[key] = df_final
    print(len(df_final))
    
# Sauvegarde des dataframes finaux avec couple systeme
for key in dict_df_par_usage_final.keys():
    df = dict_df_par_usage_final[key]
    print(key)
    df.to_csv(r"C:\Users\MOREIRA\OneDrive - CSTBGroup\2019\COTEC_RSLAB\RT2012_simulation_OPE\dataframes_syst_COTEC\{}_final".format(key))

# Lecture des dataframes globaux filtrés avec couple système et operations non valides
df_par_usage_final = {}
for csv in os.listdir(r"C:\Users\MOREIRA\Documents\COTEC"):
    print(csv)
    df = pd.read_csv(r"C:\Users\MOREIRA\Documents\COTEC\{}".format(csv), engine='python', encoding='utf-8')
    df_vf = df.reset_index(drop = True)
    df2 = df_vf.drop(columns='Unnamed: 0')
    df_par_usage_final[csv] = df2
    
    
for key in df_par_usage_final.keys():
    df=df_par_usage_final[key]
    print(key)
    if ('ENS' in key or 'BUR' in key):
        print("GLOBAL")
        print(repartition_princ_comb_syst(df, 'couple_systeme', 4))
        print("NON_CLIM")
        df_non_clim = df.loc[df['is_clim'] == 0]
        print(repartition_princ_comb_syst(df_non_clim, 'couple_systeme', 4))
        print("CLIM")
        df_clim = df.loc[df['is_clim'] == 1]
        print(repartition_princ_comb_syst(df_clim, 'couple_systeme', 4))
    else:
        print(repartition_princ_comb_syst(df, 'couple_systeme', 4))
    print('')
        
# Echantillonage final des dataframes pour chaque usage
#res_usage_vf_ech_vf = {}
#for key in res_usage_vf.keys():
#    df = res_usage_vf[key]
#    if key == 1:
#        df_final = new_random_echantillon(df, ['altitude', 'zone_climatique'], 10000)
#    elif key == 5:
#        continue
#    else:
#        df_final = new_random_echantillon(df, ['altitude', 'zone_climatique'], 1000)
#    res_usage_vf_ech_vf[key] = df_final
#    df_final.to_csv(r"C:\Users\MOREIRA\OneDrive - CSTBGroup\2019\COTEC_RSLAB\RT2012_simulation_OPE\dataframes_syst_COTEC\{}_ech_final".format(key))
#    
#pathProjet = resource_filename('pycometh', 'tests/sample_data/rt2012_1.xml') # loading an rt2012 project from the pycometh test folder
#
#rt2012 = Rt2012(pathProjet)


#not_available_cases_by_usage = {}
#for key in res_usage_vf.keys():
#    not_available_cases = []
#    df = res_usage_vf[key]
#    print(key, len(df))
#    if key == 1:
#        path = r"C:\Users\MOREIRA\OneDrive - CSTBGroup\2019\COTEC_RSLAB\RT2012_simulation_OPE\cas_tests\mi_SM"
#    if key == 3:
#        path = r"C:\Users\MOREIRA\OneDrive - CSTBGroup\2019\COTEC_RSLAB\RT2012_simulation_OPE\cas_tests\lc_SM"
#    if key == 7:
#        path = r"C:\Users\MOREIRA\OneDrive - CSTBGroup\2019\COTEC_RSLAB\RT2012_simulation_OPE\cas_tests\bur_SM"
#    if key == 5:
#        path = r"C:\Users\MOREIRA\OneDrive - CSTBGroup\2019\COTEC_RSLAB\RT2012_simulation_OPE\cas_tests\ens_SM"
#    for i in range(0, len(df)):
#        xml = df.loc[df.index[i], 'operations.xml']
#        if cn.is_rset_rsee_available_from_sever(xml) == True:
#            cn.get_xml_from_server(xml, path)
#        else:
#            not_available_cases.append(xml)
#            continue
#    not_available_cases_by_usage[key] = not_available_cases
#    
#df_xml_not_available = pd.DataFrame.from_dict(not_available_cases_by_usage, orient='index')
#df_xml_not_available.to_csv(r"C:\Users\MOREIRA\OneDrive - CSTBGroup\2019\COTEC_RSLAB\RT2012_simulation_OPE\cas_tests\cas_non_valables_par_usage")
    
    