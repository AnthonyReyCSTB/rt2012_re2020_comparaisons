#=========================================
# coding = utf-8
#=========================================
# Name         : analyze_rt2012_re2020.py
# Author       : Anthony Rey
# Version      : 1.0.0.0
# Date         : 09/10/2019
# Update       : 23/10/2019
# Description  : Analyze the RT2012 and RE2020 simulations (re-simulated xml cases)
#========================================

#========================================
# Links
#========================================

#

#========================================
# Packages
#========================================

# Python core packages/libraries/modules
try:
    import os
except ImportError:
    raise ImportError("Sorry, but the \"os\" module/package is required to run this program!")
try:
    import pandas as pd
except ImportError:
    raise ImportError("Sorry, but the \"pandas\" module/package is required to run this program!")
try:
    import copy
except ImportError:
    raise ImportError("Sorry, but the \"copy\" module/package is required to run this program!")
try:
    import collections
except ImportError:
    raise ImportError("Sorry, but the \"collections\" module/package is required to run this program!")

# Python extension packages/libraries/modules
try:
    import pycometh
except ImportError:
    raise ImportError("Sorry, but the \"pycometh\" module/package is required to run this program!")
try:
    from analyzer import Analyzer
except ImportError:
    raise ImportError("Sorry, but the \"analyzer\" module/package is required to run this program!")
try:
    from plot_handler import PlotHandler
except ImportError:
    raise ImportError("Sorry, but the \"plot_handler\" module/package is required to run this program!")


#========================================
# Classes
#========================================

#========================================
# Functions
#========================================

def clean_file(file_path):
    with open(file_path, 'w') as file_descriptor:
        file_descriptor.write("")

def save_in_file(file_path, text_to_be_saved):
    with open(file_path, 'a') as file_descriptor:
        file_descriptor.write(text_to_be_saved)

def add_engine_version(dataframe, version_number):
    dataframe["version"] = version_number
    return dataframe

def clean_engine_version(dataframe, version_number_key, version_number):
    dataframe[version_number_key] = version_number
    return dataframe

def clean_different_number_of_buildings(dataframe_one, dataframe_two, column_name):
    # Get all files which are different
    indices = []
    for key, value in collections.Counter(dataframe_two[column_name]).items():
        if value != len(dataframe_one[dataframe_one[column_name] == key]):
            indices.append(key)
    # Remove all of them
    dataframe_one = dataframe_one[~dataframe_one[column_name].isin(indices)]
    dataframe_two = dataframe_two[~dataframe_two[column_name].isin(indices)]
    return dataframe_one, dataframe_two

def update_indices(dataframe_one, dataframe_two, column_name = None):
    dataframe_one.reset_index(inplace = True, drop = True)
    dataframe_two.reset_index(inplace = True, drop = True)
    if column_name is not None:
        dataframe_one["directory_one"] = dataframe_one[column_name]
        dataframe_two["directory_two"] = dataframe_two[column_name]
        del dataframe_one[column_name]
        del dataframe_two[column_name]
    return dataframe_one, dataframe_two

def merge(dataframe_one, dataframe_two):
    dataframe = dataframe_one.merge(dataframe_two, left_on = "directory_one", right_on = "directory_two")
    return dataframe

def filter_dataframe_with_quantiles(dataframe, column, percent = 0.05):
    quantile_min    = percent
    quantile_max    = 1 - percent
    quantiles       = dataframe[column].quantile([quantile_min, quantile_max])
    value_min       = quantiles[quantile_min]
    value_max       = quantiles[quantile_max]
    mask            = (dataframe[column] >= value_min) & (dataframe[column] <= value_max)
    dataframe       = dataframe.loc[mask]
    return dataframe

def remove_rows_on_condition(dataframe_one, dataframe_two, condition_column):
    dataframe_one           = copy.deepcopy(dataframe_one)
    dataframe_two           = copy.deepcopy(dataframe_two)
    dataframe_one_temporary = copy.deepcopy(dataframe_one)
    dataframe_two_temporary = copy.deepcopy(dataframe_two)
    dataframe_one.drop(dataframe_one[~dataframe_one_temporary[condition_column].isin(dataframe_two_temporary[condition_column])].index, inplace = True)
    dataframe_two.drop(dataframe_two[~dataframe_two_temporary[condition_column].isin(dataframe_one_temporary[condition_column])].index, inplace = True)
    return dataframe_one, dataframe_two

def first_indicator_builder(dataframe, indicators):
    # Create empty dictionary
    dictionary = dict()
    # For each indicator, get data
    for key in collections.Counter(dataframe[indicators[0]]):
        dictionary[key] = copy.deepcopy(dataframe[dataframe[indicators[0]] == key][indicators[1]])
    return dictionary

def second_indicator_builder(dataframe, indicators):
    # Create empty dictionary
    dictionary = dict()
    # For each indicator, get data
    for item in indicators:
        for key in collections.Counter(dataframe[item[0]]):
            name = str(key) + "_" + str(set(dataframe[item[1]]).pop())
            dictionary[name] = copy.deepcopy(dataframe[dataframe[item[0]] == key][item[2]])
    return collections.OrderedDict(sorted(dictionary.items(), reverse = True))

def third_indicator_builder(dataframe, indicators):
    # Create empty dictionary
    dictionary = dict()
    # Create a list of the main group of systems
    dicitonary_groups           = collections.Counter(dataframe[indicators[0]])
    ordered_dicitonary_groups   = collections.OrderedDict(sorted(dicitonary_groups.items(), key = lambda item: item[1], reverse = True))
    items                       = list(ordered_dicitonary_groups)
    # For the most common items
    for item in items[:5]:
        dictionary[item]                 = dict()
        dictionary[item][indicators[1]]  = copy.deepcopy(dataframe[dataframe[indicators[0]] == item][indicators[1]])
        dictionary[item][indicators[2]]  = copy.deepcopy(dataframe[dataframe[indicators[0]] == item][indicators[2]])
    return dictionary

def fourth_indicator_builder(dataframe, indicators):
    # Create empty dictionary
    dictionary = dict()
    # For each indicator, get data
    for usage in collections.Counter(dataframe[indicators[0]]):
        for option in ["RT2012", "RT/RE"]:
            name = str(usage) + "_" + str(option)
            if option == "RT2012":
                dictionary[name] = copy.deepcopy(dataframe[dataframe[indicators[0]] == usage][indicators[3]])
            else:
                dataframe_ = copy.deepcopy(dataframe)
                for index, row in dataframe_.iterrows():
                    if row[indicators[0]] == usage:
                        dataframe_.loc[index, indicators[3]] = row[indicators[3]] * row[indicators[1]] / row[indicators[2]]
                dictionary[name] = copy.deepcopy(dataframe_[dataframe_[indicators[0]] == usage][indicators[3]])
    return collections.OrderedDict(sorted(dictionary.items(), reverse = True))

def fifth_indicator_builder(dataframe_one, dataframe_two, indicators):
    # Create empty dictionary
    dictionary = dict()
    # For each indicator, get data
    for usage in collections.Counter(dataframe_one[indicators[0]]):
        for option in ["RT2012", "RE2020"]:
            name = str(usage) + "_" + str(option)
            if option == "RT2012":
                dictionary[name] = copy.deepcopy(dataframe_one[dataframe_one[indicators[0]] == usage][indicators[1]])
            else:
                dictionary[name] = copy.deepcopy(dataframe_two[dataframe_two[indicators[0]] == usage][indicators[1]])
    return collections.OrderedDict(sorted(dictionary.items(), reverse = True))

def sixth_indicator_builder(dataframe_one, dataframe_two, indicators):
    # Create empty dictionary
    dictionary = dict()
    # For each indicator, get data
    for usage in collections.Counter(dataframe_one[indicators[0]]):
        for option in ["RE2020", "RE2020 METEO"]:
            name = str(usage) + "_" + str(option)
            if option == "RE2020":
                dictionary[name] = copy.deepcopy(dataframe_one[dataframe_one[indicators[0]] == usage][indicators[1]])
            else:
                dictionary[name] = copy.deepcopy(dataframe_two[dataframe_two[indicators[0]] == usage][indicators[1]])
    return collections.OrderedDict(sorted(dictionary.items(), reverse = True))

def seventh_indicator_builder(dataframe_one, dataframe_two, indicators):
    # Create empty dictionary
    dictionary = dict()
    # For each indicator, get data
    for usage in collections.Counter(dataframe_one[indicators[0]]):
        for option in ["RE2020", "RE2020 CLIM FICTIVE"]:
            name = str(usage) + "_" + str(option)
            if option == "RE2020":
                dictionary[name] = copy.deepcopy(dataframe_one[dataframe_one[indicators[0]] == usage][indicators[1]])
            else:
                dictionary[name] = copy.deepcopy(dataframe_two[dataframe_two[indicators[0]] == usage][indicators[1]])
    return collections.OrderedDict(sorted(dictionary.items(), reverse = True))

def eighth_indicator_builder(dataframe_one, dataframe_two, indicators):
    # Create empty dictionary
    dictionary = dict()
    # Create a list of the main group of systems
    dicitonary_groups               = collections.Counter(dataframe_one[indicators[0]])
    ordered_dicitonary_groups       = collections.OrderedDict(sorted(dicitonary_groups.items(), key = lambda item: item[1], reverse = True))
    items                           = list(ordered_dicitonary_groups)
    dataframe_one, dataframe_two    = clean_different_number_of_buildings(dataframe_one, dataframe_two, "operations.xml")
    dataframe_one, dataframe_two    = update_indices(dataframe_one, dataframe_two)
    # For the most common items
    for item in items[:5]:
        dictionary[item]                 = dict()
        dictionary[item][indicators[1]]  = copy.deepcopy(dataframe_one[dataframe_one[indicators[0]] == item][indicators[1]])
        dictionary[item][indicators[2]]  = copy.deepcopy(2.3 * dataframe_two[dataframe_one[indicators[0]] == item][indicators[2]])
    return dictionary



def main():
    print("\nSTART\n")

    # Parameters
    directory_path              = "..//"
    directory_simulations       = "cases"
    directory_data              = "data"
    case_studies                = ["rt2012", "re2020", "re2020_meteo", "re2020_mode_d", "re2020_clim_fictive"]
    building_characteristics    = {"rt2012": [("cep",  "/Sortie_Batiment_C/O_Cep_annuel", "kWh/(m².an)"),
                                              ("bbio", "/Sortie_Batiment_B/O_Bbio_pts_annuel", "-"),
                                              ("degh", "/Sortie_Groupe_D/O_NbDegresHeures", "°C.h"),
                                              ("co2",  "post_processing.get_CO2_rt2012_by_bat", "kg eq CO2/(kWh.m²)"),
                                              ("rcr",  "post_processing.get_rcr_rt2012", "-"),
                                              ("energie_principale", "post_processing.get_main_energy_per_building", "-"),
                                              ("shab", "post_processing.get_shab_per_building", "m²"),
                                              ("shon", "post_processing.get_shon_rt_per_building", "m²"),
                                              ("surt", "post_processing.get_surt_per_building", "m²")],
                                   "re2020": [("cep", "/Sortie_Batiment_C/O_Cep_annuel", "kWh/(m².an)"),
                                              ("bbio", "/Sortie_Batiment_B/O_Bbio_pts_annuel", "-"),
                                              ("bbio_froid", "/Sortie_Batiment_B/O_B_Fr_pts_annuel", "-"),
                                              ("degh", "/Sortie_Groupe_D/O_NbDegresHeures", "°C.h"),
                                              ("co2", "post_processing.get_CO2_rt2012_by_bat", "kg eq CO2/(kWh.m²)"),
                                              ("rcr", "post_processing.get_rcr", "-"),
                                              ("energie_principale", "post_processing.get_main_energy_per_building", "-")],
                                   "re2020_meteo": [("cep", "/Sortie_Batiment_C/O_Cep_annuel", "kWh/(m².an)"),
                                              ("bbio", "/Sortie_Batiment_B/O_Bbio_pts_annuel", "-"),
                                              ("degh", "/Sortie_Groupe_D/O_NbDegresHeures", "°C.h"),
                                              ("co2", "post_processing.get_CO2_rt2012_by_bat", "kg eq CO2/(kWh.m²)"),
                                              ("rcr", "post_processing.get_rcr", "-"),
                                              ("energie_principale", "post_processing.get_main_energy_per_building", "-")],
                                   "re2020_mode_d": [("degh", "/Sortie_Groupe_D/O_NbDegresHeures", "°C.h") ],
                                   "re2020_clim_fictive":[("cep", "/Sortie_Batiment_C/O_Cep_annuel", "kWh/(m².an)"),
                                              ("cef_froid", "/Sortie_Batiment_C/O_Cef_imp_fr_annuel", "kWh/(m².an)"),
                                              ("rcr", "post_processing.get_rcr", "-"),
                                              ("energie_principale", "post_processing.get_main_energy_per_building", "-")]
                                  }
    column_names                = {"rt2012": ["index", "bbio", "cep", "co2", "degh", "directory", "energie_principale", "rcr", "shab", "shon", "surt", "usage"],
                                   "re2020": ["index", "bbio", "bbio_froid", "cep", "co2", "degh", "directory", "energie_principale", "rcr", "usage"],
                                   "re2020_meteo": ["index", "bbio", "cep", "co2", "degh", "directory", "energie_principale", "rcr", "usage"],
                                   "re2020_mode_d": ["index", "degh", "directory", "usage"],
                                   "re2020_clim_fictive": ["index", "cef_froid", "cep", "directory", "energie_principale", "rcr", "usage"],
                                  }
    usages                      = [("mi", "/Sortie_Zone_C/Usage", [1]),
                                   ("lc", "/Sortie_Zone_C/Usage", [2]),
                                   ("bu", "/Sortie_Zone_C/Usage", [16]),
                                   ("ens", "/Sortie_Zone_C/Usage", [4,5,6,7])]
    usage_keys                  = {"mi":"Maisons individuelles",
                                   "lc":"Logements collectifs",
                                   "bu":"Bureaux",
                                   "ens":"Etablissements d'enseignement"}
    systems                     = {"Maisons individuelles":["PAC_DS-PAC_DS", "PAC_air_ext/air_recy-CET", "Poele/insert-CET", "Chaud_gaz_cond-CET"],
                                   "Logements collectifs": ["Chaud_gaz_cond-Chaud_gaz_cond", "Chaud_gaz_cond-B_chaud_gaz_cond", "Res_ch-B_res_ch", "Gen_EJ-CET"],
                                   "Bureaux": ["PAC_rev-B_EJ-PAC_rev", "Syst_cond_air_DRV-B_EJ-Syst_cond_air_DRV", "PAC_air_ext/air_recy-B_EJ-", "Gen_EJ-B_EJ-"],
                                   "Etablissements d'enseignement": ["Chaud_gaz_cond-B_EJ-", "Chaud_gaz_cond-B_chaud_gaz_cond-", "PAC_air_ext/air_recy-B_EJ-", "Gen_EJ-B_EJ-"],
    }

    # Tell whether or not the results should be read or computed
    by_pass = {case_study : len(os.listdir(os.path.join(directory_path, directory_data, case_study, "dataframes_outputs"))) > 0 for case_study in case_studies}

    data_dictionary = dict()
    for case_study in case_studies:
        data_dictionary[case_study] = dict()

        # Get information from dataframes (information before being simulated again)
        input_dictionary = dict()
        for usage in usages:
            dataframe_path = os.path.join(directory_path, directory_data, case_study, "dataframes_inputs", "dataframe_" + usage[0] + ".csv")
            input_dictionary[usage[0]] = pd.read_csv(dataframe_path)
            input_dictionary[usage[0]] = input_dictionary[usage[0]][["operations.xml", "couple_systeme", "version_moteur", "cep_projet", "cep_max", "bbio_projet", "bbio_max"]]
        data_dictionary[case_study]["inputs"] = copy.deepcopy(input_dictionary)

        # Build analyzer
        directory_outputs       = os.path.join(directory_path, directory_simulations, case_study, "outputs")
        analyzer_rt2012_re2020  = Analyzer(directory_outputs, building_characteristics[case_study], usages)

        # Read (from dataframe) or compute results (from experiment plan)
        if by_pass[case_study] is True:
            output_dictionary = dict()
            for usage in usages:
                dataframe_path = os.path.join(directory_path, directory_data, case_study, "dataframes_outputs", "dataframe_" + usage[0] + ".csv")
                output_dictionary[usage[0]] = pd.read_csv(dataframe_path)
                output_dictionary[usage[0]] = output_dictionary[usage[0]][column_names[case_study]]
                output_dictionary[usage[0]]["directory"] = output_dictionary[usage[0]]["directory"].apply(lambda element: str(element))
                output_dictionary[usage[0]].set_index("index", inplace = True)
        else:
            # Store results in a dictionary (keys = usages)
            output_dictionary = analyzer_rt2012_re2020.get_results()
        data_dictionary[case_study]["outputs"] = copy.deepcopy(output_dictionary)

        # Clean file before saving information
        clean_file(os.path.join(directory_path, directory_data, case_study, "information.out"))

        # Post-process data and results
        list_of_outputs_dictionaries = []
        list_of_inputs_dictionaries   = []
        for usage in usages:
            # Set usage for each dataframe (dictionary key)
            data_dictionary[case_study]["outputs"][usage[0]]["usage"] = usage_keys[usage[0]]
            # Save each dataframe
            saving_path = os.path.join(directory_path, directory_data, case_study, "dataframes_outputs", "dataframe_" + usage[0] + ".csv")
            data_dictionary[case_study]["outputs"][usage[0]].to_csv(saving_path)
            # Save some information about them
            save_in_file(os.path.join(directory_path, directory_data, case_study, "information.out"), usage[0] + ": " + str(len(data_dictionary[case_study]["outputs"][usage[0]]["usage"])) + " cas\n")
            # Append all dataframes to concatenate them later
            list_of_outputs_dictionaries.append(data_dictionary[case_study]["outputs"][usage[0]])
            list_of_inputs_dictionaries.append(data_dictionary[case_study]["inputs"][usage[0]])

        # Store everything in a dataframe
        data_dictionary[case_study]["all_inputs"]    = pd.concat(list_of_inputs_dictionaries)
        data_dictionary[case_study]["all_outputs"]   = pd.concat(list_of_outputs_dictionaries)

        # Save some memory
        del data_dictionary[case_study]["inputs"]
        del data_dictionary[case_study]["outputs"]

        # Create a column "directory" that both will share
        data_dictionary[case_study]["all_inputs"]["directory"] = data_dictionary[case_study]["all_inputs"]["operations.xml"].apply(lambda operation: operation.split(".")[0])

        # Clean data
        data_dictionary[case_study]["all_inputs"].dropna(inplace = True)
        data_dictionary[case_study]["all_outputs"].dropna(inplace = True)

        # Reindex and clean data
        data_dictionary[case_study]["all_inputs"].reset_index(drop = True, inplace = True)
        data_dictionary[case_study]["all_outputs"].reset_index(drop = True, inplace = True)
        index_inputs = data_dictionary[case_study]["all_inputs"]["directory"].isin(data_dictionary[case_study]["all_outputs"]["directory"])
        index_outputs = data_dictionary[case_study]["all_outputs"]["directory"].isin(data_dictionary[case_study]["all_inputs"]["directory"])
        data_dictionary[case_study]["all_inputs"] = data_dictionary[case_study]["all_inputs"][index_inputs]
        data_dictionary[case_study]["all_outputs"] = data_dictionary[case_study]["all_outputs"][index_outputs]

        # Correct engine version number
        data_dictionary[case_study]["all_inputs"] = clean_engine_version(data_dictionary[case_study]["all_inputs"], "version_moteur", "< 8.1")
        data_dictionary[case_study]["all_outputs"] = add_engine_version(data_dictionary[case_study]["all_outputs"], "8.1")

        # Remove cases from xml files where the number of buildings does not match
        #data_dictionary[case_study]["all_outputs"], data_dictionary[case_study]["all_inputs"] = remove_rows_on_condition(data_dictionary[case_study]["all_outputs"], data_dictionary[case_study]["all_inputs"], "directory")
        data_dictionary[case_study]["all_outputs"], data_dictionary[case_study]["all_inputs"] = clean_different_number_of_buildings(data_dictionary[case_study]["all_outputs"], data_dictionary[case_study]["all_inputs"], "directory")

        # Update the indices and remove the directory column
        data_dictionary[case_study]["all_outputs"], data_dictionary[case_study]["all_inputs"] = update_indices(data_dictionary[case_study]["all_outputs"], data_dictionary[case_study]["all_inputs"], "directory")

        # Merge dataframes
        data_dictionary[case_study]["all"] = merge(data_dictionary[case_study]["all_outputs"], data_dictionary[case_study]["all_inputs"])

        # Save some memory
        del data_dictionary[case_study]["all_inputs"]
        del data_dictionary[case_study]["all_outputs"]

        # Filter dataframe
        if "degh" in data_dictionary[case_study]["all"].columns:
            data_dictionary[case_study]["all"] = filter_dataframe_with_quantiles(data_dictionary[case_study]["all"], "degh", percent = 0.02)

        if "cep" in data_dictionary[case_study]["all"].columns:
            # Add new columns
            data_dictionary[case_study]["all"] = data_dictionary[case_study]["all"].assign(cep_relatif = data_dictionary[case_study]["all"]["cep_max"] - data_dictionary[case_study]["all"]["cep"])

        if "bbio" in data_dictionary[case_study]["all"].columns:
            # Add new columns
            data_dictionary[case_study]["all"] = data_dictionary[case_study]["all"].assign(bbio_relatif = data_dictionary[case_study]["all"]["bbio_max"] - data_dictionary[case_study]["all"]["bbio"])

    # RT2020 / RE2020 meteo
    case_study_one = case_studies[1]
    case_study_two = case_studies[2]

    # RT2012 vs RE2020 - Maisons individuelles / Logements collectifs
    dataframe_mi_lc = copy.deepcopy(data_dictionary[case_study_one]["all"])


    # RT2012
    case_study = case_studies[0]
    # All usages
    analysis = PlotHandler(data_dictionary[case_study]["all"], [], [], os.path.join("..//figures", case_study))
    analysis.display_scatter_plot("tous_les_usages_rcr_vs_co2", third_indicator_builder, ["couple_systeme", "co2", "rcr"], "CO$_{\mathrm{2}}$ [kg$_{\mathrm{eq}}$ CO$_{\mathrm{2}}$/(m²$\cdot$an)]", "RCR [-]", None, None)
    analysis.display_scatter_plot("usages_rcr_vs_co2", third_indicator_builder, ["usage", "co2", "rcr"], "CO$_{\mathrm{2}}$ [kg$_{\mathrm{eq}}$ CO$_{\mathrm{2}}$/(m²$\cdot$an)]", "RCR [-]", None, None)

    analysis.display_violin_plot("tous_les_usages_cep",  first_indicator_builder, ["usage", "cep"], "Usages [-]", "Cep [kWh$_{\mathrm{EP}}$/(m²$\cdot$an)]", [(0,250),(0,275,25)], None, True)
    analysis.display_violin_plot("tous_les_usages_bbio", first_indicator_builder, ["usage", "bbio"], "Usages [-]", "Bbio [-]", [(0,200),(0,225,25)], None, True)
    analysis.display_violin_plot("tous_les_usages_degh", first_indicator_builder, ["usage", "degh"], "Usages [-]", "Degrés-heures [°C$\cdot$h]", [(0,1000),(0,1100,100)], None, True)
    analysis.display_violin_plot("tous_les_usages_cep_energie",  first_indicator_builder, ["energie_principale", "cep"], "Energie principale [-]", "Cep [kWh$_{\mathrm{EP}}$/(m²$\cdot$an)]", [(0,250),(0,275,25)], ["électricité", "gaz", "bois", "fioul"], True)
    analysis.display_violin_plot("tous_les_usages_bbio_energie", first_indicator_builder, ["energie_principale", "bbio"], "Energie principale [-]", "Bbio [-]", [(0,200),(0,225,25)], ["électricité", "gaz", "bois", "fioul"], True)
    analysis.display_violin_plot("tous_les_usages_cep_relatif",  first_indicator_builder, ["usage", "cep_relatif"], "Usages [-]", "$\Delta$Cep (Cep$_{\mathrm{max}}$ - Cep) [kWh$_{\mathrm{EP}}$/(m²$\cdot$an)]", [(0, 150), (0, 175, 25)], None, True)
    analysis.display_violin_plot("tous_les_usages_bbio_relatif", first_indicator_builder, ["usage", "bbio_relatif"], "Usages [-]", "$\Delta$Bbio (Bbio$_{\mathrm{max}}$ - Bbio) [-]", [(0,150),(0,175,25)], None, True)
    analysis.display_violin_plot("tous_les_usages_cep_energie_relatif", first_indicator_builder, ["energie_principale", "cep_relatif"], "Energie principale [-]", "$\Delta$Cep (Cep$_{\mathrm{max}}$ - Cep) [kWh$_{\mathrm{EP}}$/(m²$\cdot$an)]", [(0, 150), (0, 175, 25)], ["électricité", "gaz", "bois", "fioul"], True)
    analysis.display_violin_plot("tous_les_usages_bbio_energie_relatif", first_indicator_builder, ["energie_principale", "bbio_relatif"], "Energie principale [-]", "$\Delta$Bbio (Bbio$_{\mathrm{max}}$ - Bbio) [-]", [(0, 150), (0, 175, 25)], ["électricité", "gaz", "bois", "fioul"], True)

    # RCR / CO2 (remove heatin district)
    analysis.dataframe_temporary = analysis.dataframe_temporary[~analysis.dataframe_temporary.couple_systeme.str.contains("Res")]
    analysis.display_violin_plot("tous_les_usages_co2", first_indicator_builder, ["usage", "co2"], "Usages [-]", "CO$_{\mathrm{2}}$ [kg$_{\mathrm{eq}}$ CO$_{\mathrm{2}}$/(m²$\cdot$an)]", [(0,30),(0,35,5)], None, True)
    analysis.display_violin_plot("tous_les_usages_rcr", first_indicator_builder, ["usage", "rcr"], "Usages [-]", "RCR [-]", [(0,0.8),(0,0.9,0.1)], None, True)

    # Maisons individuelles
    dataframe_mi                    = copy.deepcopy(data_dictionary[case_study]["all"])
    dataframe_mi["couple_systeme"]  = dataframe_mi["couple_systeme"].apply(lambda system: system if system in systems["Maisons individuelles"] else "Autres")
    analysis = PlotHandler(dataframe_mi, [("usage", "==", "Maisons individuelles")], [], os.path.join("..//figures", case_study))
    analysis.display_violin_plot("couple_system_cep_mi",
                                 first_indicator_builder,
                                 ["couple_systeme", "cep", "co2"],
                                 "Systèmes [-]",
                                 "Cep [kWh$_{\mathrm{EP}}$/(m²$\cdot$an)]",
                                 [(0, 90), (0, 100, 10)],
                                 ["pac ds\npac ds", "chaudière gaz à \ncondensation\nCET", "poêle/insert\nCET", "autres", "pac air ext/recyclé\nCET"], True)
    analysis.display_violin_plot("couple_system_cep_mi_relatif",
                                 first_indicator_builder,
                                 ["couple_systeme", "cep_relatif", "co2"],
                                 "Système [-]",
                                 "$\Delta$Cep (Cep$_{\mathrm{max}}$ - Cep) [kWh$_{\mathrm{EP}}$/(m²$\cdot$an)]",
                                 [(0, 70), (0, 80, 10)],
                                 ["pac ds\npac ds", "chaudière gaz à \ncondensation\nCET", "poêle/insert\nCET", "autres", "pac air ext/recyclé\nCET"], True)

    # Logements collectifs
    dataframe_lc                    = copy.deepcopy(data_dictionary[case_study]["all"])
    dataframe_lc["couple_systeme"]  = dataframe_lc["couple_systeme"].apply(lambda system: system if system in systems["Logements collectifs"] else "Autres")
    analysis = PlotHandler(dataframe_lc, [("usage", "==", "Logements collectifs")], [], os.path.join("..//figures", case_study))
    analysis.display_violin_plot("couple_system_cep_lc",
                                 first_indicator_builder,
                                 ["couple_systeme", "cep", "co2"],
                                 "Systèmes [-]",
                                 "Cep [kWh$_{\mathrm{EP}}$/(m²$\cdot$an)]",
                                 [(0, 90), (0, 100, 10)],
                                 ["chaudière gaz à \ncondensation\nchaudière gaz à \ncondensation (ballon)",
                                  "chaudière gaz à \ncondensation\nchaudière gaz à \ncondensation", "autres",
                                 "effet joule\nCET", "réseau de chaleur\nréseau de chaleur"], True)
    analysis.display_violin_plot("couple_system_cep_lc_relatif",
                                 first_indicator_builder,
                                 ["couple_systeme", "cep_relatif", "co2"],
                                 "Système [-]",
                                 "$\Delta$Cep (Cep$_{\mathrm{max}}$ - Cep) [kWh$_{\mathrm{EP}}$/(m²$\cdot$an)]",
                                 [(0, 70), (0, 80, 10)],
                                 ["chaudière gaz à \ncondensation\nchaudière gaz à \ncondensation (ballon)",
                                  "chaudière gaz à \ncondensation\nchaudière gaz à \ncondensation", "autres",
                                  "effet joule\nCET", "réseau de chaleur\nréseau de chaleur"], True)

    # Bureaux
    dataframe_bu                    = copy.deepcopy(data_dictionary[case_study]["all"])
    dataframe_bu["couple_systeme"]  = dataframe_bu["couple_systeme"].apply(lambda system: system if system in systems["Bureaux"] else "Autres")
    analysis = PlotHandler(dataframe_bu, [("usage", "==", "Bureaux")], [], os.path.join("..//figures", case_study))
    analysis.display_violin_plot("couple_system_cep_bu",
                                 first_indicator_builder,
                                 ["couple_systeme", "cep", "co2"],
                                 "Systèmes [-]",
                                 "Cep [kWh$_{\mathrm{EP}}$/(m²$\cdot$an)]",
                                 [(0, 175), (0, 200, 25)],
                                 ["pac réversible\nballon effet joule\npac réversible", "effet joule\nCET",
                                  "drv\nballon effet joule\ndrv", "autres",  "pac air ext/recyclé\nballon effet joule"], True)
    analysis.display_violin_plot("couple_system_cep_bu_relatif",
                                 first_indicator_builder,
                                 ["couple_systeme", "cep_relatif", "co2"],
                                 "Systèmes [-]",
                                 "$\Delta$Cep (Cep$_{\mathrm{max}}$ - Cep) [kWh$_{\mathrm{EP}}$/(m²$\cdot$an)]",
                                 [(0, 125), (0, 150, 25)],
                                 ["pac réversible\nballon effet joule\npac réversible", "effet joule\nCET",
                                  "drv\nballon effet joule\ndrv", "autres",  "pac air ext/recyclé\nballon effet joule"], True)

    # Enseignement
    dataframe_ens                    = copy.deepcopy(data_dictionary[case_study]["all"])
    dataframe_ens["couple_systeme"]  = dataframe_ens["couple_systeme"].apply(lambda system: system if system in systems["Etablissements d'enseignement"] else "Autres")
    analysis = PlotHandler(dataframe_ens, [("usage", "==", "Etablissements d'enseignement")], [], os.path.join("..//figures", case_study))
    analysis.display_violin_plot("couple_system_cep_ens",
                                 first_indicator_builder,
                                 ["couple_systeme", "cep", "co2"],
                                 "Systèmes [-]",
                                 "Cep [kWh$_{\mathrm{EP}}$/(m²$\cdot$an)]",
                                 [(0, 250), (0, 275, 25)],
                                 ["autres", "chaudière gaz à \ncondensation\nballon effet joule", "effet joule\nballon effet joule", "pac air ext/recyclé\nballon effet joule", "chaudière gaz à \ncondensation\nchaudière gaz à \ncondensation (ballon)"], True)
    analysis.display_violin_plot("couple_system_cep_ens_relatif",
                                 first_indicator_builder,
                                 ["couple_systeme", "cep_relatif", "co2"],
                                 "Systèmes [-]",
                                 "$\Delta$Cep (Cep$_{\mathrm{max}}$ - Cep) [kWh$_{\mathrm{EP}}$/(m²$\cdot$an)]",
                                 [(0, 150), (0, 175, 25)],
                                 ["autres", "chaudière gaz à \ncondensation\nballon effet joule", "effet joule\nballon effet joule", "pac air ext/recyclé\nballon effet joule", "chaudière gaz à \ncondensation\nchaudière gaz à \ncondensation (ballon)"], True)

    # Maisons individuelles / Logements collectifs
    dataframe_mi_lc = copy.deepcopy(data_dictionary[case_study]["all"])
    analysis = PlotHandler(dataframe_mi_lc,
                           [("usage", "==", "Maisons individuelles"), ("usage", "==", "Logements collectifs")], [],
                           os.path.join("..//figures", case_study))
    analysis.display_violin_plot("mi_lc_cep_version",
                                 second_indicator_builder,
                                 [("usage", "version", "cep"), ("usage", "version_moteur", "cep_projet")],
                                 "Usage [-]",
                                 "Cep [kWh$_{\mathrm{EP}}$/(m²$\cdot$an)]",
                                 [(0, 100), (0, 110, 10)],
                                 ["MI < 8.1", "MI 8.1", "LC < 8.1", "LC 8.1"], True)
    analysis.display_violin_plot("mi_lc_bbio_version",
                                 second_indicator_builder,
                                 [("usage", "version", "bbio"), ("usage", "version_moteur", "bbio_projet")],
                                 "Usage [-]",
                                 "Bbio [-]",
                                 [(0, 120), (0, 130, 10)],
                                 ["MI < 8.1", "MI 8.1", "LC < 8.1", "LC 8.1"], True)

    # Bureaux / Enseignement
    dataframe_bu_ens = copy.deepcopy(data_dictionary[case_study]["all"])
    analysis = PlotHandler(dataframe_bu_ens,
                           [("usage", "==", "Bureaux"), ("usage", "==", "Etablissements d'enseignement")], [],
                           os.path.join("..//figures", case_study))
    analysis.display_violin_plot("bu_ens_cep_version",
                                 second_indicator_builder,
                                 [("usage", "version", "cep"), ("usage", "version_moteur", "cep_projet")],
                                 "Usage [-]",
                                 "Cep [kWh$_{\mathrm{EP}}$/(m²$\cdot$an)]",
                                 [(0, 250), (0, 275, 25)],
                                 ["ENS < 8.1", "ENS 8.1", "BU < 8.1", "BU 8.1"], True)
    analysis.display_violin_plot("bu_ens_bbio_version",
                                 second_indicator_builder,
                                 [("usage", "version", "bbio"), ("usage", "version_moteur", "bbio_projet")],
                                 "Usage [-]",
                                 "Bbio [-]",
                                 [(0, 200), (0, 225, 25)],
                                 ["ENS < 8.1", "ENS 8.1", "BU < 8.1", "BU 8.1"], True)

    # SURFACE - Maisons individuelles / Logements collectifs
    dataframe_mi_lc = copy.deepcopy(data_dictionary[case_study]["all"])
    analysis = PlotHandler(dataframe_mi_lc,
                           [("usage", "==", "Maisons individuelles"), ("usage", "==", "Logements collectifs")], [],
                           os.path.join("..//figures", case_study))
    analysis.display_violin_plot("mi_lc_cep_version_surface",
                                 fourth_indicator_builder,
                                 ["usage", "shon", "shab", "cep"],
                                 "Usage [-]",
                                 "Cep [kWh$_{\mathrm{EP}}$/(m²$\cdot$an)]",
                                 [(0, 120), (0, 130, 10)],
                                 ["MI RT2012", "MI RT/RE", "LC RT2012", "LC RT/RE"], True)
    analysis.display_violin_plot("mi_lc_bbio_version_surface",
                                 fourth_indicator_builder,
                                 ["usage", "shon", "shab", "bbio"],
                                 "Usage [-]",
                                 "Bbio [-]",
                                 [(0, 140), (0, 150, 10)],
                                 ["MI RT2012", "MI RT/RE", "LC RT2012", "LC RT/RE"], True)

    # SURFACE - Bureaux / Enseignement
    dataframe_bu_ens = copy.deepcopy(data_dictionary[case_study]["all"])
    analysis = PlotHandler(dataframe_bu_ens,
                           [("usage", "==", "Bureaux"), ("usage", "==", "Etablissements d'enseignement")], [],
                           os.path.join("..//figures", case_study))
    analysis.display_violin_plot("bu_ens_cep_version_surface",
                                 fourth_indicator_builder,
                                 ["usage", "shon", "surt", "cep"],
                                 "Usage [-]",
                                 "Cep [kWh$_{\mathrm{EP}}$/(m²$\cdot$an)]",
                                 [(0, 250), (0, 275, 25)],
                                 ["ENS RT2012", "ENS RT/RE", "BU RT2012", "BU RT/RE"], True)
    analysis.display_violin_plot("bu_ens_bbio_version_surface",
                                 fourth_indicator_builder,
                                 ["usage", "shon", "surt", "bbio"],
                                 "Usage [-]",
                                 "Cep [kWh$_{\mathrm{EP}}$/(m²$\cdot$an)]",
                                 [(0, 200), (0, 225, 25)],
                                 ["ENS RT2012", "ENS RT/RE", "BU RT2012", "BU RT/RE"], True)






    # RT2020
    case_study = case_studies[1]
    # All usages
    analysis = PlotHandler(data_dictionary[case_study]["all"], [], [], os.path.join("..//figures", case_study))
    analysis.display_scatter_plot("tous_les_usages_rcr_vs_co2", third_indicator_builder, ["couple_systeme", "co2", "rcr"], "CO$_{\mathrm{2}}$ [kg$_{\mathrm{eq}}$ CO$_{\mathrm{2}}$/(m²$\cdot$an)]", "RCR [-]", [(0,1),(0,1.1,0.1)], None)
    analysis.display_scatter_plot("usages_rcr_vs_co2", third_indicator_builder, ["usage", "co2", "rcr"], "CO$_{\mathrm{2}}$ [kg$_{\mathrm{eq}}$ CO$_{\mathrm{2}}$/(m²$\cdot$an)]", "RCR [-]", None, None)

    analysis.display_violin_plot("tous_les_usages_cep",  first_indicator_builder, ["usage", "cep"], "Usages [-]", "Cep [kWh$_{\mathrm{EP}}$/(m²$\cdot$an)]", [(0,200),(0,225,25)], None, True)
    analysis.display_violin_plot("tous_les_usages_bbio", first_indicator_builder, ["usage", "bbio"], "Usages [-]", "Bbio [-]", [(0,150),(0,175,25)], None, True)
    analysis.display_violin_plot("tous_les_usages_degh", first_indicator_builder, ["usage", "degh"], "Usages [-]", "Degrés-heures [°C$\cdot$h]", [(0,2000),(0,2250,250)], None, True)
    analysis.display_violin_plot("tous_les_usages_cep_energie",  first_indicator_builder, ["energie_principale", "cep"], "Energie principale [-]", "Cep [kWh$_{\mathrm{EP}}$/(m²$\cdot$an)]", [(0,200),(0,225,25)], ["électricité", "gaz", "bois", "fioul"], True)
    analysis.display_violin_plot("tous_les_usages_bbio_energie", first_indicator_builder, ["energie_principale", "bbio"], "Energie principale [-]", "Bbio [-]", [(0,150),(0,175,25)], ["électricité", "gaz", "bois", "fioul"], True)
    analysis.display_violin_plot("tous_les_usages_cep_relatif",  first_indicator_builder, ["usage", "cep_relatif"], "Usages [-]", "$\Delta$Cep (Cep$_{\mathrm{max}}$ - Cep) [kWh$_{\mathrm{EP}}$/(m²$\cdot$an)]", [(0,150),(0,175,25)], None, True)
    analysis.display_violin_plot("tous_les_usages_bbio_relatif", first_indicator_builder, ["usage", "bbio_relatif"], "Usages [-]", "$\Delta$Bbio (Bbio$_{\mathrm{max}}$ - Bbio) [-]", [(0,150),(0,175,25)], None, True)
    analysis.display_violin_plot("tous_les_usages_cep_energie_relatif", first_indicator_builder, ["energie_principale", "cep_relatif"], "Energie principale [-]", "$\Delta$Cep (Cep$_{\mathrm{max}}$ - Cep) [kWh$_{\mathrm{EP}}$/(m²$\cdot$an)]", [(0, 200), (0, 225, 25)], ["électricité", "gaz", "bois", "fioul"], True)
    analysis.display_violin_plot("tous_les_usages_bbio_energie_relatif", first_indicator_builder, ["energie_principale", "bbio_relatif"], "Energie principale [-]", "$\Delta$Bbio (Bbio$_{\mathrm{max}}$ - Bbio) [-]", [(0, 150), (0, 175, 25)], ["électricité", "gaz", "bois", "fioul"], True)

    # RCR / CO2 (remove heatin district)
    analysis.dataframe_temporary = analysis.dataframe_temporary[~analysis.dataframe_temporary.couple_systeme.str.contains("Res")]
    analysis.display_violin_plot("tous_les_usages_co2", first_indicator_builder, ["usage", "co2"], "Usages [-]", "CO$_{\mathrm{2}}$ [kg$_{\mathrm{eq}}$ CO$_{\mathrm{2}}$/(m²$\cdot$an)]", [(0,50),(0,55,5)], None, True)
    analysis.display_violin_plot("tous_les_usages_rcr", first_indicator_builder, ["usage", "rcr"], "Usages [-]", "RCR [-]", [(0,1),(0,1.1,0.1)], None, True)

    # Maisons individuelles
    dataframe_mi                        = copy.deepcopy(data_dictionary[case_study]["all"])
    dataframe_mi["couple_systeme"]      = dataframe_mi["couple_systeme"].apply(lambda system: system if system in systems["Maisons individuelles"] else "Autres")
    analysis                            = PlotHandler(dataframe_mi, [("usage", "==", "Maisons individuelles")], [], os.path.join("..//figures", case_study))
    analysis.display_violin_plot("couple_system_cep_mi",
                                 first_indicator_builder,
                                 ["couple_systeme", "cep", "co2"],
                                 "Système [-]",
                                 "Cep [kWh$_{\mathrm{EP}}$/(m²$\cdot$an)]",
                                 [(0, 130), (0, 140, 10)],
                                 ["pac ds\npac ds", "chaudière gaz à \ncondensation\nCET", "poêle/insert\nCET", "autres", "pac air ext/recyclé\nCET"], True)
    analysis.display_violin_plot("couple_system_cep_mi_relatif",
                                 first_indicator_builder,
                                 ["couple_systeme", "cep_relatif", "co2"],
                                 "Système [-]",
                                 "$\Delta$Cep (Cep$_{\mathrm{max}}$ - Cep) [kWh$_{\mathrm{EP}}$/(m²$\cdot$an)]",
                                 [(0, 80), (0, 90, 10)],
                                 ["pac ds\npac ds", "chaudière gaz à \ncondensation\nCET", "poêle/insert\nCET", "autres", "pac air ext/recyclé\nCET"], True)

    # Logements collectifs
    dataframe_lc                    = copy.deepcopy(data_dictionary[case_study]["all"])
    dataframe_lc["couple_systeme"]  = dataframe_lc["couple_systeme"].apply(lambda system: system if system in systems["Logements collectifs"] else "Autres")
    analysis                        = PlotHandler(dataframe_lc, [("usage", "==", "Logements collectifs")], [], os.path.join("..//figures", case_study))
    analysis.display_violin_plot("couple_system_cep_lc",
                                 first_indicator_builder,
                                 ["couple_systeme", "cep", "co2"],
                                 "Système [-]",
                                 "Cep [kWh$_{\mathrm{EP}}$/(m²$\cdot$an)]",
                                 [(0, 130), (0, 140, 10)],
                                 ["chaudière gaz à \ncondensation\nchaudière gaz à \ncondensation (ballon)",
                                  "chaudière gaz à \ncondensation\nchaudière gaz à \ncondensation", "autres",
                                  "effet joule\nCET", "réseau de chaleur\nréseau de chaleur"], True)
    analysis.display_violin_plot("couple_system_cep_lc_relatif",
                                 first_indicator_builder,
                                 ["couple_systeme", "cep_relatif", "co2"],
                                 "Systèmes [-]",
                                 "$\Delta$Cep (Cep$_{\mathrm{max}}$ - Cep) [kWh$_{\mathrm{EP}}$/(m²$\cdot$an)]",
                                 [(0, 70), (0, 80, 10)],
                                 ["chaudière gaz à \ncondensation\nchaudière gaz à \ncondensation (ballon)",
                                  "chaudière gaz à \ncondensation\nchaudière gaz à \ncondensation", "autres",
                                  "effet joule\nCET", "réseau de chaleur\nréseau de chaleur"], True)

    # Bureaux
    dataframe_bu                    = copy.deepcopy(data_dictionary[case_study]["all"])
    dataframe_bu["couple_systeme"]  = dataframe_bu["couple_systeme"].apply(lambda system: system if system in systems["Bureaux"] else "Autres")
    analysis                        = PlotHandler(dataframe_bu, [("usage", "==", "Bureaux")], [], os.path.join("..//figures", case_study))
    analysis.display_violin_plot("couple_system_cep_bu",
                                 first_indicator_builder,
                                 ["couple_systeme", "cep", "co2"],
                                 "Systèmes [-]",
                                 "Cep [kWh$_{\mathrm{EP}}$/(m²$\cdot$an)]",
                                 [(0, 150), (0, 175, 25)],
                                 ["pac réversible\nballon effet joule\npac réversible", "effet joule\nCET",
                                  "drv\nballon effet joule\ndrv", "autres", "pac air ext/recyclé\nballon effet joule"],
                                 True)
    analysis.display_violin_plot("couple_system_cep_bu_relatif",
                                 first_indicator_builder,
                                 ["couple_systeme", "cep_relatif", "co2"],
                                 "Systèmes [-]",
                                 "$\Delta$Cep (Cep$_{\mathrm{max}}$ - Cep) [kWh$_{\mathrm{EP}}$/(m²$\cdot$an)]",
                                 [(0, 140), (0, 150, 10)],
                                 ["pac réversible\nballon effet joule\npac réversible", "effet joule\nCET",
                                  "drv\nballon effet joule\ndrv", "autres", "pac air ext/recyclé\nballon effet joule"],
                                 True)

    # Enseignement
    dataframe_ens                   = copy.deepcopy(data_dictionary[case_study]["all"])
    dataframe_ens["couple_systeme"] = dataframe_ens["couple_systeme"].apply(lambda system: system if system in systems["Etablissements d'enseignement"] else "Autres")
    analysis                        = PlotHandler(dataframe_ens, [("usage", "==", "Etablissements d'enseignement")], [], os.path.join("..//figures", case_study))
    analysis.display_violin_plot("couple_system_cep_ens",
                                 first_indicator_builder,
                                 ["couple_systeme", "cep", "co2"],
                                 "Systèmes [-]",
                                 "Cep [kWh$_{\mathrm{EP}}$/(m²$\cdot$an)]",
                                 [(0, 175), (0, 200, 25)],
                                 ["autres", "chaudière gaz à \ncondensation\nballon effet joule",
                                  "effet joule\nballon effet joule", "pac air ext/recyclé\nballon effet joule",
                                  "chaudière gaz à \ncondensation\nchaudière gaz à \ncondensation (ballon)"], True)
    analysis.display_violin_plot("couple_system_cep_ens_relatif",
                                 first_indicator_builder,
                                 ["couple_systeme", "cep_relatif", "co2"],
                                 "Systèmes [-]",
                                 "$\Delta$Cep (Cep$_{\mathrm{max}}$ - Cep) [kWh$_{\mathrm{EP}}$/(m²$\cdot$an)]",
                                 [(0, 150), (0, 160, 10)],
                                 ["autres", "chaudière gaz à \ncondensation\nballon effet joule",
                                  "effet joule\nballon effet joule", "pac air ext/recyclé\nballon effet joule",
                                  "chaudière gaz à \ncondensation\nchaudière gaz à \ncondensation (ballon)"], True)

    # RT2012 / RE2020
    case_study_one = case_studies[0]
    case_study_two = case_studies[1]

    dataframe_one, dataframe_two = remove_rows_on_condition(data_dictionary[case_study_one]["all"], data_dictionary[case_study_two]["all"], "operations.xml")

    # RT2012 vs RE2020 - Maisons individuelles / Logements collectifs
    dataframe_mi_lc = copy.deepcopy(dataframe_one)
    analysis = PlotHandler(dataframe_mi_lc,
                           [("usage", "==", "Maisons individuelles"), ("usage", "==", "Logements collectifs")], [],
                           os.path.join("..//figures", case_study_one + "_" + case_study_two))
    analysis.add_additional_dataframe(dataframe_two)
    analysis.display_violin_plot("mi_lc_cep_version",
                                 fifth_indicator_builder,
                                 ["usage", "cep"],
                                 "Usage [-]",
                                 "Cep [kWh$_{\mathrm{EP}}$/(m²$\cdot$an)]",
                                 [(0, 120), (0, 130, 10)],
                                 ["MI RT2012", "MI RE2020", "LC RT2012", "LC RE2020"], True)
    analysis.display_violin_plot("mi_lc_bbio_version",
                                 fifth_indicator_builder,
                                 ["usage", "bbio"],
                                 "Usage [-]",
                                 "Bbio [-]",
                                 [(0, 140), (0, 150, 10)],
                                 ["MI RT2012", "MI RE2020", "LC RT2012", "LC RE2020"], True)








    # RT2020 / RE2020 meteo
    case_study_one = case_studies[1]
    case_study_two = case_studies[2]

    dataframe_one, dataframe_two = remove_rows_on_condition(data_dictionary[case_study_one]["all"], data_dictionary[case_study_two]["all"], "operations.xml")

    # RT2012 vs RE2020 - Maisons individuelles / Logements collectifs
    dataframe_mi_lc = copy.deepcopy(dataframe_one)
    analysis = PlotHandler(dataframe_mi_lc,
                           [("usage", "==", "Maisons individuelles"), ("usage", "==", "Logements collectifs")], [],
                           os.path.join("..//figures", case_study_one + "_" + case_study_two))
    analysis.add_additional_dataframe(dataframe_two)
    analysis.display_violin_plot("mi_lc_cep_version",
                                 sixth_indicator_builder,
                                 ["usage", "cep"],
                                 "Usage [-]",
                                 "Cep [kWh$_{\mathrm{EP}}$/(m²$\cdot$an)]",
                                 [(0, 120), (0, 130, 10)],
                                 ["MI RE2020 METEO", "MI RE2020", "LC RE2020 METEO", "LC RE2020"], True)
    analysis.display_violin_plot("mi_lc_cep_version_relatif",
                                 sixth_indicator_builder,
                                 ["usage", "cep_relatif"],
                                 "Usage [-]",
                                 "$\Delta$Cep (Cep$_{\mathrm{max}}$ - Cep) [kWh$_{\mathrm{EP}}$/(m²$\cdot$an)]",
                                 [(0, 70), (0, 80, 10)],
                                 ["MI RE2020 METEO", "MI RE2020", "LC RE2020 METEO", "LC RE2020"], True)
    analysis.display_violin_plot("mi_lc_bbio_version",
                                 sixth_indicator_builder,
                                 ["usage", "bbio"],
                                 "Usage [-]",
                                 "Bbio [-]",
                                 [(0, 175), (0, 200, 25)],
                                 ["MI RE2020 METEO", "MI RE2020", "LC RE2020 METEO", "LC RE2020"], True)
    analysis.display_violin_plot("mi_lc_bbio_version_relatif",
                                 sixth_indicator_builder,
                                 ["usage", "bbio_relatif"],
                                 "Usage [-]",
                                 "$\Delta$Bbio (Bbio$_{\mathrm{max}}$ - Bbio) [-]",
                                 [(0, 60), (0, 70, 10)],
                                 ["MI RE2020 METEO", "MI RE2020", "LC RE2020 METEO", "LC RE2020"], True)

    # RT2012 vs RE2020 - Bureaux / Logements collectifs
    dataframe_bu_ens = copy.deepcopy(dataframe_one)
    analysis = PlotHandler(dataframe_bu_ens,
                           [("usage", "==", "Bureaux"), ("usage", "==", "Etablissements d'enseignement")], [],
                           os.path.join("..//figures", case_study_one + "_" + case_study_two))
    analysis.add_additional_dataframe(dataframe_two)
    analysis.display_violin_plot("bu_ens_cep_version",
                                 sixth_indicator_builder,
                                 ["usage", "cep"],
                                 "Usage [-]",
                                 "Cep [kWh$_{\mathrm{EP}}$/(m²$\cdot$an)]",
                                 [(0, 175), (0, 200, 25)],
                                 ["ENS RE2020 METEO", "ENS RE2020", "BU RE2020 METEO", "BU RE2020"], True)
    analysis.display_violin_plot("bu_ens_cep_version_relatif",
                                 sixth_indicator_builder,
                                 ["usage", "cep_relatif"],
                                 "Usage [-]",
                                 "$\Delta$Cep (Cep$_{\mathrm{max}}$ - Cep) [kWh$_{\mathrm{EP}}$/(m²$\cdot$an)]",
                                 [(0, 125), (0, 150, 25)],
                                 ["ENS RE2020 METEO", "ENS RE2020", "BU RE2020 METEO", "BU RE2020"], True)
    analysis.display_violin_plot("bu_ens_bbio_version",
                                 sixth_indicator_builder,
                                 ["usage", "bbio"],
                                 "Usage [-]",
                                 "Bbio [-]",
                                 [(0, 200), (0, 225, 25)],
                                 ["ENS RE2020 METEO", "ENS RE2020", "BU RE2020 METEO", "BU RE2020"], True)
    analysis.display_violin_plot("bu_ens_bbio_version_relatif",
                                 sixth_indicator_builder,
                                 ["usage", "bbio_relatif"],
                                 "Usage [-]",
                                 "$\Delta$Bbio (Bbio$_{\mathrm{max}}$ - Bbio) [-]",
                                 [(0, 150), (0, 175, 25)],
                                 ["ENS RE2020 METEO", "ENS RE2020", "BU RE2020 METEO", "BU RE2020"], True)



    # RT2020 / RE2020 CLIM FICTIVE
    case_study_one = case_studies[1]
    case_study_two = case_studies[4]

    dataframe_one, dataframe_two = remove_rows_on_condition(data_dictionary[case_study_one]["all"], data_dictionary[case_study_two]["all"], "operations.xml")

    # All usages
    analysis = PlotHandler(dataframe_one, [], [], os.path.join("..//figures", case_study_one + "_" + case_study_two))
    analysis.add_additional_dataframe(dataframe_two)
    analysis.display_scatter_plot("tous_les_usages_bbio_froid_vs_cep_froid", eighth_indicator_builder, ["couple_systeme", "bbio_froid", "cef_froid"], "Bbio froid [-]", "Cep froid [kWh$_{\mathrm{EP}}$/(m²$\cdot$an)]", [(0, 60), (0, 70, 10)], None)
    analysis.display_scatter_plot("usages_bbio_froid_vs_cep_froid", eighth_indicator_builder, ["usage", "bbio_froid", "cef_froid"], "Bbio froid [-]", "Cep froid [kWh$_{\mathrm{EP}}$/(m²$\cdot$an)]", [(0, 90), (0, 100, 10)], None)



    # RT2012 vs RE2020 - Maisons individuelles / Logements collectifs
    dataframe_mi_lc = copy.deepcopy(dataframe_one)
    analysis = PlotHandler(dataframe_mi_lc,
                           [("usage", "==", "Maisons individuelles"), ("usage", "==", "Logements collectifs")], [],
                           os.path.join("..//figures", case_study_one + "_" + case_study_two))
    analysis.add_additional_dataframe(dataframe_two)
    analysis.display_violin_plot("mi_lc_cep_version",
                                 seventh_indicator_builder,
                                 ["usage", "cep"],
                                 "Usage [-]",
                                 "Cep [kWh$_{\mathrm{EP}}$/(m²$\cdot$an)]",
                                 [(0, 150), (0, 175, 25)],
                                 ["MI RE2020 CLIM FICTIVE", "MI RE2020", "LC RE2020 CLIM FICTIVE", "LC RE2020"], True)
    analysis.display_violin_plot("mi_lc_cep_version_relatif",
                                 seventh_indicator_builder,
                                 ["usage", "cep_relatif"],
                                 "Usage [-]",
                                 "$\Delta$Cep (Cep$_{\mathrm{max}}$ - Cep) [kWh$_{\mathrm{EP}}$/(m²$\cdot$an)]",
                                 [(0, 70), (0, 80, 10)],
                                 ["MI RE2020 CLIM FICTIVE", "MI RE2020", "LC RE2020 CLIM FICTIVE", "LC RE2020"], True)

    # RT2012 vs RE2020 - Bureaux / Etablissements d'enseignement
    dataframe_bu_ens        = copy.deepcopy(dataframe_one)
    analysis                = PlotHandler(dataframe_bu_ens, [("usage", "==", "Bureaux"), ("usage", "==", "Etablissements d'enseignement")], [], os.path.join("..//figures", case_study_one + "_" + case_study_two))
    analysis.add_additional_dataframe(dataframe_two)
    analysis.display_violin_plot("bu_ens_cep_version",
                                 seventh_indicator_builder,
                                 ["usage", "cep"],
                                 "Usage [-]",
                                 "Cep [kWh$_{\mathrm{EP}}$/(m²$\cdot$an)]",
                                 [(0, 175), (0, 200, 25)],
                                 ["ENS RE2020 CLIM FICTIVE", "ENS RE2020", "BU RE2020 CLIM FICTIVE", "BU RE2020"], True)
    analysis.display_violin_plot("bu_ens_cep_version_relatif",
                                 seventh_indicator_builder,
                                 ["usage", "cep_relatif"],
                                 "Usage [-]",
                                 "$\Delta$Cep (Cep$_{\mathrm{max}}$ - Cep) [kWh$_{\mathrm{EP}}$/(m²$\cdot$an)]",
                                 [(0, 150), (0, 175, 25)],
                                 ["ENS RE2020 CLIM FICTIVE", "ENS RE2020", "BU RE2020 CLIM FICTIVE", "BU RE2020"], True)

    print("\nEND")


if __name__ == "__main__":
    main()